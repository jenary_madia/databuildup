<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return Redirect::to('login');
});

Auth::routes();

Route::get('/logout', function () {
    Auth::logout();
    return redirect()->route('login');
})->name('logout');
    
Route::group(['middleware' => 'auth'], function() {

    Route::get('dashboard', function () {
        return view('dashboard',[
            'tab' => 'Dashboard',
            'module' => 'Dashboard'
        ]);
    });

    /*****User Maintenance******/
    Route::group(['prefix'=>'user'], function () {
        /* Change Password */
        Route::get('change-password', function () {
            return view('user.change-password',[
                'tab' => 'User',
                'module' => 'Change Password'
            ]);
        });
        Route::post('change-password','UserController@updatePassword');
        /* End Change Password */
    });

    Route::group(['prefix'=>'file-maintenance'], function()
    {
        /***************COMPANY BUILD UP***************/
            Route::get('company-profile', 'FileMaintenance\CompanyBuildUpController@companyProfile')->name('maintenance.company');
            Route::get('departments', 'FileMaintenance\CompanyBuildUpController@departments')->name('maintenance.department');
            Route::get('employee-types', 'FileMaintenance\CompanyBuildUpController@employeTypes')->name('maintenance.employee-types');
            Route::get('pay-class', 'FileMaintenance\CompanyBuildUpController@payClass')->name('maintenance.pay-class');
            Route::get('job-positions', 'FileMaintenance\CompanyBuildUpController@jobPositions')->name('maintenance.job-positions');

            //Action
            Route::post('add-buildup', 'Api\FileMaintenance\CompanyBuildUpController@store')->name('maintenance.add.buildup');
            Route::post('delete-buildup', 'Api\FileMaintenance\CompanyBuildUpController@remove')->name('maintenance.delete.buildup');
            Route::post('update-company-details', 'Api\FileMaintenance\CompanyBuildUpController@updateCompDetails')->name('maintenance.update.company_details');

            //Datatable
            Route::post('fetch-buildup/{id}', 'Api\FileMaintenance\CompanyBuildUpController@fetch')->name('maintenance.fetch.buildup');
        /***************END COMPANY BUILD UP***************/

	/*
            Access Rights
        */
        Route::group(['prefix'=>'access-rights'], function ()
        {
            Route::get('user_roles', 'FileMaintenance\AccessRightsController@userRoles')->name('maintenance.access-rights.user-roles');
            Route::get('user_access', 'FileMaintenance\AccessRightsController@userAccess')->name('maintenance.access-rights.user-access');

            Route::post('add_user_role', 'Api\FileMaintenance\AccessRightsController@store')->name('maintenance.access-rights.add-user-roles');

            Route::post('fetch_modules', 'Api\FileMaintenance\AccessRightsController@fetch')->name('maintenance.access-rights.modules.list');
        });

        Route::get('holiday', 'HolController@report')->name('holiday.maintenance');
         Route::post('add-holiday', 'HolController@store')->name('holiday.add');
         Route::post('delete-holiday', 'HolController@delete')->name('holiday.delete');

         Route::get('tardiness', 'TardinessController@report')->name('tardiness.maintenance');
         Route::post('add-tardiness', 'TardinessController@store')->name('tardiness.add');
         Route::post('delete-tardiness', 'TardinessController@delete')->name('tardiness.delete');

         Route::get('overtime', 'OvertimeController@report')->name('overtime.maintenance');
         Route::post('add-overtime', 'OvertimeController@store')->name('overtime.add');
         Route::post('delete-overtime', 'OvertimeController@delete')->name('overtime.delete');

         Route::get('undertime', 'UndertimeController@report')->name('undertime.maintenance');
         Route::post('add-undertime', 'UndertimeController@store')->name('undertime.add');
         Route::post('delete-undertime', 'UndertimeController@delete')->name('undertime.delete');

         Route::get('philhealth', 'PhilhealthController@report')->name('philhealth.maintenance');
         Route::post('add-philhealth', 'PhilhealthController@store')->name('philhealth.add');
         Route::post('delete-philhealth', 'PhilhealthController@delete')->name('philhealth.delete');

         Route::get('sss', 'SSSController@report')->name('sss.maintenance');
         Route::post('add-sss', 'SSSController@store')->name('sss.add');
         Route::post('delete-sss', 'SSSController@delete')->name('sss.delete');

         Route::get('leavetype', 'LeaveTypeController@report')->name('leavetype.maintenance');
         Route::post('add-leavetype', 'LeaveTypeController@store')->name('leavetype.add');
         Route::post('delete-leavetype', 'LeaveTypeController@delete')->name('leavetype.delete');

         Route::get('approver', 'ApproverController@report')->name('approver.maintenance');
         Route::post('add-approver', 'ApproverController@store')->name('approver.add');
         Route::post('delete-approver', 'ApproverController@delete')->name('approver.delete');
    });



    /* Work Schedule */

        Route::group(['prefix'=>'work-schedule'],function(){
            Route::get('list/', 'FileMaintenance\WorkscheduleController@index')->name('Work Schedule');
            Route::get('{id?}', 'FileMaintenance\WorkscheduleController@scheduleDetails')->name('schedule details');
            //Datatable
            Route::post('fetch-work-schedule', 'Api\DatatablesController@workSchedule')->name('fetch-work-schedule');
            //Fetch Employees
            Route::get('fetch-employees/{sched_type?}', 'Api\FileMaintenance\WorkScheduleController@getEmployees')->name('work-sched-fetch-emp');
            //Update employee schedule
            Route::post('emp-update-sched', 'Api\FileMaintenance\WorkScheduleController@updateEmpSchedule');

            //update / create work schedule
            Route::post('update-create-sched', 'Api\FileMaintenance\WorkScheduleController@updateOrcreate');
        });

    /* End Work Schedule */

    /***************PERSONNEL INFORMATION******************/



        /*++++EMPLOYEE MASTER LIST++++*/

            /***************API ******************/
            Route::post('test', 'Api\PersonnelInformation\EmployeeDetailsController@test');
            Route::post('employee-details', 'Api\PersonnelInformation\EmployeeDetailsController@storeEmployeeDetails');
            Route::post('get-employee-details', 'Api\PersonnelInformation\EmployeeDetailsController@getEmployeeDetails');
            Route::post('get-employee-info', 'Api\PersonnelInformation\EmployeeDetailsController@getEmployeeInfo');
            /***************END ***************/

            /***************VIEW ***************/
            Route::group(['prefix'=>'employee-master-list'], function() {
                Route::get('', 'PersonnelInformation\EmployeesController@index')->name('Employee Masterlist');
                Route::post('fetch-employees', 'Api\DatatablesController@employees')->name('employee.masterlist');
            });

            Route::get('employee-details/{employee_number?}', 'PersonnelInformation\EmployeeDetailsController@viewEmployee')->name('employee-details.index');
            /***************END***************/

        /*++++END EMPLOYEE MASTER LIST++++*/




    /***************END PERSONNEL INFORMATION******************/


    /***************MANPOWER REQUEST******************/



        /*++++APPLICANTS PROFILING++++*/

            /***************API ******************/
            Route::post('applicant-details', 'Api\ManpowerRequest\ApplicantsProfilingController@storeApplicantDetails');
            Route::post('get-applicant-details', 'Api\ManpowerRequest\ApplicantsProfilingController@getApplicantDetails');
            Route::post('get-applicant-info', 'Api\ManpowerRequest\ApplicantsProfilingController@getApplicantInfo');
            /***************END ***************/

            /***************VIEW ***************/
            Route::group(['prefix'=>'applicants-profiling'], function() {
                Route::get('', 'ManpowerRequest\ApplicantsProfilingController@index')->name('Applicants Profiling');
                Route::post('fetch-applicants', 'Api\DatatablesController@applicants')->name('applicants.masterlist');
            });

            Route::get('applicant-details/{applicant_number?}', 'ManpowerRequest\ApplicantsProfilingController@viewApplicant')->name('applicant-details.index');
            /***************END***************/

        /*++++END APPLICANTS PROFILING++++*/



    /***************END MANPOWER REQUEST******************/

});



    