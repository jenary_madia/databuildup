<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PayClasses extends Model
{
    protected $table = 'pay_classes';
    protected $guarded = ['class_id'];
}
