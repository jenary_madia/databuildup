<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Modules extends Model
{
    protected $table = 'modules';
    protected $primaryKey = 'module_id';
    public $incrementing = true;
    protected $guarded = [];
}
