<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employees extends Model
{
    protected $table = 'employees';
    protected $primaryKey = 'emp_id';
    public $incrementing = true;
    protected $guarded = [];

    public function generateEmployeeNumber()
    {
        $documentCodeToday = date('Y-m');
        $latestDateCreated = $this->max('created_at');
        if (date("Y-m", strtotime($latestDateCreated)) >= $documentCodeToday)
        {
            $latestCodenumber = $this->where('created_at','=',$latestDateCreated)->max('emp_number');
            if (! $latestCodenumber) {
                return date("Y-m", strtotime($latestDateCreated))."-".'00000';
            }else{
                $latestCodenumber = explode('-',$latestCodenumber)[2];
                return date("Y-m", strtotime($latestDateCreated))."-".str_pad($latestCodenumber + 1, 5, '0', STR_PAD_LEFT);
            }
        }else{

            return $documentCodeToday."-".str_pad('00000' + 1, 5, '0', STR_PAD_LEFT);

        }

    }

    public function generateApplicantNumber()
    {
        $documentCodeToday = date('Y-m');
        $latestDateCreated = $this->max('created_at');
        if (date("Y-m", strtotime($latestDateCreated)) >= $documentCodeToday)
        {
            $latestCodenumber = $this->where('created_at','=',$latestDateCreated)->max('emp_applicant_number');
            if (! $latestCodenumber) {
                return date("Y-m", strtotime($latestDateCreated))."-".'00000';
            }else{
                $latestCodenumber = explode('-',$latestCodenumber)[2];
                return date("Y-m", strtotime($latestDateCreated))."-".str_pad($latestCodenumber + 1, 5, '0', STR_PAD_LEFT);
            }
        }else{

            return $documentCodeToday."-".str_pad('00000' + 1, 5, '0', STR_PAD_LEFT);

        }

    }

}
