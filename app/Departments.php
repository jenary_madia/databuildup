<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Departments extends Model
{
    protected $table = 'departments_clients';
    protected $guarded = ['dept_id'];
}
