<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Applicants extends Model
{

    protected $table = 'applicants';
    protected $primaryKey = 'app_id';
    public $incrementing = true;
    protected $guarded = [];

    
}
