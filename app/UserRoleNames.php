<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserRoleNames extends Model
{
    protected $table = 'user_role_names';
    protected $primaryKey = 'user_role_name_id';
    public $incrementing = true;
    protected $guarded = [];
}
