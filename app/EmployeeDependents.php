<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmployeeDependents extends Model
{
    protected $table = 'dependents';
    protected $primaryKey = 'dep_id';
    protected $guarded = [];
}
