<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmployeePersonalInfo extends Model
{
    protected $table = 'personal_informations';
    protected $primaryKey = 'info_id';
    protected $guarded = [];
    public $timestamps = false;
}
