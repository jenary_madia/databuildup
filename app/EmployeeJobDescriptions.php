<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmployeeJobDescriptions extends Model
{
    protected $table = 'job_descriptions';
    protected $primaryKey = 'jd_id';
    protected $guarded = [];
    public $timestamps = false;
}
