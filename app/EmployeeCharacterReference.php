<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmployeeCharacterReference extends Model
{
    protected $table = 'character_references';
    protected $primaryKey = 'ref_id';
    protected $guarded = [];
}
