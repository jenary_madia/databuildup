<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmployeeEmploymentHistory extends Model
{
    protected $table = 'histories';
    protected $primaryKey = 'history_id';
    protected $guarded = [];
    public $timestamps = false;
}
