<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use DB;

class Approver extends Authenticatable
{
      public $timestamps = false;
        protected $guarded = array();

       public static function employee()
       {
       		$rec = DB::table("employees")->get();
       		$emp = array();
       		foreach($rec as $rs)
       		{
       			$emp[$rs->emp_id] = $rs->emp_firstname . " ". $rs->emp_lastname;
       		}

       		return $emp;
       }

       public static function department()
       {
       		$rec = DB::table("departments_clients")
       			->where("active",1)
       			->get();
       		$emp = array();
       		foreach($rec as $rs)
       		{
       			$emp[$rs->dept_id] = $rs->dept_name;
       		}

       		return $emp;
       }
}
