<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobPositions extends Model
{
    protected $table = 'job_positions';
    protected $guarded = ['job_id'];
}
