<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubModules extends Model
{
    protected $table = 'sub_modules';
    protected $primaryKey = 'sub_module_id';
    public $incrementing = true;
    protected $guarded = [];

    public function getModuleId()
    {
    	return $this->belongsTo('App\Modules', 'module_id');
    }
}
