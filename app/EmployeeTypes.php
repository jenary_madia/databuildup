<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmployeeTypes extends Model
{
    protected $table = 'employee_types';
    protected $guarded = ['type_id'];
}
