<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmployeeCompensationBenefits extends Model
{
    protected $table = 'compensations';
    protected $primaryKey = 'comp_id';
    protected $guarded = [];
    public $timestamps = false;
}
