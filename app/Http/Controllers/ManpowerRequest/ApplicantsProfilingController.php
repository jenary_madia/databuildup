<?php

namespace App\Http\Controllers\ManpowerRequest;

use App\Departments;
use App\Employees;
use App\EmployeeTypes;
use App\JobPositions;
use App\PayClasses;
use App\Http\Controllers\Controller;

class ApplicantsProfilingController extends Controller
{
    private $tab;
    private $module;
    protected $employees;
    protected $departments;
    protected $employeeTypes;
    protected $payClasses;
    protected $jobPositions;
    public function __construct(Employees $employees,Departments $departments,EmployeeTypes $employeeTypes,PayClasses $payClasses,JobPositions $jobPositions)
    {
        $this->tab = "Manpower Request";
        $this->module = "Employees Profiling";
        $this->applicants = $employees;
        $this->departments = $departments;
        $this->employeeTypes = $employeeTypes;
        $this->payClasses = $payClasses;
        $this->jobPositions = $jobPositions;
    }

    public function index() {
    	$data = [
    		'tab' => $this->tab,
    		'module' => $this->module
    	];
        return view("manpower_request.applicants-profiling.index",$data);
    }

    public function viewApplicant($applicant_number = null) {
        $tab = $this->tab;
        $module = $this->module;
        $app_number = $applicant_number ? $applicant_number : $this->applicants->generateApplicantNumber();
        // TODO!! validation (check if employee_number is existing)
        $departments = $this->departments->where('active',1)->select(['dept_id','dept_name'])->get();
        $employee_types = $this->employeeTypes->where('active',1)->select(['type_id','type_title'])->get();
        $pay_classes = $this->payClasses->where('active',1)->select(['class_id','class_title'])->get();
        $job_positions = $this->jobPositions->where('active',1)->select(['job_id','job_title'])->get();
        return view('manpower_request.applicants-profiling.applicant-details',compact("app_number","departments","employee_types","pay_classes","job_positions","tab","module"));
    }

}
