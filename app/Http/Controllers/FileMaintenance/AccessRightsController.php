<?php

namespace App\Http\Controllers\FileMaintenance;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

use App\Modules;
use App\SubModules;

class AccessRightsController extends Controller
{   

    private $tab;
    private $module;

    public function __construct() {
        $this->tab = "File Maintenance";
        $this->module = "Accecss Rights";
    }

   public function userRoles()
   {
        $data = [
            'tab' => $this->tab,
            'module' => $this->module,
            'tableName' => 'table_modules',
            'tableHead' => 'Modules / Sub Modules',
            'modules' => Modules::where('active', 1)->get(),
            'sub_modules' => SubModules::where('active', 1)->get()
        ];

        return view('file_maintenance.access_rights.user_roles', $data);
   }
}
