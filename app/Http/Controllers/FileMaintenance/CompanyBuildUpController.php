<?php

namespace App\Http\Controllers\FileMaintenance;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Companies;

class CompanyBuildUpController extends Controller
{   

    private $tab;
    private $module;

    public function __construct() {
        $this->tab = "File Maintenance";
        $this->module = "Company Build Up";
    }

    public function companyProfile() {
        $companyDetails = Companies::where('comp_id',Auth::user()->comp_id)->first();
        $data = [
            'tab' => $this->tab,
            'module' => $this->module,
            "companyDetails" => $companyDetails
        ];
        return view('file_maintenance.company_build_up.company_profile',$data);
    }

    public function departments() {
        $data = [
            'tab' => $this->tab,
            'module' => $this->module,
            'tab' => "File Maintenance",
            'module' => "Company Build Up",
            'tableName' => 'table_departments',
            'tableHead' => 'Department / Client Name',
            'buildupID' => 1
        ];
        return view('file_maintenance.company_build_up.list-table',$data);
    }

    public function jobPositions() {
        $data = [
            'tab' => $this->tab,
            'module' => $this->module,
            'tableName' => 'table_positions',
            'tableHead' => 'Job Title / Position',
            'buildupID' => 2,
        ];
        return view('file_maintenance.company_build_up.list-table', $data);
    }

    public function payClass() {
        $data = [
            'tab' => $this->tab,
            'module' => $this->module,
            'tableName' => 'table_classes',
            'tableHead' => 'Pay Class',
            'buildupID' => 3
        ];
        return view('file_maintenance.company_build_up.list-table', $data);
    }

    public function employeTypes() {
        $data = [
            'tab' => $this->tab,
            'module' => $this->module,
            'tableName' => 'table_types',
            'tableHead' => 'Employee Type',
            'buildupID' => 4
        ];
        return view('file_maintenance.company_build_up.list-table', $data);
    }
}
