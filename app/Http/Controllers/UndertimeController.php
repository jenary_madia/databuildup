<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Undertime_table;
use Validator;
use Response;
use Redirect;

class UndertimeController extends Controller
{
    public function report() 
    {
    	$data["report"] = Undertime_table::get();
        $data['module'] = "Time Keeping Table";
        $data['tab'] = "File Maintenance";  

        return view('tables.undertime' ,$data );
    }

     public function store(Request $request) 
     {
        $rules = ['und_from' => 'required','und_to' => 'required','und_minutes' => 'required'];
        $params = [
            'und_from' =>  $request->und_from,
            'und_to' => $request->und_to,
            'und_minutes' => $request->und_minutes
        ];

 
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return Response::json(array(
                'success' => false,
                'errors' => $validator->getMessageBag()->toArray(),
                'message' => 'Some fields are incomplete',
                200
            ));
        }

        if($request->und_id)
        {
            Undertime_table::where("und_id",$request->und_id)->update($params);
        }
        else
        {
            Undertime_table::create($params);
        }
        

        $report = Undertime_table::get();
        $data = array();
        foreach($report as $index=>$rs)
        {
        	$data[$index][0] = "<input type='checkbox' class='und_id' name='und_id[]' value='$rs->und_id' />";
            $data[$index][1]  =  ($rs->und_from) . ($rs->und_from > 1 ?  " minutes" : " minute") ;
        	$data[$index][2]  =  ($rs->und_to) .  ($rs->und_to > 1 ?  " minutes" : " minute");
        	$data[$index][3]  =  ($rs->und_minutes) .  ($rs->und_minutes > 1 ?   " minutes" : " minute") ;
        }

        return Response::json(array(
            'success' => true,
            'message' => $request->und_id ? "Undertime successfully updated" : "Undertime successfully added",
            'data'=>$data,
            200
        ));
    }

  	 public function delete(Request $request) 
     {
        $rules = ['und_id' => 'required'];
   
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails())
		{
        	return Redirect::to('file-maintenance/undertime')
		          	 ->with('errorMessage', "At least one undertime is required to proceed transaction.");
		}
		else
		{
			Undertime_table::whereIn("und_id",$request->und_id)->delete();

			return Redirect::to('file-maintenance/undertime')
		          	 ->with('successMessage', "Successfully Deleted undertime.");
		}
        
    }
}
