<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Overtime_table;
use Validator;
use Response;
use Redirect;

class OvertimeController extends Controller
{
    public function report() 
    {
    	$data["report"] = Overtime_table::get();
        $data['module'] = "Time Keeping Table";
        $data['tab'] = "File Maintenance";  

        return view('tables.overtime' ,$data );
    }

     public function store(Request $request) 
     {
        $rules = ['ovt_from' => 'required','ovt_to' => 'required','ovt_minutes' => 'required'];
        $params = [
            'ovt_from' =>  $request->ovt_from,
            'ovt_to' => $request->ovt_to,
            'ovt_minutes' => $request->ovt_minutes
        ];

 
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return Response::json(array(
                'success' => false,
                'errors' => $validator->getMessageBag()->toArray(),
                'message' => 'Some fields are incomplete',
                200
            ));
        }

        if($request->ovt_id)
        {
            Overtime_table::where("ovt_id",$request->ovt_id)->update($params);
        }
        else
        {
            Overtime_table::create($params);
        }
        

        $report = Overtime_table::get();
        $data = array();
        foreach($report as $index=>$rs)
        {
        	$data[$index][0] = "<input type='checkbox' class='ovt_id' name='ovt_id[]' value='$rs->ovt_id' />";
            $data[$index][1]  =  ($rs->ovt_from) . ($rs->ovt_from > 1 ?  " minutes" : " minute") ;
        	$data[$index][2]  =  ($rs->ovt_to) .  ($rs->ovt_to > 1 ?  " minutes" : " minute");
        	$data[$index][3]  =  ($rs->ovt_minutes) .  ($rs->ovt_minutes > 1 ?   " minutes" : " minute") ;
        }

        return Response::json(array(
            'success' => true,
            'message' => $request->ovt_id ? "Overtime successfully updated" : "Overtime successfully added",
            'data'=>$data,
            200
        ));
    }

  	 public function delete(Request $request) 
     {
        $rules = ['ovt_id' => 'required'];
   
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails())
		{
        	return Redirect::to('file-maintenance/overtime')
		          	 ->with('errorMessage', "At least one overtime is required to proceed transaction.");
		}
		else
		{
			Overtime_table::whereIn("ovt_id",$request->ovt_id)->delete();

			return Redirect::to('file-maintenance/overtime')
		          	 ->with('successMessage', "Successfully Deleted overtime.");
		}
        
    }
}
