<?php

namespace App\Http\Controllers\Api\PersonnelInformation;

use App\EmployeeCharacterReference;
use App\EmployeeCompensationBenefits;
use App\EmployeeDependents;
use App\EmployeeEmploymentHistory;
use App\EmployeeJobDescriptions;
use App\EmployeeSkills;
use DB;
use App\Employees;
use App\EmployeePersonalInfo;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\employeeDetails;

class EmployeeDetailsController extends Controller
{
    public function test(Request $request) {
        return $request->photo->storeAs('images', 'filename.jpg');
    }

    /**
     * @param Request $request
     */
    public function storeEmployeeDetails(employeeDetails $request) {
        $filter_params = ["emp_number" => $request->employee_details['employee_number']];
        $employee_details = [
            "emp_firstname" => $request->employee_details['firstname'],
            "emp_middlename" => $request->employee_details['middlename'],
            "emp_lastname" => $request->employee_details['lastname'],
            "emp_image" => $request->employee_details['photo'],
            "emp_dept_id" => $request->employee_details['department'],
            "emp_position_id" => $request->employee_details['position'],
            "emp_class_id" => $request->employee_details['payclass'],
            "emp_type_id" => $request->employee_details['employee_type'],
            "emp_hiring_date" => $request->employee_details['hiring_date'],
            "emp_contract_from" => $request->employee_details['contract_duration_from'],
            "emp_contract_to" => $request->employee_details['contract_duration_to'],
            "emp_work_sched_id" => $request->employee_details['work_schedule'],
            "emp_status" => $request->employee_details['employee_status'],
            "emp_monthly_rate" => $request->employee_details['monthly_rate'],
            "emp_semi_monthly_rate" => $request->employee_details['semi_monthly_rate'],
            "emp_daily_rate" => $request->employee_details['daily_rate'],
            "emp_hourly_rate" => $request->employee_details['hourly_rate'],
            "is_employee" => 1,
        ];
        DB::beginTransaction();
        try {
            $employee_id = Employees::updateOrCreate($filter_params, $employee_details)->emp_id;
            $data_to_save = $request->data_for_update;
            $to_update = $request->to_update;
            switch ($to_update) {
                case "personalInformation";
                    $params = [
                        "info_birth_date" => $request->data_for_update['birthdate'],
                        "info_birth_place" => $request->data_for_update['birthplace'],
                        "info_civil_status" => $request->data_for_update['civil_status'],
                        "info_contact_no" => $request->data_for_update['contact_number'],
                        "info_email" => $request->data_for_update['email_address'],
                        "info_gender" => $request->data_for_update['gender'],
                        "info_height" => $request->data_for_update['height'],
                        "info_language" => $request->data_for_update['language_spoken'],
                        "info_nationality" => $request->data_for_update['nationality'],
                        "info_pagibig" => $request->data_for_update['pagibig'],
                        "info_philhealth" => $request->data_for_update['philhealth'],
                        "info_religion" => $request->data_for_update['religion'],
                        "info_sss" => $request->data_for_update['sss'],
                        "info_tin" => $request->data_for_update['tin'],
                        "info_weight" => $request->data_for_update['weight'],
                        "info_present_street" => $request->data_for_update['present_address']['street'],
                        "info_present_municipal" => $request->data_for_update['present_address']['municipal'],
                        "info_present_province" => $request->data_for_update['present_address']['province'],
                        "info_present_zipcode" => $request->data_for_update['present_address']['zipcode'],
                        "info_perma_street" => $request->data_for_update['permanent_address']['street'],
                        "info_perma_municipal" => $request->data_for_update['permanent_address']['municipal'],
                        "info_perma_province" => $request->data_for_update['permanent_address']['province'],
                        "info_perma_zipcode" => $request->data_for_update['permanent_address']['zipcode'],
                        "info_cperson_name" => $request->data_for_update['contact_person']['name'],
                        "info_cperson_relationship" => $request->data_for_update['contact_person']['relationship'],
                        "info_cperson_address" => $request->data_for_update['contact_person']['address'],
                        "info_cperson_no" => $request->data_for_update['contact_person']['number']
                    ];
                    EmployeePersonalInfo::updateOrCreate(['info_employee_id' => $employee_id], $params);
                    break;
                case "characterReference";
                    for ($i = 0; $i < count($data_to_save); $i++) {
                        $data_to_save[$i]['ref_employee_id'] = $employee_id;
                    }
                    EmployeeCharacterReference::where("ref_employee_id",$employee_id)->delete();
                    EmployeeCharacterReference::insert($data_to_save);
                    break;
                case "compensationBenefits";
                    EmployeeCompensationBenefits::updateOrCreate(['comp_employee_id' => $employee_id],$data_to_save);
                    break;
                case "dependents";
                    for ($i = 0; $i < count($data_to_save); $i++) {
                        $data_to_save[$i]['dep_employee_id'] = $employee_id;
                    }
                    EmployeeDependents::where("dep_employee_id",$employee_id)->delete();
                    EmployeeDependents::insert($data_to_save);
                    break;
                case "jobDescription";
                    $responsibilities = $data_to_save["responsibilities"];
                    unset($data_to_save["responsibilities"]);
                    $job_description_id = EmployeeJobDescriptions::updateOrCreate(['jd_employee_id' => $employee_id],$data_to_save)->jd_id;
                    for ($i = 0; $i < count($responsibilities); $i++) {
                        $responsibilities[$i]['job_description_id'] = $job_description_id;
                    }
                    DB::table("job_responsibilities")->where('job_description_id',$job_description_id)->delete();
                    DB::table('job_responsibilities')->insert($responsibilities);
                    break;
                case "employmentHistory";
                    for ($i = 0; $i < count($data_to_save); $i++) {
                        $data_to_save[$i]['history_employee_id'] = $employee_id;
                    }
                    EmployeeEmploymentHistory::where('history_employee_id',$employee_id)->delete();
                    EmployeeEmploymentHistory::insert($data_to_save);
                    break;
                case "educationalBackground";
                    for ($i = 0; $i < count($data_to_save); $i++) {
                        $data_to_save[$i]['educ_employee_id'] = $employee_id;
                    }
                    DB::table("educational_background")->where('educ_employee_id',$employee_id)->delete();
                    DB::table('educational_background')->insert($data_to_save);
                    break;
                case "familyBackground";
                    for ($i = 0; $i < count($data_to_save); $i++) {
                        $data_to_save[$i]['fam_employee_id'] = $employee_id;
                    }
                    DB::table("family_background")->where('fam_employee_id',$employee_id)->delete();
                    DB::table('family_background')->insert($data_to_save);
                    break;
                case "requirements";
                    $other_requirements = $data_to_save["other"];
                    $primary = [];
                    for ($i = 0; $i < count($other_requirements); $i++) {
                        $other_requirements[$i]['req_employee_id'] = $employee_id;
                        $other_requirements[$i]['req_name'] = $other_requirements[$i]['req_name'];
                        $other_requirements[$i]['req_type'] = 2;
                    }
                    for ($i = 0; $i < count($data_to_save["primary"]); $i++) {
                        array_push($primary,[
                            "req_name" => $data_to_save["primary"][$i],
                            'req_employee_id' => $employee_id,
                            "req_type" => 1
                        ]);
                    }
                    $data_to_save = array_merge($primary,$other_requirements);
                    DB::table("requirements")->where('req_employee_id',$employee_id)->delete();
                    DB::table('requirements')->insert($data_to_save);
                    break;
                case "trainings";
                    for ($i = 0; $i < count($data_to_save); $i++) {
                        $data_to_save[$i]['training_employee_id'] = $employee_id;
                    }
                    DB::table("trainings")->where('training_employee_id',$employee_id)->delete();
                    DB::table('trainings')->insert($data_to_save);
                    break;
                case "skills";
                    $eligibities = $data_to_save["eligibilities"];
                    unset($data_to_save["eligibilities"]);
                    $skill_id = EmployeeSkills::updateOrCreate(['skill_employee_id' => $employee_id],$data_to_save)->skill_id;
                    for ($i = 0; $i < count($eligibities); $i++) {
                        $eligibities[$i]['eligibility_skill_id'] = $skill_id;
                    }
                    DB::table("skill_eligibilities")->where('eligibility_skill_id',$skill_id)->delete();
                    DB::table('skill_eligibilities')->insert($eligibities);
                    break;
            }
            DB::commit();
            return [
                'success' => true,
                'message' => "Details successfully updated",
            ];
        }catch (\Exception $e) {
            DB::rollBack();
            return [
                'success' => false,
                'message' => $e->getMessage(),
            ];
        }
    }

    public function getEmployeeDetails(Request $request) {
        $employee_details = DB::table("employees")
            ->where("emp_number",$request->employee_number)
            ->first();
        return response()->json($employee_details);
    }

    public function getEmployeeInfo(Request $request) {
        $employee = DB::table("employees")
            ->where("emp_number",$request->employee_number)
            ->first();
        if($employee) {
            $employee_id = $employee->emp_id;
            switch ($request->to_get) {
                case "personalInformation";
                    return EmployeePersonalInfo::where("info_employee_id",$employee_id)->first();
                    break;
                case "compensationBenefits";
                    return EmployeeCompensationBenefits::where("comp_employee_id",$employee_id)->first();
                    break;
                case "characterReference";
                    return EmployeeCharacterReference::where("ref_employee_id",$employee_id)->get();
                    break;
                case "dependents";
                    return EmployeeDependents::where("dep_employee_id",$employee_id)->get();
                    break;
                case "jobDescription";
                    $jobDesc = EmployeeJobDescriptions::where("jd_employee_id",$employee_id)->first();
                    if($jobDesc) {
                        $jd_id = $jobDesc->jd_id;
                        $responsibilities = DB::table("job_responsibilities")->where("job_description_id",$jd_id)->get();
                        $jobDesc['responsibilities'] = $responsibilities;
                        unset($jobDesc["jd_employee_id"]);
                        unset($jobDesc["jd_id"]);
                        return $jobDesc;
                    }
                    break;
                case "employmentHistory";
                    return EmployeeEmploymentHistory::where('history_employee_id',$employee_id)->get();
                    break;
                case "educationalBackground";
                    return DB::table("educational_background")->where('educ_employee_id',$employee_id)->get();
                    break;
                case "familyBackground";
                    return DB::table("family_background")->where('fam_employee_id',$employee_id)->get();
                    break;
                case "requirements";
                    return DB::table("requirements")->where('req_employee_id',$employee_id)->get();
                    break;
                case "trainings";
                    return DB::table("trainings")->where('training_employee_id',$employee_id)->get();
                    break;
                case "skills";
                    $skills = EmployeeSkills::where("skill_employee_id",$employee_id)->first();
                    if($skills) {
                        $skill_id = $skills->skill_id;
                        $eligibilities = DB::table("skill_eligibilities")->where("eligibility_skill_id",$skill_id)->get();
                        $skills['eligibilities'] = $eligibilities;
                        unset($skills["skill_employee_id"]);
                        unset($skills["skill_id"]);
                        return $skills;
                    }
                    break;
            }
        }
    }
}
