<?php

namespace App\Http\Controllers\Api\FileMaintenance;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;

use Auth;
use Validator;
use Response;

use DB;

use App\Modules;
use App\SubModules;

use App\UserRoles;
use App\UserRoleNames;

class AccessRightsController extends Controller
{
    public function store(Request $request)
    {

    	$params['comp_id'] = Auth::user()->comp_id;
        $validator = Validator::make($request->all(), [
        	'role_name' => 'required|unique:user_role_names,user_role_name',
        	'modules' => 'required'
        ]);

        if ($validator->fails())
            return Response::json(array(
                'success' => false,
                'errors' => $validator->getMessageBag()->toArray(),
                'message' => 'Some fields are incomplete',
                200
            ));

    	DB::beginTransaction();

		$user_role_name = new UserRoleNames();

		$user_role_name->user_role_name = $request['role_name'];
		$user_role_name->save();

		if (isset($request['sub_modules']))
	    	foreach ($request['sub_modules'] as $sub_module)
	    	{
	    		$user_role = new UserRoles();
	    		$user_role->user_role_name_id = $user_role_name->user_role_name_id;
	    		$user_role->module_id = SubModules::where('sub_module_id', $sub_module)->first()['module_id'];
	    		$user_role->sub_module_id = $sub_module;
	    		$user_role->save();
	    	}

    	DB::commit();

        return Response::json(array(
            'success' => true,
            'message' => 'Success',
            200
        ));
    }

    public function fetch()
    {
        $result = UserRoleNames::get();
        $result_data["iTotalDisplayRecords"] = count($result);
        $result_data["iTotalRecords"] = count($result);
        if ( count($result) > 0){
            $ctr = 0;
            foreach($result as $req) {
                $result_data["aaData"][$ctr][] = $req['user_role_name'];
                $result_data["aaData"][$ctr][] = '<button class="btn btn-warning btn-xs btn_delete_data">Edit</button> <button class="btn btn-danger btn-xs btn_delete_data">Delete</button>';
                $ctr++;
            }
        }
        else {
            $result_data["aaData"] = $result;
        }

        return $result_data;
    }
}
