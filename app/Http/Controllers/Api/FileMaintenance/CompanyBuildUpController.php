<?php

namespace App\Http\Controllers\Api\FileMaintenance;

use App\Companies;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Validator;
use Response;
use App\Departments;
use App\JobPositions;
use App\PayClasses;
use App\EmployeeTypes;
use Auth;
use App\Http\Requests\CompanyProfileRequest;

class CompanyBuildUpController extends Controller
{
    public function store(Request $request) {
        switch($request->buildup_id) {
            case 1; // Department
                $rules = [
                    'to_add' => 'required|unique:departments_clients,dept_name|max:50'
                ];
                $model = new Departments();
                $params = [
                    'dept_name' => $request->to_add,
                ];
                $successMessage = 'Department / Client successfully added';
                break;
            case 2; // Job position
                $rules = ['to_add' => 'required|unique:job_positions,job_title|max:50'];
                $model = new JobPositions();
                $params = [
                    'job_title' => $request->to_add,
                ];
                $successMessage = 'Job Title / Position successfully added';
                break;
            case 3; // Pay Class
                $rules = ['to_add' => 'required|unique:pay_classes,class_title|max:50'];
                $model = new PayClasses();
                $params = [
                    'class_title' => $request->to_add,
                ];
                $successMessage = 'Pay Class successfully added';
                break;
            case 4; // Employee type
                $rules = ['to_add' => 'required|unique:employee_types,type_title|max:50'];
                $model = new EmployeeTypes();
                $params = [
                    'type_title' => $request->to_add,
                ];
                $successMessage = 'Employee Type successfully added';
                break;
        }

        $params['comp_id'] = Auth::user()->comp_id;
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return Response::json(array(
                'success' => false,
                'errors' => $validator->getMessageBag()->toArray(),
                'message' => 'Some fields are incomplete',
                200
            ));
        }

        $model->create($params);
        return Response::json(array(
            'success' => true,
            'message' => $successMessage,
            200
        ));
    }

    public function fetch($id) {
        switch ($id) {
            case 1;
                $model = new Departments();
                $columnToFetch = 'dept_name';
                $orderBy = 'dept_id';
                break;
            case 2;
                $model = new JobPositions();
                $columnToFetch = 'job_title';
                $orderBy = 'job_id';
                break;
            case 3;
                $model = new PayClasses();
                $columnToFetch = 'class_title';
                $orderBy = 'class_id';
                break;
            case 4;
                $model = new EmployeeTypes();
                $columnToFetch = 'type_title';
                $orderBy = 'type_id';
                break;
        }

        $result = $model->where('comp_id',Auth::user()->comp_id)
            ->orderBy($orderBy, 'desc')
            ->get();
        $result_data["iTotalDisplayRecords"] = count($result);
        $result_data["iTotalRecords"] = count($result);
        if ( count($result) > 0){
            $ctr = 0;
            foreach($result as $req) {
                $result_data["aaData"][$ctr][] = $req[$columnToFetch];
                $result_data["aaData"][$ctr][] = '<button class="btn btn-danger btn-xs btn_delete_data" value="'.$req[$orderBy].'">delete</button>';
                $ctr++;
            }
        }
        else {
            $result_data["aaData"] = $result;
        }
        return $result_data;
    }

    public function remove(Request $request) {
        switch($request->buildup_id) {
            case 1; // Department
                $rules = [
                    'to_remove' => 'required|unique:departments_clients,dept_name|max:50'
                ];
                $model = Departments::where('dept_id',$request->to_remove);
                $successMessage = 'Department / Client successfully deleted';
                break;
            case 2; // Job position
                $rules = ['to_remove' => 'required|unique:job_positions,job_title|max:50'];
                $model = JobPositions::where('job_id',$request->to_remove);
                $successMessage = 'Job Title / Position successfully deleted';
                break;
            case 3; // Pay Class
                $rules = ['to_remove' => 'required|unique:pay_classes,class_title|max:50'];
                $model = PayClasses::where('class_id',$request->to_remove);;
                $successMessage = 'Pay Class successfully deleted';
                break;
            case 4; // Employee type
                $rules = ['to_remove' => 'required|unique:employee_types,type_title|max:50'];
                $model = EmployeeTypes::where('type_id',$request->to_remove);
                $successMessage = 'Employee Type successfully deleted';
                break;
        }

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return Response::json(array(
                'success' => false,
                'errors' => $validator->getMessageBag()->toArray(),
                'message' => 'Some fields are incomplete',
                200
            ));
        }
        $model->where('comp_id',Auth::user()->comp_id)->delete();
        return Response::json(array(
            'success' => true,
            'message' => $successMessage,
            200
        ));
    }

    public function updateCompDetails(CompanyProfileRequest $request) {
        $compID = Auth::user()->comp_id;
        if ($request->hasFile('company_logo')) {
            $request->company_logo->storeAs("images/company/$compID","company-logo.png");
        }
        $company = Companies::where('comp_id',$compID);
        $parameters = [
            'comp_name' => $request->company_name,
            'comp_industry' => $request->industry,
            'comp_logo' => 'company-logo.png',
            'comp_address' => $request->address,
            'comp_zipcode' => $request->zipcode,
            'comp_city' => $request->city,
            'comp_province' => $request->province,
            'comp_telephone_number' => $request->tel_number,
            'comp_mobile_number' => $request->mobile_number,
            'comp_fax_number' => $request->fax_number,
            'comp_contact_person' => $request->contact_person,
            'comp_email' => $request->email_address,
            'comp_website_url' => $request->website_url,
            'comp_tin' => $request->tax_number
        ];
        $company->update($parameters);

        return Redirect::back();
    }
}
