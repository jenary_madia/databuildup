<?php

namespace App\Http\Controllers\Api;

use App\Employees;
use Illuminate\Http\Request;
use DB;
use App\Http\Controllers\Controller;

class DatatablesController extends Controller
{
    public function employees() {
        $result = DB::table('employees')
            ->join('departments_clients', 'employees.emp_dept_id', '=', 'departments_clients.dept_id')
            ->join('job_positions', 'employees.emp_position_id', '=', 'job_positions.job_id')
            ->join('employee_types', 'employees.emp_type_id', '=', 'employee_types.type_id')
            ->where('employees.is_employee',1)
            ->select(DB::raw("concat(employees.emp_firstname,employees.emp_middlename,employees.emp_lastname) as fullname"),"employees.emp_number","employees.emp_hiring_date","employees.emp_status","departments_clients.dept_name","job_positions.job_title","employee_types.type_title")
            ->get();
        $result_data["iTotalDisplayRecords"] = count($result);
        $result_data["iTotalRecords"] = count($result);
        if ( count($result) > 0){
            $ctr = 0;
            foreach($result as $req) {
                $result_data["aaData"][$ctr][] = "<input type='checkbox' class='emp_number' value='$req->emp_number'>";
                $result_data["aaData"][$ctr][] = $req->fullname;
                $result_data["aaData"][$ctr][] = $req->emp_number;
                $result_data["aaData"][$ctr][] = $req->dept_name;
                $result_data["aaData"][$ctr][] = $req->job_title;
                $result_data["aaData"][$ctr][] = $req->emp_hiring_date;
                $result_data["aaData"][$ctr][] = $req->emp_status ? "Active" : "Inactive";
                $result_data["aaData"][$ctr][] = $req->type_title;
                $ctr++;
            }
        }
        else {
            $result_data["aaData"] = $result;
        }
        return $result_data;
    }

    public function applicants() {
        $result = DB::table('employees')
            ->join('departments_clients', 'employees.emp_dept_id', '=', 'departments_clients.dept_id')
            ->join('applicants','employees.emp_id','=','applicants.app_employee_id')
            ->join('job_positions', 'applicants.app_position', '=', 'job_positions.job_id')
            ->where('employees.is_applicant',1)
            ->select(DB::raw("concat(employees.emp_firstname,employees.emp_middlename,employees.emp_lastname) as fullname"),"employees.emp_applicant_number","applicants.app_date","applicants.app_status","departments_clients.dept_name","job_positions.job_title","applicants.app_desired_salary")
            ->get();
        $result_data["iTotalDisplayRecords"] = count($result);
        $result_data["iTotalRecords"] = count($result);
        if ( count($result) > 0){
            $ctr = 0;
            foreach($result as $req) {
                $result_data["aaData"][$ctr][] = "<input type='checkbox' class='emp_applicant_number' value='$req->emp_applicant_number'>";
                $result_data["aaData"][$ctr][] = $req->fullname;
                $result_data["aaData"][$ctr][] = $req->emp_applicant_number;
                $result_data["aaData"][$ctr][] = $req->job_title;
                $result_data["aaData"][$ctr][] = $req->app_date;
                $result_data["aaData"][$ctr][] = $req->app_desired_salary;
                $result_data["aaData"][$ctr][] = $req->dept_name;
                $result_data["aaData"][$ctr][] = $req->app_status;
                $ctr++;
            }
        }
        else {
            $result_data["aaData"] = $result;
        }
        return $result_data;
    }
}
