<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Philhealth_table;
use Validator;
use Response;
use Redirect;

class PhilhealthController extends Controller
{
    public function report() 
    {
    	$data["report"] = Philhealth_table::get();
        $data['module'] = "Payroll Set Up";
        $data['tab'] = "File Maintenance";

        return view('tables.philhealth' ,$data );
    }

     public function store(Request $request) 
     {
        $rules = ['phl_from' => 'required','phl_to' => 'required','phl_employer' => 'required','phl_employee' => 'required'];
        $params = [
            'phl_from' =>  $request->phl_from,
            'phl_to' => $request->phl_to,
            'phl_employer' => $request->phl_employer,
            'phl_employee' => $request->phl_employee
        ];

 
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return Response::json(array(
                'success' => false,
                'errors' => $validator->getMessageBag()->toArray(),
                'message' => 'Some fields are incomplete',
                200
            ));
        }

        if($request->phl_id)
        {
            Philhealth_table::where("phl_id",$request->phl_id)->update($params);
        }
        else
        {
            Philhealth_table::create($params);
        }
        

        $report = Philhealth_table::get();
        $data = array();
        foreach($report as $index=>$rs)
        {
        	$data[$index][0] = "<input type='checkbox' class='phl_id' name='phl_id[]' value='$rs->phl_id' />";
            $data[$index][1]  =  $rs->phl_from ;
        	$data[$index][2]  =  $rs->phl_to;
        	$data[$index][3]  =  $rs->phl_employer ;
            $data[$index][4]  =  $rs->phl_employee ;
            $data[$index][5]  =  $rs->phl_employer + $rs->phl_employee ;
        }

        return Response::json(array(
            'success' => true,
            'message' => $request->phl_id ? "Philhealth successfully updated" : "Philhealth successfully added",
            'data'=>$data,
            200
        ));
    }

  	 public function delete(Request $request) 
     {
        $rules = ['phl_id' => 'required'];
   
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails())
		{
        	return Redirect::to('file-maintenance/philhealth')
		          	 ->with('errorMessage', "At least one philhealth is required to proceed transaction.");
		}
		else
		{
			Philhealth_table::whereIn("phl_id",$request->phl_id)->delete();

			return Redirect::to('file-maintenance/philhealth')
		          	 ->with('successMessage', "Successfully Deleted philhealth.");
		}
        
    }
}
