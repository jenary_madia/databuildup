<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\SSS_table;
use Validator;
use Response;
use Redirect;

class SSSController extends Controller
{
    public function report() 
    {
    	$data["report"] = SSS_table::get();
        $data['module'] = "Payroll Set Up";
        $data['tab'] = "File Maintenance";

        return view('tables.sss' ,$data );
    }

     public function store(Request $request) 
     {
        $rules = ['sss_from' => 'required','sss_to' => 'required','sss_employer' => 'required','sss_employee' => 'required'];
        $params = [
            'sss_from' =>  $request->sss_from,
            'sss_to' => $request->sss_to,
            'sss_employer' => $request->sss_employer,
            'sss_employee' => $request->sss_employee
        ];

 
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return Response::json(array(
                'success' => false,
                'errors' => $validator->getMessageBag()->toArray(),
                'message' => 'Some fields are incomplete',
                200
            ));
        }

        if($request->sss_id)
        {
            SSS_table::where("sss_id",$request->sss_id)->update($params);
        }
        else
        {
            SSS_table::create($params);
        }
        

        $report = SSS_table::get();
        $data = array();
        foreach($report as $index=>$rs)
        {
        	$data[$index][0] = "<input type='checkbox' class='sss_id' name='sss_id[]' value='$rs->sss_id' />";
            $data[$index][1]  =  $rs->sss_from ;
        	$data[$index][2]  =  $rs->sss_to;
        	$data[$index][3]  =  $rs->sss_employer ;
            $data[$index][4]  =  $rs->sss_employee ;
            $data[$index][5]  =  $rs->sss_employer + $rs->sss_employee ;
        }

        return Response::json(array(
            'success' => true,
            'message' => $request->sss_id ? "SSS successfully updated" : "SSS successfully added",
            'data'=>$data,
            200
        ));
    }

  	 public function delete(Request $request) 
     {
        $rules = ['sss_id' => 'required'];
   
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails())
		{
        	return Redirect::to('file-maintenance/sss')
		          	 ->with('errorMessage', "At least one sss is required to proceed transaction.");
		}
		else
		{
			SSS_table::whereIn("sss_id",$request->sss_id)->delete();

			return Redirect::to('file-maintenance/sss')
		          	 ->with('successMessage', "Successfully Deleted sss.");
		}
        
    }
}
