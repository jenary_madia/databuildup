<?php

namespace App\Http\Controllers\PersonnelInformation;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class EmployeesController extends Controller
{
	private $tab;
    private $module;

    public function __construct() {
        $this->tab = "Personnel Information";
        $this->module = "Employee Masterlist";
    }

    public function index() {
    	$data = [
    		'tab' => $this->tab,
    		'module' => $this->module
    	];
        return view("personnel_information.employee-master-list",$data);
    }
}
