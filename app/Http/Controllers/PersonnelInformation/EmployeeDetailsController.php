<?php

namespace App\Http\Controllers\PersonnelInformation;

use App\Departments;
use App\Employees;
use App\EmployeeTypes;
use App\JobPositions;
use App\PayClasses;
use App\Http\Controllers\Controller;

class EmployeeDetailsController extends Controller
{
    private $tab;
    private $module;
    protected $employees;
    protected $departments;
    protected $employeeTypes;
    protected $payClasses;
    protected $jobPositions;
    public function __construct(Employees $employees,Departments $departments,EmployeeTypes $employeeTypes,PayClasses $payClasses,JobPositions $jobPositions)
    {
        $this->tab = "Personnel Information";
        $this->module = "Employee Masterlist";
        $this->employees = $employees;
        $this->departments = $departments;
        $this->employeeTypes = $employeeTypes;
        $this->payClasses = $payClasses;
        $this->jobPositions = $jobPositions;
    }

    public function viewEmployee($employee_number = null) {
        $tab = $this->tab;
        $module = $this->module;
        $emp_number = $employee_number ? $employee_number : $this->employees->generateEmployeeNumber();
        // TODO!! validation (check if employee_number is existing)
        $departments = $this->departments->where('active',1)->select(['dept_id','dept_name'])->get();
        $employee_types = $this->employeeTypes->where('active',1)->select(['type_id','type_title'])->get();
        $pay_classes = $this->payClasses->where('active',1)->select(['class_id','class_title'])->get();
        $job_positions = $this->jobPositions->where('active',1)->select(['job_id','job_title'])->get();
        return view('personnel_information.employee-details.index',compact("emp_number","departments","employee_types","pay_classes","job_positions","tab","module"));
    }
}
