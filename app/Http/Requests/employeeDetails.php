<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class employeeDetails extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "employee_details.employee_number" => "required",
            "employee_details.photo" => "required",
            "employee_details.lastname" => "required",
            "employee_details.middlename" => "required",
            "employee_details.firstname" => "required",
            "employee_details.department" => "required",
            "employee_details.hiring_date" => "required",
            "employee_details.position" => "required",
            "employee_details.contract_duration_from" => "required",
            "employee_details.contract_duration_to" => "required",
            "employee_details.payclass" => "required",
            "employee_details.work_schedule" => "required",
            "employee_details.employee_status" => "required",
            "employee_details.employee_type" => "required",
            "employee_details.monthly_rate" => "required",
            "employee_details.semi_monthly_rate" => "required",
            "employee_details.daily_rate" => "required",
            "employee_details.hourly_rate" => "required",
            'data_for_update.*.*' => 'required', 
            'data_for_update.*' => 'required',            
        ];
    }

    public function messages() 
    {
        return [];
        // return [
        //         'data_for_update.comp_amount.required' => "Amount is required",
        //         'data_for_update.comp_basic_pay.required' =>  "Basic pay is required",
        //         'data_for_update.comp_cash_bond.required' => "Cash bond is required",
        //         'data_for_update.comp_meal_allowance.required' => "Meal allowance is required",
        //         'data_for_update.comp_others.required' => "Other is required",
        //         'data_for_update.comp_pagibig.required' => "Pagibig is required",
        //         'data_for_update.comp_philhealth.required' => "Philhealth is required",
        //         'data_for_update.comp_semi_monthly_rate.required' => "Semi monthly rate is required",
        //         'data_for_update.comp_special_pay.required' => "Special pay is required",
        //         'data_for_update.comp_sss.required' => "SSS is required",
        //         'data_for_update.comp_standard_allowance.required' => "Standar Allowance is required",
        //         'data_for_update.comp_tax.required' => "Tax is required",
        //         "data_for_update.birthdate.required" => "Date of birth is required",
        //         "data_for_update.height.required" => "Height is required",
        //         "data_for_update.birthplace.required" => "Place of birth is required",
        //         "data_for_update.weight.required" => "weight is required",
        //         "data_for_update.gender.required" => "gender is required",
        //         "data_for_update.sss.required" => "SSS is required",
        //         "data_for_update.civil_status.required" => "Civil status is required",
        //         "data_for_update.tin.required" => "TIN is required",
        //         "data_for_update.nationality.required" => "Nationality is required",
        //         "data_for_update.email_address.required" => "Email address is required",
        //         "data_for_update.contact_number.required" => "Contact number is required",
        //         "data_for_update.pagibig.required" => "Pagibig is required",
        //         "data_for_update.language_spoken.required" => "Language spoken is required",
        //         "data_for_update.philhealth.required" => "is required",
        //         "data_for_update.religion.required" => "is required",
        //         "data_for_update.present_address.required" => "is required",
        //         "data_for_update.street.required" => "is required",
        //         "data_for_update.municipal.required" => "is required",
        //         "data_for_update.province.required" => "is required",
        //         "data_for_update.zipcode.required" => "is required",
        //         "data_for_update.permanent_address.required" => "is required",
        //         "data_for_update.street.required" => "is required",
        //         "data_for_update.municipal.required" => "is required",
        //         "data_for_update.province.required" => "is required",
        //         "data_for_update.zipcode.required" => "is required",
        //         "data_for_update.contact_person.required" => "is required",
        //         "data_for_update.name.required" => "is required",
        //         "data_for_update.relationship.required" => "is required",
        //         "data_for_update.number.required" => "is required",
        //         "data_for_update.address.required" => "is required"
        //     ];
    }

    public function response(array $errors) {
        return Response::create([
            "success" => false,
            "message" => "All fields are required",
            "errors" =>$errors,
        ],200);
    }
}
