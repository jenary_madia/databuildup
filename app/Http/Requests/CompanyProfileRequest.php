<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Auth;
use Illuminate\Http\Response;

class CompanyProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $comp_id = Auth::user()->comp_id;
        return [
            'company_name' => "required|unique:companies,comp_name,$comp_id,comp_id|max:50",
            'address' => 'required',
            'email_address' => 'required',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'company_name.required' => 'Company Name is required',
            'address' => 'Address is required',
            'email_address.required'  => 'Email address is required',
        ];
    }

    public function response(array $errors) {

        return Response::create([
            "success" => false,
            "errors" =>$errors,
        ],200);
    }
}
