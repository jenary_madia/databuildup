<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmployeeSkills extends Model
{
    protected $table = 'skills';
    protected $primaryKey = 'skill_id';
    protected $guarded = [];
    public $timestamps = false;
}
