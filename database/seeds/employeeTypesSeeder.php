<?php

use Illuminate\Database\Seeder;

class employeeTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('employee_types')->insert([
            "comp_id" => 1,
            "type_title" => "Regular",
            "active" => 1
        ]);

    }
}
