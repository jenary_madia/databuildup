<?php

use Illuminate\Database\Seeder;

class jobPositionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('job_positions')->insert([
            'comp_id' => 1,
            'job_title' => 'PHP Developer',
            'active' => 1,
        ]);
    }
}
