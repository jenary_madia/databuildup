<?php

use Illuminate\Database\Seeder;

class modulesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$pre_define = [
			'Home'
			, 'File Maintenance'
			, 'Personal Information System'
			, 'Compensation and Benefits'
			, 'Manpower Request Processing'
			, 'Time Monitoring System'
			, 'Payroll Management System'
		];

    	foreach ($pre_define as $pre)
	        DB::table('modules')->insert([
	            'module_name' => $pre
	        ]);
    }
}
