<?php

use Illuminate\Database\Seeder;

class subModulesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $pre_define = [
        	[
        	],
        	[
        		'Company Build up'
        		, 'Approver Set up'
        		, 'Holiday Set up'
        		, 'Access Rights'
        		, 'Leave Management'
        		, 'Work Schedule'
        		, 'Time Keeping Tables'
        		, 'Payroll Set up'
        	],
        	[
        		'Employee Masterlist'
        		, 'Casual Masterlist'
        		, 'Separation Processing'
        		, 'Salary Movement'
        		, 'Performance Appraisal'
        		, 'Certificate of Employment'
        		, 'Employee Disciplinary Action Records'
        		, 'Reimbursement Logbook'
        		, 'Accountability Record'
        		, 'Regularization List'
        		, 'Reports'
        		, 'Employee Medical Record'
        		, 'Department / Client Transfer History'
        		, 'Promotion History'
        	],
        	[
    			'Retirement Computation'
    			, 'Wage Increase'
    			, 'Announcements'
    			, 'Employee Manual'
        	],
        	[
        		'Applicants Profiling'
        		, 'MR Processing'
        		, 'Hiring and Placement'
        		, 'Intern'
        		, 'Contract Generation'
        		, 'Manpower Budget'
        		, 'List of Vacant Regular'
        		, 'List of Vacant Casual'
        		, 'Employee ID Processing'
        		, 'Cost Center Change History'
        		, 'Employers Account'
        		, 'Job Posting'
        		, 'Reports'
        	],
        	[
    			'DTR'
    			, 'Process DTR'
    			, 'Incomplete DTR'
    			, 'Final DTR / Upload to Payroll'
    			, 'Time Off Monitoring'
    			, 'Leave Monitoring'
    			, 'Attendance Monitoring'
        	],
        	[
        		'Payroll History'
        		, 'Payroll Period Builder'
        		, 'Payroll Entry'
        		, 'Payroll Manual'
        		, 'Payroll On Hold'
        		, 'Process 13th Month'
        		, 'Bonus File Entry'
        		, 'Other Earnings'
        		, 'Process Year End Tax'
        		, 'Recurring Loan and Deductions'
        		, 'Send Payslip'
        	]
    	];

    	foreach ($pre_define as $a => $pre_def)
    		foreach ($pre_def as $pre)
		        DB::table('sub_modules')->insert([
		            'sub_module_name' => $pre,
		            'module_id' => ($a + 1)
		        ]);
    }
}
