<?php

use Illuminate\Database\Seeder;

class userSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'admin account',
            'username' => 'admin',
            'email' => 'admin@gmail.com',
            'comp_id' => 1,
            'password' => bcrypt('secret'),
        ]);
    }
}
