<?php

use Illuminate\Database\Seeder;

class departmentsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('departments_clients')->insert([
            'comp_id' => 1,
            'dept_name' => 'HR Department',
            'active' => 1,
            'type' => 1
        ]);
    }
}
