<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         $this->call(userSeeder::class);
         $this->call(companySeeder::class);
         $this->call(departmentsSeeder::class);
         $this->call(employeeTypesSeeder::class);
         $this->call(jobPositionsSeeder::class);
         $this->call(payClassSeeder::class);
         $this->call(modulesSeeder::class);
         $this->call(subModulesSeeder::class);
    }
}
