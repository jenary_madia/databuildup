<?php

use Illuminate\Database\Seeder;

class payClassSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pay_classes')->insert([
            "comp_id" => 1,
            "class_title" => "Regular",
            "active" => 1
        ]);
    }
}
