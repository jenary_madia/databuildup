<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeeDependentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dependents', function (Blueprint $table) {
            $table->increments('dep_id');
            $table->integer('dep_employee_id');
            $table->string('dep_name');
            $table->string('dep_relationship');
            $table->date('dep_bdate');
            $table->integer('dep_age');
            $table->string('dep_gender',10);
            $table->string('dep_disability')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dependents');
    }
}
