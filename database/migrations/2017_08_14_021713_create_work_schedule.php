<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWorkSchedule extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('work_schedule', function (Blueprint $table) {
            $table->increments('sched_id');
            $table->string('sched_type')->unique();
            $table->text('sched_desc')->nullable();
            $table->time('start_time')->nullable();
            $table->time('end_time')->nullable();
            $table->string('rest_day')->nullable();
            $table->string('min_overtime')->nullable();
            $table->string('min_early_overtime')->nullable();
            $table->string('min_night_diff')->nullable();
            $table->time('night_start_time')->nullable();
            $table->time('night_end_time')->nullable();
            $table->string('grace_period')->nullable();
            $table->string('early_in')->nullable();
            $table->string('min_working_hours');
            $table->tinyInteger('less_hr_break')->default(0);
            $table->tinyInteger('comp_rd')->default(0);
            $table->tinyInteger('comp_ot')->default(0);
            $table->tinyInteger('comp_early_ot')->default(0);
            $table->tinyInteger('comp_tardiness')->default(0);
            $table->tinyInteger('comp_undertime')->default(0);
            $table->tinyInteger('comp_night_diff')->default(0);
            $table->tinyInteger('use_tardiness_tbl')->default(0);
            $table->tinyInteger('use_undertime_tbl')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('work_schedule');
    }
}
