<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeeHistoriesDesc extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('histories', function (Blueprint $table) {
            $table->increments('history_id');
            $table->integer('history_employee_id');
            $table->string('history_position');
            $table->string('history_comp_name');
            $table->integer('history_number');
            $table->string('history_address');
            $table->decimal('history_patest_pay',10,2);
            $table->date('history_date_from');
            $table->date('history_date_to');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('histories');
    }
}
