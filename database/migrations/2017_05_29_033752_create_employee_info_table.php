<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeeInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('personal_informations', function (Blueprint $table) {
            $table->increments('info_id');
            $table->integer('info_employee_id');
            $table->date('info_birth_date');
            $table->string('info_birth_place',100);
            $table->integer('info_height');
            $table->integer('info_weight');
            $table->string('info_gender',50);
            $table->string('info_civil_status',50);
            $table->string('info_nationality',100);
            $table->string('info_language');
            $table->string('info_religion',50);
            $table->integer('info_sss');
            $table->integer('info_tin');
            $table->integer('info_pagibig');
            $table->integer('info_philhealth');
            $table->string('info_present_street');
            $table->string('info_present_municipal');
            $table->string('info_present_province');
            $table->integer('info_present_zipcode');
            $table->string('info_perma_street');
            $table->string('info_perma_municipal');
            $table->string('info_perma_province');
            $table->integer('info_perma_zipcode');
            $table->integer('info_contact_no');
            $table->string('info_email',50);
            $table->string('info_cperson_name',50);
            $table->string('info_cperson_relationship',50);
            $table->string('info_cperson_address');
            $table->integer('info_cperson_no');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('personal_informations');
    }
}
