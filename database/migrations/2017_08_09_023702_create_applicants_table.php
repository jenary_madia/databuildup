<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApplicantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('applicants', function (Blueprint $table) {
            $table->increments('app_id');
            $table->string('app_employee_id')->unique();
            $table->string('app_status');
            $table->string('app_position');
            $table->string('app_position_two');
            $table->date('app_date');
            $table->decimal('app_desired_salary',10,2);
            $table->text('app_reason');
            $table->tinyInteger('app_blacklist');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('applicants');
    }
}
