<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {
            $table->increments('comp_id');
            $table->string('comp_name',50)->unique();
            $table->string('comp_industry',50)->nullable();
            $table->string('comp_logo',100)->nullable();
            $table->string('comp_address')->nullable();
            $table->integer('comp_zipcode')->nullable();
            $table->string('comp_city',50)->nullable();
            $table->string('comp_province',50)->nullable();
            $table->integer('comp_telephone_number')->nullable()->length(20);
            $table->integer('comp_mobile_number')->nullable()->length(20);
            $table->integer('comp_fax_number')->nullable()->length(20);
            $table->string('comp_contact_person',100)->nullable();
            $table->string('comp_email',50)->nullable();
            $table->string('comp_website_url',100)->nullable();
            $table->integer('comp_tin')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companies');
    }
}
