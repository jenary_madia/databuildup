<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableSkillEligibilitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('skill_eligibilities', function (Blueprint $table) {
            $table->increments('eligibility_id');
            $table->integer('eligibility_skill_id');
            $table->string('eligibility_exam_name');
            $table->date('eligibility_date_taken');
            $table->string('eligibility_rating');
            $table->string('eligibility_conducted_by');
            $table->string('eligibility_status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('skill_eligibilities');
    }
}
