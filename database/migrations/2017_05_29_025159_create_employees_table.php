<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->increments('emp_id');
            $table->string('emp_number')->unique()->nullable();
            $table->string('emp_applicant_number')->unique()->nullable();
            $table->string('emp_firstname',50)->nullable();
            $table->string('emp_middlename',50)->nullable();
            $table->string('emp_lastname',50)->nullable();
            $table->string('emp_image')->nullable();
            $table->tinyInteger('emp_dept_id')->nullable();
            $table->tinyInteger('emp_position_id')->nullable();
            $table->tinyInteger('emp_class_id')->nullable();
            $table->tinyInteger('emp_type_id')->nullable();
            $table->date('emp_hiring_date')->nullable();
            $table->date('emp_contract_from')->nullable();
            $table->date('emp_contract_to')->nullable();
            $table->tinyInteger('emp_work_sched_id')->nullable();
            $table->tinyInteger('emp_status')->nullable();
            $table->decimal('emp_monthly_rate',10,2)->nullable();
            $table->decimal('emp_semi_monthly_rate',10,2)->nullable();
            $table->decimal('emp_daily_rate',10,2)->nullable();
            $table->decimal('emp_hourly_rate',10,2)->nullable();
            $table->tinyInteger('emp_active')->default(0);
            $table->tinyInteger('is_employee')->default(0)->comment('1 if hired, 0 if not');
            $table->tinyInteger('is_applicant')->default(0)->comment('1 if user pass through applicant table, 0 if not');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees');
    }
}
