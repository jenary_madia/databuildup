<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeeCompensationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('compensations', function (Blueprint $table) {
            $table->increments('comp_id');
            $table->integer('comp_employee_id');
            $table->decimal('comp_basic_pay',10,2);
            $table->decimal('comp_standard_allowance',10,2);
            $table->decimal('comp_special_pay',10,2);
            $table->decimal('comp_semi_monthly_rate',10,2);
            $table->decimal('comp_meal_allowance',10,2);
            $table->decimal('comp_cash_bond',10,2);
            $table->string('comp_others')->nullable();
            $table->decimal('comp_amount',10,2)->nullable();
            $table->decimal('comp_sss',10,2);
            $table->decimal('comp_philhealth',10,2);
            $table->decimal('comp_pagibig',10,2);
            $table->decimal('comp_tax',10,2);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('compensations');
    }
}
