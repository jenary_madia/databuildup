<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDepartmentsClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('departments_clients', function (Blueprint $table) {
            $table->increments('dept_id');
            $table->integer('comp_id');
            $table->string('dept_name',50)->unique();
            $table->tinyInteger('active')->default(1);
            $table->tinyInteger('type')->default(1)->comment('1 if department and 2 for client');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('departments_clients');
    }
}
