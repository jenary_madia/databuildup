@extends('layouts.app')
@section('content')
    <div id="div-employee-details">
        <employee-index
            employee_number="{!! $emp_number !!}"
            departments='{!! $departments !!}'
            employee_types='{!! $employee_types !!}'
            pay_classes='{!! $pay_classes !!}'
            job_positions='{!! $job_positions !!}'
            base_url='{!! url("/") !!}'
        >
        </employee-index>
    </div>
@endsection
@section('scripts')
    <script src="{{ asset('assets/js/employee-details.js') }}"></script>
@endsection