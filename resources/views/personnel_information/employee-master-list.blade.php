@extends('layouts.app')
@section('content')
        <table id="employee-master-list" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
            <tr>
                <th></th>
                <th>Employee Name</th>
                <th>Employee Number</th>
                <th>Department/Client</th>
                <th>Position</th>
                <th>Hiring Date</th>
                <th>Employee Status</th>
                <th>Employee Type</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
        <a type="button" class="btn btn-default" href="{!! route('employee-details.index') !!}">ADD</a>
        <a type="button" class="btn btn-default" id="btn_edit_employee">EDIT</a>
        <button type="button" class="btn btn-default">DELETE</button>
@endsection
@section('scripts')
    <script>
        var url = "{!! route('employee.masterlist') !!}";
        var employee_url = "{!! route('employee-details.index') !!}";
        var emp_numbers = [];
        $(document).ready(function() {
            var list_table = $("#employee-master-list").DataTable({
                "processing": true,
                "serverSide": true,
                "ajax": {
                    "headers": {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    "url": url,
                    "type": "POST"
                }
            });

            $("#btn_edit_employee").on("click",function () {
                if(emp_numbers.length > 1) {
                    swal("","Please select one employee only.","error");
                }else if(emp_numbers.length == 1){
                    window.location = employee_url+'/'+emp_numbers[0];
                }
            });
        } );

        $(document).on("click",'.emp_number',function () {
            if($(this).is(':checked')) {
                emp_numbers.push($(this).val());
            }else{
                emp_numbers.splice($(this).val(),1);
            }
        });
    </script>
@endsection