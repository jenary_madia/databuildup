@extends('layouts.app')
@section('content')
    <form class="form-inline" role="form" method="POST" action="{!! route('holiday.delete') !!}">
    {{ csrf_field() }}
    <table id="holiday_tb" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
        <tr>
            <th></th>
            <th>Holiday Date</th>
            <th>Day</th>
            <th>Description</th>
            <th>Holiday Type</th>
        </tr>
        </thead>
     
        <tbody>

        @foreach($report as $rs)
        <tr>
            <td align="center"><input type="checkbox" name="hol_id[]" class="hol_id" value="{{ $rs->hol_id }}"/></td>
            <td>{{ date("F d, Y",strtotime($rs->hol_date)) }}</td>
            <td>{{ date("l",strtotime($rs->hol_date)) }}</td>
            <td>{{ $rs->hol_desc }}</td>
            <td>{{ $rs->hol_type }}</td> 
        </tr>
        @endforeach


        </tbody>
    </table>

    <button type="button" class="btn btn-default add_build_up">ADD</button>
    <button type="button" class="btn btn-default edit_build_up">EDIT</button>
    <button type="submit" class="btn btn-default delete_build_up">DELETE</button>
    </form>

<div class="modal fade" id="holiday-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
    

      <div class="modal-body">
        <div class="container-fluid bd-example-row">
          <form class="form-horizontal"  >
              <div class="clear_10"></div>
              <input type="hidden" class=" form-control"   name="hol_id" id="hol_id"  />

              <div class="row">
                <div class="col-sm-4"><label class="labels"><span class="text-danger">*</span>Holiday Date:</label></div>
                <div class="col-sm-8">
                    <div class =" input-group  ">
                        <input type="text" class=" form-control date_picker"   name="hol_date" id="hol_date"  />
                        <label class="input-group-addon btn" for="hol_date">
                           <span class="glyphicon glyphicon-calendar"></span>
                        </label>  
                    </div>
                </div>
              </div>

               <br>

               <div class="row">
                <div class="col-sm-4"><label class="labels">Day:</label></div>
                <div class="col-sm-8">
                    <input type="text" class="  form-control" name="day" readonly />
                </div>
              </div>

               <br>

               <div class="row">
                <div class="col-sm-4"><label class="labels"><span class="text-danger">*</span>Description:</label></div>
                <div class="col-sm-8">
                    <input type="text" class="form-control" name="hol_desc" />
                </div>
              </div>

               <br>

               <div class="row">
                <div class="col-sm-4"><label class="labels"><span class="text-danger">*</span>Holiday Type:</label></div>
                <div class="col-sm-8">
                    <select  class="form-control" name="hol_type" >
                        <option></option>
                        <option>Regular Holiday</option>
                        <option>Special Non-Working Holiday</option> 
                    </select>
                </div>
              </div>

               <br>

              <div class="row">
                  <div class="col-sm-12">
                      <div class="text-center  ">
                        <button type="button" class="btn btn-secondary text-center" id="save_hol" >Save</button>
                        <button type="button" class="btn btn-secondary text-center" data-dismiss="modal">Cancel</button>        
                      </div>
                  </div>
              </div>
              
              </form>

          </div>


      </div>

     
     
    </div>
  </div>
</div>

@endsection

@section('scripts')
<script>
 
    var add_url = "{!! route('holiday.add') !!}";
    // var add_url = "{!! route('maintenance.add.buildup') !!}";
 
</script>
<script src="{{ asset('assets/js/holiday.js') }}"></script>
@stop