@extends('layouts.app')
@section('content')
  @include('layouts.sidebar_leave')
  <div class="col-md-10">
    <form class="form-inline" role="form" method="POST" action="{!! route('leavetype.delete') !!}">
    {{ csrf_field() }}
    <table id="leavetype_tb" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
        <tr>
            <th></th>
            <th>Leave Code</th>
            <th>Leave Description</th>
            <th>Pay Mode</th>
        </tr>
        </thead>
     
        <tbody>

        @foreach($report as $rs)
        <tr>
            <td align="center"><input type="checkbox" name="lt_id[]" class="lt_id" value="{{ $rs->lt_id }}"/></td>
            <td>{{ $rs->lt_code }}</td>
            <td>{{ $rs->lt_desc }}</td>
            <td>{{ $rs->lt_type }}</td> 
        </tr>
        @endforeach


        </tbody>
    </table>

    <button type="button" class="btn btn-default add_build_up">ADD</button>
    <button type="button" class="btn btn-default edit_build_up">EDIT</button>
    <button type="submit" class="btn btn-default delete_build_up">DELETE</button>
    </form>
    </div>

<div class="modal fade" id="leavetype-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
    

      <div class="modal-body">
        <div class="container-fluid bd-example-row">
          <form class="form-horizontal"  >
              <div class="clear_10"></div>
              <input type="hidden" class=" form-control"   name="lt_id" id="lt_id"  />

              <div class="row">
                <div class="col-sm-4"><label class="labels"><span class="text-danger">*</span>Leave Code:</label></div>
                <div class="col-sm-8">
                     <input type="text" class=" form-control "   name="lt_code"   />
                </div>
              </div>

               <br>
 

               <div class="row">
                <div class="col-sm-4"><label class="labels"><span class="text-danger">*</span>Leave Description:</label></div>
                <div class="col-sm-8">
                    <input type="text" class="form-control" name="lt_desc" />
                </div>
              </div>

               <br>

               <div class="row">
                <div class="col-sm-4"><label class="labels"><span class="text-danger">*</span>Leave Type:</label></div>
                <div class="col-sm-8">
                    <select  class="form-control" name="lt_type" >
                        <option></option>
                        <option>With Pay</option>
                        <option>Without Pay</option> 
                    </select>
                </div>
              </div>

               <br>

              <div class="row">
                  <div class="col-sm-12">
                      <div class="text-center  ">
                        <button type="button" class="btn btn-secondary text-center" id="save_lt" >Save</button>
                        <button type="button" class="btn btn-secondary text-center" data-dismiss="modal">Cancel</button>        
                      </div>
                  </div>
              </div>
              
              </form>

          </div>


      </div>

     
     
    </div>
  </div>
</div>

@endsection

@section('scripts')
<script>
 
    var add_url = "{!! route('leavetype.add') !!}";
    // var add_url = "{!! route('maintenance.add.buildup') !!}";
 
</script>
<script src="{{ asset('assets/js/leavetype.js') }}"></script>
@stop