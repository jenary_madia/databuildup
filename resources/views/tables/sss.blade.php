@extends('layouts.app')
@section('content')
  @include('layouts.sidebar_payroll')
  <div class="col-md-10">
    <form class="form-inline" role="form" method="POST" action="{!! route('sss.delete') !!}">
    {{ csrf_field() }}
    <table id="sss_tb" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
        <tr>
            <th></th>
            <th>From</th>
            <th>To</th>
            <th>Employer</th>
            <th>Employee</th>
            <th>Total Contribution</th>
        </tr>
        </thead>
     
        <tbody>

        @foreach($report as $rs)
        <tr>
            <td align="center"><input type="checkbox" name="sss_id[]" class="sss_id" value="{{ $rs->sss_id }}"/></td>
            <td>{{ $rs->sss_from }}</td>
            <td>{{ $rs->sss_to }}</td>
            <td>{{ $rs->sss_employer }}</td>
            <td>{{ $rs->sss_employee }}</td>
            <td>{{ $rs->sss_employer + $rs->sss_employee }}</td>
        </tr>
        @endforeach


        </tbody>
    </table>

    <button type="button" class="btn btn-default add_build_up">ADD</button>
    <button type="button" class="btn btn-default edit_build_up">EDIT</button>
    <button type="submit" class="btn btn-default delete_build_up">DELETE</button>
    </form>
    </div>

<div class="modal fade" id="sss-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
    

      <div class="modal-body">
        <div class="container-fluid bd-example-row">
          <form class="form-horizontal"  >
              <div class="clear_10"></div>
              <input type="hidden" class=" form-control"   name="sss_id" id="sss_id"  />

              <div class="row">
                <div class="col-sm-4"><label class="labels"><span class="text-danger">*</span>From:</label></div>
                <div class="col-sm-8">
                    <input type="text" class="form-control" name="sss_from" />
                </div>
              </div>

               <br>

               <div class="row">
                <div class="col-sm-4"><label class="labels"><span class="text-danger">*</span>To:</label></div>
                <div class="col-sm-8">
                    <input type="text" class="form-control" name="sss_to" />
                </div>
              </div>

               <br>

 

               <div class="row">
                <div class="col-sm-4"><label class="labels"><span class="text-danger">*</span>Employer:</label></div>
                <div class="col-sm-8">
                    <input type="text" class="form-control" name="sss_employer" />
                </div>
              </div>

               <br>

               <div class="row">
                <div class="col-sm-4"><label class="labels"><span class="text-danger">*</span>Employee:</label></div>
                <div class="col-sm-8">
                    <input type="text" class="form-control" name="sss_employee" />
                </div>
              </div>

               <br>

    

              <div class="row">
                  <div class="col-sm-12">
                      <div class="text-center  ">
                        <button type="button" class="btn btn-secondary text-center" id="save_sss" >Save</button>
                        <button type="button" class="btn btn-secondary text-center" data-dismiss="modal">Cancel</button>        
                      </div>
                  </div>
              </div>
              
              </form>

          </div>


      </div>

     
     
    </div>
  </div>
</div>

@endsection

@section('scripts')
<script>
 
    var add_url = "{!! route('sss.add') !!}";
    // var add_url = "{!! route('maintenance.add.buildup') !!}";
 
</script>
<script src="{{ asset('assets/js/sss.js') }}"></script>
@stop