@extends('layouts.app')
@section('content')
    @include('layouts.sidebar_payroll')
  <div class="col-md-10">
    <form class="form-inline" role="form" method="POST" action="{!! route('philhealth.delete') !!}">
    {{ csrf_field() }}
    <table id="philhealth_tb" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
        <tr>
            <th></th>
            <th>From</th>
            <th>To</th>
            <th>Employer</th>
            <th>Employee</th>
            <th>Total Contribution</th>
        </tr>
        </thead>
     
        <tbody>

        @foreach($report as $rs)
        <tr>
            <td align="center"><input type="checkbox" name="phl_id[]" class="phl_id" value="{{ $rs->phl_id }}"/></td>
            <td>{{ $rs->phl_from }}</td>
            <td>{{ $rs->phl_to }}</td>
            <td>{{ $rs->phl_employer }}</td>
            <td>{{ $rs->phl_employee }}</td>
            <td>{{ $rs->phl_employer + $rs->phl_employee }}</td>
        </tr>
        @endforeach


        </tbody>
    </table>

    <button type="button" class="btn btn-default add_build_up">ADD</button>
    <button type="button" class="btn btn-default edit_build_up">EDIT</button>
    <button type="submit" class="btn btn-default delete_build_up">DELETE</button>
    </form>
    </div>

<div class="modal fade" id="philhealth-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
    

      <div class="modal-body">
        <div class="container-fluid bd-example-row">
          <form class="form-horizontal"  >
              <div class="clear_10"></div>
              <input type="hidden" class=" form-control"   name="phl_id" id="phl_id"  />

              <div class="row">
                <div class="col-sm-4"><label class="labels"><span class="text-danger">*</span>From:</label></div>
                <div class="col-sm-8">
                    <input type="text" class="form-control" name="phl_from" />
                </div>
              </div>

               <br>

               <div class="row">
                <div class="col-sm-4"><label class="labels"><span class="text-danger">*</span>To:</label></div>
                <div class="col-sm-8">
                    <input type="text" class="form-control" name="phl_to" />
                </div>
              </div>

               <br>

 

               <div class="row">
                <div class="col-sm-4"><label class="labels"><span class="text-danger">*</span>Employer:</label></div>
                <div class="col-sm-8">
                    <input type="text" class="form-control" name="phl_employer" />
                </div>
              </div>

               <br>

               <div class="row">
                <div class="col-sm-4"><label class="labels"><span class="text-danger">*</span>Employee:</label></div>
                <div class="col-sm-8">
                    <input type="text" class="form-control" name="phl_employee" />
                </div>
              </div>

               <br>

    

              <div class="row">
                  <div class="col-sm-12">
                      <div class="text-center  ">
                        <button type="button" class="btn btn-secondary text-center" id="save_phl" >Save</button>
                        <button type="button" class="btn btn-secondary text-center" data-dismiss="modal">Cancel</button>        
                      </div>
                  </div>
              </div>
              
              </form>

          </div>


      </div>

     
     
    </div>
  </div>
</div>

@endsection

@section('scripts')
<script>
 
    var add_url = "{!! route('philhealth.add') !!}";
    // var add_url = "{!! route('maintenance.add.buildup') !!}";
 
</script>
<script src="{{ asset('assets/js/philhealth.js') }}"></script>
@stop