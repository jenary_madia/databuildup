@extends('layouts.app')
@section('content')
    <div id="div-applicant-details">
        <applicant-index
            applicant_number="{!! $app_number !!}"
            departments='{!! $departments !!}'
            employee_types='{!! $employee_types !!}'
            pay_classes='{!! $pay_classes !!}'
            job_positions='{!! $job_positions !!}'
            base_url='{!! url("/") !!}'
        >
        </applicant-index>
    </div>
@endsection
@section('scripts')
    <script src="{{ asset('assets/js/applicant-details.js') }}"></script>
@endsection