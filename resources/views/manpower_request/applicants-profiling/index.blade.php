@extends('layouts.app')
@section('content')	
	<div class="col-md-10 col-md-offset-1">
		<form class="form-inline">
			<div class="row">
				<div class="col-md-4">
					<label style="padding-top: 5px;">Position Applied : </label>
					<select name="" id="" class="form-control input-sm ap-search-select pull-right"></select>
				</div>
				<div class="col-md-4">
					<label style="padding-top: 5px;">Desired Salary : </label>
					<select name="" id="" class="form-control input-sm ap-search-select pull-right"></select>
				</div>
				<div class="clearfix space-10"></div>
				<div class="col-md-4">
					<label style="padding-top: 5px;">Client : </label>
					<select name="" id="" class="form-control input-sm ap-search-select pull-right"></select>
				</div>
				<div class="col-md-4">
					<label style="padding-top: 5px;">Applicant Status : </label>
					<select name="" id="" class="form-control input-sm ap-search-select pull-right"></select>
				</div>
				<button type="submit" class="btn btn-default btn-sm">Search</button>
				<button type="submit" class="btn btn-default btn-sm">Generate report</button>
				<!-- <div class="col-md-4" style="border : 1px solid black;">
					<button type="submit" class="btn btn-default btn-sm">Send invitation</button>
					<button type="submit" class="btn btn-default btn-sm">Send invitation</button>
				</div> -->
			</div>
		</form>
	</div>
	<div class="clearfix"></div>
	<br>
	<div class="col-md-12">
	    <table data-info="false" id="applicant-master-list" class="table table-striped table-bordered" cellspacing="0" width="100%">
	        <thead>
	        <tr>
	            <th></th>
	            <th>Applicant Name</th>
	            <th>Applicant Number</th>
	            <th>Position Applied</th>
	            <th>Application Date</th>
	            <th>Desired Salary</th>
	            <th>Client</th>
	            <th>Applicant Status</th>
	        </tr>
	        </thead>
	        <tbody>
	        </tbody>
	    </table>
	    <a type="button" class="btn btn-default" href="{!! route('applicant-details.index') !!}">ADD</a>
	    <a type="button" class="btn btn-default" id="btn_edit_employee">EDIT</a>
	    <button type="button" class="btn btn-default">DELETE</button>
    </div>
@endsection
@section('scripts')
    <script>
        var url = "{!! route('applicants.masterlist') !!}";
        var employee_url = "{!! route('applicant-details.index') !!}";
        var emp_numbers = [];
        $(document).ready(function() {
            var list_table = $("#applicant-master-list").DataTable({
                "processing": true,
                "serverSide": false,
                "ajax": {
                    "headers": {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    "url": url,
                    "type": "POST"
                },
				"bPaginate": true,
				"bFilter": false,
            });

            $("#btn_edit_employee").on("click",function () {
                if(emp_numbers.length > 1) {
                    swal("","Please select one employee only.","error");
                }else if(emp_numbers.length == 1){
                    window.location = employee_url+'/'+emp_numbers[0];
                }
            });
        } );

        $(document).on("click",'.emp_applicant_number',function () {
            if($(this).is(':checked')) {
                emp_numbers.push($(this).val());
            }else{
                emp_numbers.splice($(this).val(),1);
            }
        });
    </script>
@endsection