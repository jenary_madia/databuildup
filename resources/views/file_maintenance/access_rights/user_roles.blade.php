@extends('layouts.app')
@section('content')
    @include('layouts.sidebar')
    <div class="user-role-container">
        <div class="col-md-10">
            <table id="{{ $tableName }}" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>Role</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
            <div class="form-group">
                <button type="button" class="btn btn-default" @click="showForm">Add</button>
                <button type="button" class="btn btn-default add_build_up">Delete</button>
            </div>
        </div>
        <div class="col-md-10 col-md-offset-2">
            <form class="form-horizontal" @submit.prevent="submitForm($event)" v-if="formShown" role="form" method="POST" enctype="multipart/form-data" action="{{ route('maintenance.access-rights.add-user-roles') }}">
                {{ csrf_field() }}
                <div class="form-group">
                    <div class="col-md-12">
                        <div class="col-md-2">
                            <label for="username" class="text-left">*Role : </label>
                        </div>
                        <div class="col-md-10">
                            <input id="username" type="text" class="form-control input-sm" name="role_name" value="" required autofocus>
                        </div>
                    </div>
                </div>
                <br>
                <div class="form-group">
                    <div class="col-md-12">
                        <div class="col-md-6">
                            <div class="div-bordered-content col-md-12">
                                <div class="div-title">Modules</div>
                                <br>
                                <label v-for="module in modules">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <input type="checkbox" name="modules[]" :value="module.module_id" @click="populateSubModules(module.module_id, $event)">
                                        </span>
                                        <span class="form-control">@{{ module.module_name }}</span>
                                    </div>
                                </label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="div-bordered-content col-md-12">
                                <div class="div-title">Sub Modules</div>
                                <br>
                                <label v-if="sub_modules.length > 1" v-for="sub_module in sub_modules">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <input type="checkbox" name="sub_modules[]" :value="sub_module.sub_module_id" @click="checkAllSubModules(sub_module.sub_module_id, $event)">
                                        </span>
                                        <span class="form-control">@{{ sub_module.sub_module_name }}</span>
                                    </div>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-12">
                        <div class="col-md-12 text-center">
                            <button type="submit" class="btn btn-default">Save</button>
                            <button type="button" class="btn btn-default" @click="hideForm">Cancel</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

@endsection
@section('scripts')
    <script>
        var get_modules_url = "{{ route('maintenance.access-rights.modules.list') }}";
        var add_user_roles = "{{ route('maintenance.access-rights.add-user-roles') }}";

        var sys_modules = <?php echo json_encode($modules) ?>;
        sys_modules.unshift({module_id: 0, module_name: 'All', active: 1})
        var sys_sub_modules = <?php echo json_encode($sub_modules) ?>;

        var getArrayErrors;

        function searchInArray(array, field, value, isMultiple, isRecursive)
        {
            var arrayReturn = [];

            $.each(array, function ()
            {
                if (this[field] == value)
                    if (!isMultiple)
                        return this;
                    else
                        arrayReturn.push(this);
                else
                    if (isRecursive)
                    {
                        var arr = searchInArray(this.children, field, value, isMultiple, isRecursive);
                        if (arr != '')
                            return arr;
                    }
            })

            return arrayReturn;
        }

        function pushToObject(object_container, object_transfer, common_unique_field)
        {
            $.each(object_transfer, function ()
            {
                if(searchInArray(object_container, common_unique_field, this[common_unique_field], true, false).length < 1)
                    object_container.push(this);
            })
        }

        function removeFromObject(object_container, field, value)
        {
            for (var i = object_container.length - 1; i >= 0; i--)
                if (object_container[i][field] == value)
                    object_container.splice(i, 1);
        }

        getArrayErrors = function getArrayErrors(e)
        {
            var a = '';

            $.each(e, function () {
                a += this + '\n';
            });

            return a;
        }

        var user_role_container = new Vue({
            el: '.user-role-container',
            data: {
                formShown: false,
                modules: sys_modules,
                sub_modules: [{sub_module_id: 0, sub_module_name: 'All'}]
            },
            methods: {
                hideForm: function () {
                    this.formShown = false
                },
                showForm: function () {
                    this.formShown = true
                },
                submitForm: function (a) {        
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        method: a.target.method,
                        url: a.target.action,
                        data: $(a.target).serializeArray(),
                    })
                    .done(function( jData ) {
                        if(jData.success == true) {
                            swal({
                                title: "Success",
                                text: jData.message,
                                type: "success",
                                confirmButtonText: "Ok"
                            });
                        }else{
                            swal({
                                title: "Error",
                                text: getArrayErrors(jData.errors),
                                type: "error",
                                confirmButtonText: "Ok"
                            });
                        }
                    });
                },
                populateSubModules: function (a, b)
                {
                    if (a == 0)
                        if (b.target.checked)
                            $('input[name="modules[]"][value!="0"]:not(:checked)').click();
                        else
                            $('input[name="modules[]"][value!="0"]:checked').click();
                    else
                    {
                        if (b.target.checked)
                            pushToObject(this.sub_modules, searchInArray(sys_sub_modules, 'module_id', a, true, false), 'sub_module_id');
                        else
                            removeFromObject(this.sub_modules, 'module_id', a)

                        $('input[name="modules[]"][value="0"]').prop('checked', $('input[name="modules[]"][value!="0"]:checked').length == $('input[name="modules[]"][value!="0"]').length);
                    }
                },
                checkAllSubModules: function (a, b) {
                    if (a == 0)
                        if (b.target.checked)
                            $('input[name="sub_modules[]"][value!="0"]:not(:checked)').click();
                        else
                            $('input[name="sub_modules[]"][value!="0"]:checked').click();

                    $('input[name="sub_modules[]"][value="0"]').prop('checked', $('input[name="sub_modules[]"][value!="0"]:checked').length == $('input[name="sub_modules[]"][value!="0"]').length);
                }
            }
        });

        $(document).ready(function() {

            var list_table = $('#table_modules').DataTable({
                "processing": true,
                "serverSide": true,
                "ajax": {
                    "headers": {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    "url": get_modules_url,
                    "type": "POST"
                }
            });
        } );

    </script>
@endsection