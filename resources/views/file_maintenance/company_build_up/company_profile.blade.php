@extends('layouts.app')
@section('content')
    @include('layouts.sidebar')
    <div class="col-md-10">
        <form class="form-horizontal" id="form-company-details" role="form" method="POST" enctype="multipart/form-data" action="{{ route('maintenance.update.company_details') }}">
            {{ csrf_field() }}
            <div class="div-bordered-content">
                <fieldset>
                    <div class="col-md-7">
                        <div class="form-group">
                            <div class="col-md-4">
                                <label for="username" class="text-left">*Company Name : </label>
                            </div>
                            <div class="col-md-8">
                                <input id="username" type="text" class="form-control input-sm" name="company_name" value="{!! $companyDetails->comp_name !!}" required autofocus>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-4">
                                <label for="username" class="text-left">Industry : </label>
                            </div>
                            <div class="col-md-8">
                                <input id="username" type="text" class="form-control input-sm" name="industry" value="{!! $companyDetails->comp_industry !!}" required autofocus>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="form-group">
                            <div class="col-md-4">
                                <label for="username" class="text-left">*Company Logo : </label>
                            </div>
                            <div class="col-md-8">
                                <img id="image" width="100" height="100" src="{!! $companyDetails->comp_logo ? Storage::url('app/images/company/company-logo.png') : 'http://placehold.it/100x100' !!}">
                                <br>
                                <br>
                                <input type="file" id="company_logo" name="company_logo"/>
                            </div>
                        </div>
                    </div>
                </fieldset>
            </div>
            <br>
            <br>
            <div class="div-bordered-content">
                <div class="div-title">Address / Contact Information</div>
                <br>
                <fieldset>
                    <div class="col-md-9">
                        <div class="form-group">
                            <div class="col-md-4">
                                <label for="username" class="text-left">*Address line 1 : </label>
                            </div>
                            <div class="col-md-8">
                                <input id="username" type="text" class="form-control input-sm" name="address" value="{!! $companyDetails->comp_address !!}" required autofocus>
                            </div>
                        </div>
                        {{--<div class="form-group">--}}
                            {{--<div class="col-md-4">--}}
                                {{--<label for="username" class="text-left">Address line 2 : </label>--}}
                            {{--</div>--}}
                            {{--<div class="col-md-8">--}}
                                {{--<input id="username" type="text" class="form-control input-sm" name="address_2" value="" required autofocus>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <div class="col-md-6">
                                <label for="username" class="text-left">Zipcode : </label>
                            </div>
                            <div class="col-md-6">
                                <input id="username" type="text" class="form-control input-sm" name="zipcode" value="{!! $companyDetails->comp_zipcode !!}" required autofocus>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-7">
                        <div class="form-group">
                            <div class="col-md-4">
                                <label for="username" class="text-left">City : </label>
                            </div>
                            <div class="col-md-8">
                                <input id="username" type="text" class="form-control input-sm" name="city" value="{!! $companyDetails->comp_city !!}" required autofocus>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-4">
                                <label for="username" class="text-left">Province : </label>
                            </div>
                            <div class="col-md-8">
                                <input id="username" type="text" class="form-control input-sm" name="province" value="{!! $companyDetails->comp_province !!}" required autofocus>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <div class="col-md-4">
                                <label for="username" class="text-left">Contact Number : </label>
                            </div>
                            <div class="col-md-8">
                                <input id="username" type="text" class="form-control input-sm" name="tel_number" value="{!! $companyDetails->comp_telephone_number !!}" required autofocus>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <div class="col-md-4">
                                <label for="username" class="text-left"></label>
                            </div>
                            <div class="col-md-8">
                                <input id="username" type="text" class="form-control input-sm" name="mobile_number" value="{!! $companyDetails->comp_mobile_number !!}" required autofocus>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <div class="col-md-4">
                                <label for="username" class="text-left"></label>
                            </div>
                            <div class="col-md-8">
                                <input id="username" type="text" class="form-control input-sm" name="fax_number" value="{!! $companyDetails->comp_fax_number !!}" required autofocus>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-7">
                        <div class="form-group">
                            <div class="col-md-4">
                                <label for="username" class="text-left">Contact Person : </label>
                            </div>
                            <div class="col-md-8">
                                <input id="username" type="text" class="form-control input-sm" name="contact_person" value="{!! $companyDetails->comp_contact_person !!}" required autofocus>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-4">
                                <label for="username" class="text-left">Email Address : </label>
                            </div>
                            <div class="col-md-8">
                                <input id="username" type="text" class="form-control input-sm" name="email_address" value="{!! $companyDetails->comp_email !!}" required autofocus>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-4">
                                <label for="username" class="text-left">Website URL : </label>
                            </div>
                            <div class="col-md-8">
                                <input id="username" type="text" class="form-control input-sm" name="website_url" value="{!! $companyDetails->comp_website_url !!}" required autofocus>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-4">
                                <label for="username" class="text-left">Tax Identification Number : </label>
                            </div>
                            <div class="col-md-6">
                                <input id="username" type="text" class="form-control input-sm" name="tax_number" value="{!! $companyDetails->comp_tin !!}" required autofocus>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <button class="btn btn-default pull-right update_comp_profile">Save</button>
                    </div>
                </fieldset>
            </div>
        </form>
    </div>

@endsection
@section('scripts')
    <script>
        var update_comp_details_url= "{!! route('maintenance.update.company_details') !!}";
        document.getElementById("company_logo").onchange = function () {
            var reader = new FileReader();
            reader.onload = function (e) {
                document.getElementById("image").src = e.target.result;
            };
            reader.readAsDataURL(this.files[0]);
        };
        $(document).ready(function() {
            $(".update_comp_profile").on("click",function (e) {
                e.preventDefault();
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    method: "POST",
                    url: update_comp_details_url,
                    data: $("#form-company-details").serialize(),
                })
                .done(function( response ) {
                    if(response['success'] == false) {
                        alert('hello');
                    }else{
                        $("#form-company-details").submit();
                    }
                });
            });
        } );
    </script>
@endsection