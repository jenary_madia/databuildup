@extends('layouts.app')
@section('content')
    @include('layouts.sidebar')
    <div class="col-md-10">
        <table id="{!! $tableName !!}" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>{!! $tableHead !!}</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
        <form class="form-inline" role="form" method="POST" action="{!! route('maintenance.add.buildup') !!}">
            {!! csrf_field() !!}
            <div class="form-group">
                <input type="text" class="form-control" id="to_add_data_buildup" placeholder="{!! $tableHead !!}">
            </div>
            <button type="submit" class="btn btn-default add_build_up">Add</button>
        </form>
    </div>
@endsection
@section('scripts')
    <script>
        var table_name = "#{!! $tableName !!}";
        var build_up_id = {!! $buildupID !!};
        var add_build_up_url = "{!! route('maintenance.add.buildup') !!}";
        var remove_build_up_url = "{!! route('maintenance.delete.buildup') !!}";
        var get_build_up_url = "{!! route('maintenance.fetch.buildup',$buildupID) !!}";
        $(document).ready(function() {
            var list_table = $(table_name).DataTable({
                "processing": true,
                "serverSide": true,
                "ajax": {
                    "headers": {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    "url": get_build_up_url,
                    "type": "POST"
                }
            });
            $(".add_build_up").on("click",function (e) {
                e.preventDefault();
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    method: "POST",
                    url: add_build_up_url,
                    data: {
                        buildup_id: build_up_id,
                        to_add: $("#to_add_data_buildup").val()
                    }
                })
                .done(function( response ) {
                    $("#to_add_data_buildup").val("");
                    list_table.draw();
                    if(response.success == true) {
                        swal({
                            title: "Success",
                            text: response.message,
                            type: "success",
                            confirmButtonText: "Ok"
                        });
                    }else{
                        swal({
                            title: "Error",
                            text: "Data to add is missing",
                            type: "error",
                            confirmButtonText: "Ok"
                        });
                    }
                });
            });
            $('body').on('click','.btn_delete_data',function (e) {
                e.preventDefault();
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    method: "POST",
                    url: remove_build_up_url,
                    data: {
                        buildup_id: build_up_id,
                        to_remove: $(this).val()
                    }
                })
                .done(function( response ) {
                    if(response.success == true) {
                        swal({
                            title: "Success",
                            text: response.message,
                            type: "success",
                            confirmButtonText: "Ok"
                        });
                    }else{
                        swal({
                            title: "Error",
                            text: "Oops! Something went wrong",
                            type: "error",
                            confirmButtonText: "Ok"
                        });
                    }
                    list_table.draw();
                });
            });

        } );
    </script>
@endsection