@extends('layouts.app')
@section('content')
    <form class="form-inline" role="form" method="POST" action="{!! route('approver.delete') !!}">
    {{ csrf_field() }}
    <table id="approver_tb" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
        <tr>
            <th></th>
            <th>Department / Client</th>
            <th>Approver</th>
            <th>Receiver</th> 
            <th class="text-center"><a data-toggle="modal" data-target="#info-modal" href="#"><i class="glyphicon glyphicon-question-sign  text-success" data-toggle="modal" data-target="#surrender" ></i></a></th> 
        </tr>
        </thead>
     
        <tbody>

        @foreach($report as $rs)
        <tr>
            <td align="center"><input type="checkbox" name="app_id[]" class="app_id" value="{{ $rs->app_id }}"/></td>
            <td><input type="hidden" value="{{ $rs->app_dept }}" />{{ $department[$rs->app_dept] }}</td>
            <td><input type="hidden" value="{{ $rs->app_approver }}" />{{ $employee[$rs->app_approver] }}</td>
            <td><input type="hidden" value="{{ $rs->app_receiver }}" />{{ $employee[$rs->app_receiver] }}</td> 
            <td></td>
        </tr>
        @endforeach


        </tbody>
    </table>

    <button type="button" class="btn btn-default add_build_up">ADD</button>
    <button type="button" class="btn btn-default edit_build_up">EDIT</button>
    <button type="submit" class="btn btn-default delete_build_up">DELETE</button>
    </form>

<div class="modal fade" id="approver-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
    

      <div class="modal-body">
        <div class="container-fluid bd-example-row">
          <form class="form-horizontal"  >
              <div class="clear_10"></div>
              <input type="hidden" class=" form-control"   name="app_id" id="app_id"  />

           

              <div class="row">
                <div class="col-sm-4"><label class="labels"><span class="text-danger">*</span>Department:</label></div>
                <div class="col-sm-8">
                    <select  class="form-control" name="app_dept" >
                        <option></option>
                         @foreach($department as $key => $val)
                        <option value="{{ $key }}">{{ $val }}</option> 
                        @endforeach
                    </select>
                </div>
              </div>

               <br>

               <div class="row">
                <div class="col-sm-4"><label class="labels"><span class="text-danger">*</span>Approver:</label></div>
                <div class="col-sm-8">
                    <select  class="form-control" name="app_approver" >
                        <option></option>
                        @foreach($employee as $key => $val)
                        <option value="{{ $key }}">{{ $val }}</option> 
                        @endforeach
                    </select>
                </div>
              </div>

               <br>

               <div class="row">
                <div class="col-sm-4"><label class="labels"><span class="text-danger">*</span>Receiver:</label></div>
                <div class="col-sm-8">
                    <select  class="form-control" name="app_receiver" >
                        <option></option>
                        @foreach($employee as $key => $val)
                        <option value="{{ $key }}">{{ $val }}</option> 
                        @endforeach
                    </select>
                </div>
              </div>

               <br>

              <div class="row">
                  <div class="col-sm-12">
                      <div class="text-center  ">
                        <button type="button" class="btn btn-secondary text-center" id="save_app" >Save</button>
                        <button type="button" class="btn btn-secondary text-center" data-dismiss="modal">Cancel</button>        
                      </div>
                  </div>
              </div>
              
              </form>

          </div>


      </div>

     
     
    </div>
  </div>
</div>

<div class="modal fade " id="info-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content modal-sm">
    

      <div class="modal-body">
        <div class="container-fluid bd-example-row">
          <form class="form-horizontal"  >
              <div class="clear_10"></div>
              <input type="hidden" class=" form-control"   name="app_id" id="app_id"  />

           

              <div class="row">
                <div class="col-sm-12">
                    An approval workflow must have an employee who request and the one who approves. Employee that were assigned as a default approver shall receive / approve / deny a request in case no approver assigned on specific employee.
                </div>
              </div>

             

               <br>

              <div class="row">
                  <div class="col-sm-12">
                      <div class="text-center  "> 
                        <button type="button" class="btn btn-secondary text-center" data-dismiss="modal">Close</button>        
                      </div>
                  </div>
              </div>
              
              </form>

          </div>


      </div>

     
     
    </div>
  </div>
</div>

@endsection

@section('scripts')
<script>
 
    var add_url = "{!! route('approver.add') !!}";
    // var add_url = "{!! route('maintenance.add.buildup') !!}";
 
</script>
<script src="{{ asset('assets/js/approver.js') }}"></script>
@stop