<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <img class="pull-left" src="{!! asset("assets/images/octal.png") !!}" alt="" height="50">
            <a class="navbar-brand" href="#">Octal Philippines Inc.</a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse pull-right navbar-collapse" id="bs-example-navbar-collapse-1">
            <p class="navbar-text">Welcome, Jenary Madia!</p>
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="{!! url('user/change-password') !!}">Change password</a></li>
                        <li><a href="{!! route('logout') !!}">Logout</a></li>
                    </ul>
                </li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container -->
</nav>

