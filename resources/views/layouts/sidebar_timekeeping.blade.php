<div class="col-md-2 div-sidebar">
    <ul class="list-unstyled list-sidebar">
        <li>
            <a href="{!! route('tardiness.maintenance') !!}">Tardiness Table</a>
        </li>
        <li>
            <a href="{!! route('overtime.maintenance') !!}">Overtime Table</a>
        </li>
        <li>
            <a href="{!! route('undertime.maintenance') !!}">Undertime Table</a>
        </li>
 
    </ul>
</div>