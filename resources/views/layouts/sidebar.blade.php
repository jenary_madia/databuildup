<div class="col-md-2 div-sidebar">
    <ul class="list-unstyled list-sidebar">
        <li>
            <a href="{!! route('maintenance.company') !!}">Company</a>
        </li>
        <li>
            <a href="{!! route('maintenance.department') !!}">Department</a>
        </li>
        <li>
            <a href="{!! route('maintenance.employee-types') !!}">Employee Types</a>
        </li>
        <li>
            <a href="{!! route('maintenance.pay-class') !!}">Pay Classes</a>
        </li>
        <li>
            <a href="{!! route('maintenance.job-positions') !!}">Job Positions</a>
        </li>
    </ul>
</div>