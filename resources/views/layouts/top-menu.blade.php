<div class="col-md-12">
    <ul class="list-unstyled list-inline main-menu">
        <li>
            <div class="btn-group">
                <span data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Home
                </span>
            </div>
        </li>
        <li>
            <div class="btn-group">
                <span data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    File Maintenance
                </span>
                <ul class="dropdown-menu">
                    <li><a href="{!! route('maintenance.department') !!}">Company Build up</a></li>
                    <li><a href="{!! route('approver.maintenance') !!}">Approver Set Up</a></li>
                    <li><a href="{!! route('holiday.maintenance') !!}">Holiday Set Up</a></li>
                    <li><a href="{!! route('maintenance.access-rights.user-roles') !!}">Access Rights</a></li>
                    <li><a href="{!! route('leavetype.maintenance') !!}">Leave Management</a></li>
                    <li><a href="{!! route('Work Schedule') !!}">Work Schedule</a></li>
                    <li><a href="{!! route('tardiness.maintenance') !!}">Time Keeping Tables</a></li>
                    <li><a href="{!! route('sss.maintenance') !!}">Payroll Set up</a></li>
                </ul>
            </div>
        </li>
        <li>
            <div class="btn-group">
                <span data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Personnel Information System
                </span>
                <ul class="dropdown-menu">
                    <li><a href="{!! route('Employee Masterlist') !!}">Employee Masterlist</a></li>
                    <li><a href="">Casual Masterlist</a></li>
                    <li><a href="">Separation Processing</a></li>
                    <li><a href="">Salary Movement</a></li>
                    <li><a href="">Performance Appraisal</a></li>
                    <li><a href="">Certificate of Employment</a></li>
                    <li><a href="">Employee Disciplinary Action Records</a></li>
                    <li><a href="">Reimbursement Logbook</a></li>
                    <li><a href="">Accountabilty Record</a></li>
                    <li><a href="">Regularization List</a></li>
                    <li><a href="">Reports</a></li>
                    <li><a href="">Employee Medical Record</a></li>
                    <li><a href="">Department/Client Transfer History</a></li>
                    <li><a href="">Promotion History</a></li>
                </ul>
            </div>
        </li>
        <li>
            <div class="btn-group">
                <span data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Compensation Benefits
                </span>
                <ul class="dropdown-menu">
                    <li><a href="">Retirement Computation</a></li>
                    <li><a href="">Wage Increase</a></li>
                    <li><a href="">Annoucements</a></li>
                    <li><a href="">Employee Manual</a></li>
                </ul>
            </div>
        </li>
        <li>
            <div class="btn-group">
                <span data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Manpower Request Processing
                </span>
                <ul class="dropdown-menu">
                    <li><a href="{!! route('Applicants Profiling') !!}">Applicants Profiling</a></li>
                    <li><a href="">MR Processing</a></li>
                    <li><a href="">Hiring Placement</a></li>
                    <li><a href="">Intern</a></li>
                    <li><a href="">Contract Generation</a></li>
                    <li><a href="">Manpower Budget</a></li>
                    <li><a href="">List of Vacant Regular</a></li>
                    <li><a href="">List of Vacant Casual</a></li>
                    <li><a href="">Employee ID Processing</a></li>
                    <li><a href="">Cost Center Change History</a></li>
                    <li><a href="">Employers Account</a></li>
                    <li><a href="">Job Posting</a></li>
                    <li><a href="">Reports</a></li>
                </ul>
            </div>
        </li>
        <li>
            <div class="btn-group">
                <span data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Time Monitoring System
                </span>
                <ul class="dropdown-menu">
                    <li><a href="">DTR</a></li>
                    <li><a href="">Process DTR</a></li>
                    <li><a href="">Incomplete DTR</a></li>
                    <li><a href="">Final DTR / Upload to Payroll</a></li>
                    <li><a href="">Time Off Monitoring</a></li>
                    <li><a href="">Leave Monitoring</a></li>
                    <li><a href="">Attendance Monitoring</a></li>
                </ul>
            </div>
        </li>
        <li>
            <div class="btn-group">
                <span data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Payroll Information System
                </span>
                <ul class="dropdown-menu">
                    <li><a href="">Payroll History</a></li>
                    <li><a href="">Payroll Period Builder</a></li>
                    <li><a href="">Payroll Entry</a></li>
                    <li><a href="">Payroll Manual</a></li>
                    <li><a href="">Payroll On Hold</a></li>
                    <li><a href="">Process 13th Month</a></li>
                    <li><a href="">Bonus File Entry</a></li>
                    <li><a href="">Other Earnings</a></li>
                    <li><a href="">Process Year End Tax</a></li>
                    <li><a href="">Recurring Loan and Deductions</a></li>
                    <li><a href="">Send Payslip</a></li>
                </ul>
            </div>
        </li>
    </ul>
</div>
<div class="col-md-12" style="background-color: #01b86a;height : 30px; padding: 5px; font-size: 15px;">
    {!! $tab !!} > {!! $module !!}
    <br>
    <br>
</div>
<div class="clearfix"></div>
<br>
