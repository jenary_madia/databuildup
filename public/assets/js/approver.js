$(document).ready(function() 
{
  

    var list_table = $('#approver_tb').DataTable({
    		"sPaginationType": "full_numbers",
    		  'iDisplayLength' : 10,
               'bInfo':false,
               'bLengthChange':false,     
                'bFilter':false,    
               "aoColumns": [
                  { "sClass": "text-center", "bSortable": false },
                  { "sClass": "", "bSortable": true },
                  { "sClass": "", "bSortable": true },
                  { "sClass": "", "bSortable": true },   
                     { "sClass": "", "bSortable": false },   
                ]       
    });

    $(".add_build_up").click(function()
    {
        clear();
        $("#approver-modal").modal('show');
    })
    

    $(".edit_build_up").click(function(e)
    {
        if($("input[name='app_id[]']:checked").length)
        {
            if($("input[name='app_id[]']:checked").length > 1)
            {
                swal({
                    title: "Error",
                    text: "Choose only 1 item to edit.",
                    type: "error",
                    confirmButtonText: "Ok"
                });
            }
            else
            {
                $("input[name='app_id']").val( $("input[name='app_id[]']:checked").val() );
                $("select[name='app_dept']").val( $("input[name='app_id[]']:checked").parent().siblings(":nth-child(2)").children("input[type='hidden']").val() );
                $("select[name='app_approver']").val( $("input[name='app_id[]']:checked").parent().siblings(":nth-child(3)").children("input[type='hidden']").val() );
                $("select[name='app_receiver']").val( $("input[name='app_id[]']:checked").parent().siblings(":nth-child(4)").children("input[type='hidden']").val() );
              
                $("#approver-modal").modal('show');
            }
        }
        else
        {
             swal({
                title: "Error",
                text: "Choose item to edit.",
                type: "error",
                confirmButtonText: "Ok"
            });
        }
    })
 
    $("#save_app").on("click",function (e) {
 
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    method: "POST",
                    url: add_url,
                    data: $("form").serializeArray()
                })
                .done(function( response ) {

                	
                   
                    // $("#to_add_data_buildup").val("");
                    // list_table.draw();
                    if(response.success == true) 
                    {
                    	 list_table.clear();
				        $(response.data).each(function(val,index)
				        {
				              list_table.rows.add([ index ]);  
				        });        
				         list_table.draw();


                        swal({
                            title: "Success",
                            text: response.message,
                            type: "success",
                            confirmButtonText: "Ok"
                        });

                        $("#approver-modal").modal('hide');
                    }else{
                        swal({
                            title: "Error",
                            text: "Required field must be filled up.",
                            type: "error",
                            confirmButtonText: "Ok"
                        });
                    }

                });
            });



    function clear()
    {
        $("input[name='app_id']").val("");
        $("select[name='app_dept']").val("");
        $("select[name='app_approver']").val("");
        $("select[name='app_receiver']").val(""); 
    }

} );