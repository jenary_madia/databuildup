/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 64);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports) {

// this module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle

module.exports = function normalizeComponent (
  rawScriptExports,
  compiledTemplate,
  scopeId,
  cssModules
) {
  var esModule
  var scriptExports = rawScriptExports = rawScriptExports || {}

  // ES6 modules interop
  var type = typeof rawScriptExports.default
  if (type === 'object' || type === 'function') {
    esModule = rawScriptExports
    scriptExports = rawScriptExports.default
  }

  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // render functions
  if (compiledTemplate) {
    options.render = compiledTemplate.render
    options.staticRenderFns = compiledTemplate.staticRenderFns
  }

  // scopedId
  if (scopeId) {
    options._scopeId = scopeId
  }

  // inject cssModules
  if (cssModules) {
    var computed = Object.create(options.computed || null)
    Object.keys(cssModules).forEach(function (key) {
      var module = cssModules[key]
      computed[key] = function () { return module }
    })
    options.computed = computed
  }

  return {
    esModule: esModule,
    exports: scriptExports,
    options: options
  }
}


/***/ }),
/* 1 */,
/* 2 */,
/* 3 */,
/* 4 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    name: "characterReference",
    props: ['employee_number', 'base_url'],
    mounted: function mounted() {
        var that = this;
        this.$http.post(that.base_url + '/get-employee-info', { employee_number: that.employee_number, to_get: "characterReference" }).then(function (response) {
            if (response.body) {
                var references = response.body;
                if (references.length != 0) {
                    for (var i = 0; i < references.length; i++) {
                        delete references[i]['ref_employee_id'];
                        delete references[i]['ref_id'];
                    }
                    that.references = references;
                }
            }
        }, function (response) {
            swal("Error", "Something went wrong!", "error");
        });
    },
    data: function data() {
        return {
            references: [{
                ref_name: "",
                ref_position: "",
                ref_company: "",
                ref_address: "",
                ref_contact_no: ""
            }]
        };
    },

    methods: {
        addReference: function addReference() {
            this.references.push({
                ref_name: "",
                ref_position: "",
                ref_company: "",
                ref_address: "",
                ref_contact_no: ""
            });
        },
        removeReference: function removeReference(index) {
            this.references.splice(index, 1);
        },
        update: function update() {
            this.$emit("detailsToSave", this.references);
        }
    }
});

/***/ }),
/* 5 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    name: "educationalBackground",
    props: ['employee_number', 'base_url'],
    mounted: function mounted() {
        var that = this;
        this.$http.post(that.base_url + '/get-employee-info', { employee_number: that.employee_number, to_get: "educationalBackground" }).then(function (response) {
            if (response.body.length > 0) {
                var educational_background = response.body;
                for (var i = 0; i < educational_background.length; i++) {
                    delete educational_background[i]['educ_id'];
                    delete educational_background[i]['educ_employee_id'];
                }
                that.educational_background = educational_background;
                console.log(educational_background);
            }
        }, function (response) {
            swal("Error", "Something went wrong!", "error");
        });
    },
    data: function data() {
        return {
            educational_background: [{
                educ_level: "",
                educ_school_name: "",
                educ_address: "",
                educ_year_graduated: "",
                educ_degree: ""
            }]
        };
    },

    methods: {
        addBackground: function addBackground() {
            this.educational_background.push({
                educ_level: "",
                educ_school_name: "",
                educ_address: "",
                educ_year_graduated: "",
                educ_degree: ""
            });
        },
        removeBackground: function removeBackground(index) {
            this.educational_background.splice(index, 1);
        },
        update: function update() {
            this.$emit("detailsToSave", this.educational_background);
        }
    }
});

/***/ }),
/* 6 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    name: "employmentHistory",
    props: ['employee_number', 'base_url'],
    mounted: function mounted() {
        var _this = this;

        var that = this;
        this.$http.post(that.base_url + '/get-employee-info', { employee_number: that.employee_number, to_get: "employmentHistory" }).then(function (response) {
            if (response.body) {
                var employmentHistories = response.body;
                if (employmentHistories.length != 0) {
                    for (var i = 0; i < employmentHistories.length; i++) {
                        delete employmentHistories[i]['history_employee_id'];
                        delete employmentHistories[i]['history_id'];
                    }
                    _this.employmentHistories = employmentHistories;
                }
            }
        }, function (response) {
            swal("Error", "Something went wrong!", "error");
        });
    },
    data: function data() {
        return {
            employmentHistories: [{
                history_position: "",
                history_comp_name: "",
                history_number: "",
                history_address: "",
                history_patest_pay: "",
                history_date_from: "",
                history_date_to: ""
            }]
        };
    },

    methods: {
        addHistory: function addHistory() {
            this.employmentHistories.push({
                history_position: "",
                history_comp_name: "",
                history_number: "",
                history_address: "",
                history_patest_pay: "",
                history_date_from: "",
                history_date_to: ""
            });
        },
        removeHistory: function removeHistory(index) {
            this.employmentHistories.splice(index, 1);
        },
        update: function update() {
            this.$emit("detailsToSave", this.employmentHistories);
        }
    }
});

/***/ }),
/* 7 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    name: "familyBackground",
    props: ['employee_number', 'base_url'],
    mounted: function mounted() {
        var that = this;
        this.$http.post(that.base_url + '/get-employee-info', { employee_number: that.employee_number, to_get: "familyBackground" }).then(function (response) {
            if (response.body.length > 0) {
                var family_background = response.body;
                for (var i = 0; i < family_background.length; i++) {
                    delete family_background[i]['fam_id'];
                    delete family_background[i]['fam_employee_id'];
                }
                that.family_background = family_background;
            }
        }, function (response) {
            swal("Error", "Something went wrong!", "error");
        });
    },
    data: function data() {
        return {
            family_background: [{
                fam_name: "",
                fam_relationship: "",
                fam_bdate: "",
                fam_civil_status: "",
                fam_education: ""
            }]
        };
    },

    methods: {
        addBackground: function addBackground() {
            this.family_background.push({
                fam_name: "",
                fam_relationship: "",
                fam_bdate: "",
                fam_civil_status: "",
                fam_education: ""
            });
        },
        removeBackground: function removeBackground(index) {
            this.family_background.splice(index, 1);
        },
        update: function update() {
            this.$emit("detailsToSave", this.family_background);
        }
    }
});

/***/ }),
/* 8 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    name: "personalInformation",
    props: ['employee_number', 'base_url'],
    mounted: function mounted() {
        var _this = this;

        var that = this;
        this.$http.post(that.base_url + '/get-employee-info', { employee_number: that.employee_number, to_get: "personalInformation" }).then(function (response) {
            if (response.body) {
                _this.personal_information = {
                    "birthdate": response.body.info_birth_date,
                    "height": response.body.info_height,
                    "birthplace": response.body.info_birth_place,
                    "weight": response.body.info_weight,
                    "gender": response.body.info_gender,
                    "sss": response.body.info_sss,
                    "civil_status": response.body.info_civil_status,
                    "tin": response.body.info_tin,
                    "nationality": response.body.info_nationality,
                    "email_address": response.body.info_email,
                    "contact_number": response.body.info_contact_no,
                    "pagibig": response.body.info_pagibig,
                    "language_spoken": response.body.info_language,
                    "philhealth": response.body.info_philhealth,
                    "religion": response.body.info_religion,
                    "present_address": {
                        "street": response.body.info_present_street,
                        "municipal": response.body.info_present_municipal,
                        "province": response.body.info_present_province,
                        "zipcode": response.body.info_present_zipcode
                    },
                    "permanent_address": {
                        "street": response.body.info_perma_street,
                        "municipal": response.body.info_perma_municipal,
                        "province": response.body.info_perma_province,
                        "zipcode": response.body.info_perma_zipcode
                    },
                    "contact_person": {
                        "name": response.body.info_cperson_name,
                        "relationship": response.body.info_cperson_relationship,
                        "number": response.body.info_cperson_no,
                        "address": response.body.info_cperson_address
                    }
                };
                if (that.isEquivalent(that.personal_information.permanent_address, that.personal_information.permanent_address)) {
                    that.sameAddress = true;
                }
            }
        }, function (response) {
            swal("Error", "Something went wrong!", "error");
        });
        $('#bdate').datepicker({
            "dateFormat": 'yy-mm-dd',
            onSelect: function onSelect(dateText) {
                that.personal_information.birthdate = dateText;
            }
        });
    },
    data: function data() {
        return {
            "sameAddress": false,
            personal_information: {
                "birthdate": "",
                "height": "",
                "birthplace": "",
                "weight": "",
                "gender": "",
                "sss": "",
                "civil_status": "",
                "tin": "",
                "nationality": "",
                "pagibig": "",
                "language_spoken": "",
                "philhealth": "",
                "religion": "",
                "email_address": "",
                "contact_number": "",
                "present_address": {
                    "street": "",
                    "municipal": "",
                    "province": "",
                    "zipcode": ""
                },
                "permanent_address": {
                    "street": "",
                    "municipal": "",
                    "province": "",
                    "zipcode": ""
                },
                "contact_person": {
                    "name": "",
                    "relationship": "",
                    "number": "",
                    "address": ""
                }
            }
        };
    },

    methods: {
        update: function update() {
            this.$emit("detailsToSave", this.personal_information);
        },
        isEquivalent: function isEquivalent(a, b) {
            // Create arrays of property names
            var aProps = Object.getOwnPropertyNames(a);
            var bProps = Object.getOwnPropertyNames(b);
            if (aProps.length != bProps.length) {
                return false;
            }

            for (var i = 0; i < aProps.length; i++) {
                var propName = aProps[i];
                if (a[propName] !== b[propName]) {
                    return false;
                }
            }
            return true;
        }
    },
    watch: {
        sameAddress: function sameAddress() {
            if (this.sameAddress) {
                this.personal_information.permanent_address = this.personal_information.present_address;
            } else {
                this.$set(this.personal_information, "permanent_address", {
                    "street": "",
                    "municipal": "",
                    "province": "",
                    "zipcode": ""
                });
            }
        }
    }

});

/***/ }),
/* 9 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    name: "skills",
    props: ['employee_number', 'base_url'],
    mounted: function mounted() {
        var that = this;
        this.$http.post(that.base_url + '/get-employee-info', { employee_number: that.employee_number, to_get: "skills" }).then(function (response) {
            if (response.body) {
                for (var i = 0; i < response.body.eligibilities.length; i++) {
                    delete response.body.eligibilities[i]['job_res_id'];
                    delete response.body.eligibilities[i]['job_description_id'];
                }
                that.skills = response.body;
            }
        }, function (response) {
            swal("Error", "Something went wrong!", "error");
        });
    },
    data: function data() {
        return {
            skills: {
                skill_special: "",
                skill_com_language: "",
                skill_equipment: "",
                skill_affiliations: "",
                eligibilities: [{
                    eligibility_exam_name: "",
                    eligibility_date_taken: "",
                    eligibility_rating: "",
                    eligibility_conducted_by: "",
                    eligibility_status: ""
                }]
            }
        };
    },

    methods: {
        addEligibility: function addEligibility() {
            this.skills.eligibilities.push({
                eligibility_exam_name: "",
                eligibility_date_taken: "",
                eligibility_rating: "",
                eligibility_conducted_by: "",
                eligibility_status: ""
            });
        },
        removeEligibility: function removeEligibility(index) {
            this.skills.eligibilities.splice(index, 1);
        },
        update: function update() {
            this.$emit("detailsToSave", this.skills);
        }
    }
});

/***/ }),
/* 10 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    name: "trainings",
    props: ['employee_number', 'base_url'],
    mounted: function mounted() {
        var that = this;
        this.$http.post(that.base_url + '/get-employee-info', { employee_number: that.employee_number, to_get: "trainings" }).then(function (response) {
            if (response.body.length > 0) {
                var trainings = response.body;
                for (var i = 0; i < trainings.length; i++) {
                    delete trainings[i]['training_id'];
                    delete trainings[i]['training_employee_id'];
                }
                that.trainings = trainings;
            }
        }, function (response) {
            swal("Error", "Something went wrong!", "error");
        });
    },
    data: function data() {
        return {
            trainings: [{
                training_title: "",
                training_description: "",
                training_provider: "",
                training_date_from: "",
                training_date_to: ""
            }]
        };
    },

    methods: {
        addTraining: function addTraining() {
            this.trainings.push({
                training_title: "",
                training_description: "",
                training_provider: "",
                training_date_from: "",
                training_date_to: ""
            });
        },
        removeTraining: function removeTraining(index) {
            this.trainings.splice(index, 1);
        },
        update: function update() {
            this.$emit("detailsToSave", this.trainings);
        }
    }
});

/***/ }),
/* 11 */
/***/ (function(module, exports, __webpack_require__) {

var Component = __webpack_require__(0)(
  /* script */
  __webpack_require__(4),
  /* template */
  __webpack_require__(23),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "C:\\laragon\\www\\buildup\\resources\\assets\\js\\components\\personnel-information\\employee-details\\sub-components\\char-reference.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] char-reference.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-743282d6", Component.options)
  } else {
    hotAPI.reload("data-v-743282d6", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),
/* 12 */
/***/ (function(module, exports, __webpack_require__) {

var Component = __webpack_require__(0)(
  /* script */
  __webpack_require__(5),
  /* template */
  __webpack_require__(22),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "C:\\laragon\\www\\buildup\\resources\\assets\\js\\components\\personnel-information\\employee-details\\sub-components\\educational-background.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] educational-background.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-6e23012e", Component.options)
  } else {
    hotAPI.reload("data-v-6e23012e", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),
/* 13 */
/***/ (function(module, exports, __webpack_require__) {

var Component = __webpack_require__(0)(
  /* script */
  __webpack_require__(6),
  /* template */
  __webpack_require__(18),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "C:\\laragon\\www\\buildup\\resources\\assets\\js\\components\\personnel-information\\employee-details\\sub-components\\employment-history.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] employment-history.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-2b220e98", Component.options)
  } else {
    hotAPI.reload("data-v-2b220e98", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),
/* 14 */
/***/ (function(module, exports, __webpack_require__) {

var Component = __webpack_require__(0)(
  /* script */
  __webpack_require__(7),
  /* template */
  __webpack_require__(20),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "C:\\laragon\\www\\buildup\\resources\\assets\\js\\components\\personnel-information\\employee-details\\sub-components\\family-background.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] family-background.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-5d6aa546", Component.options)
  } else {
    hotAPI.reload("data-v-5d6aa546", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),
/* 15 */
/***/ (function(module, exports, __webpack_require__) {

var Component = __webpack_require__(0)(
  /* script */
  __webpack_require__(8),
  /* template */
  __webpack_require__(19),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "C:\\laragon\\www\\buildup\\resources\\assets\\js\\components\\personnel-information\\employee-details\\sub-components\\personal-information.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] personal-information.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-32df99e0", Component.options)
  } else {
    hotAPI.reload("data-v-32df99e0", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),
/* 16 */
/***/ (function(module, exports, __webpack_require__) {

var Component = __webpack_require__(0)(
  /* script */
  __webpack_require__(9),
  /* template */
  __webpack_require__(21),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "C:\\laragon\\www\\buildup\\resources\\assets\\js\\components\\personnel-information\\employee-details\\sub-components\\skills.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] skills.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-5d9eaf43", Component.options)
  } else {
    hotAPI.reload("data-v-5d9eaf43", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),
/* 17 */
/***/ (function(module, exports, __webpack_require__) {

var Component = __webpack_require__(0)(
  /* script */
  __webpack_require__(10),
  /* template */
  __webpack_require__(24),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "C:\\laragon\\www\\buildup\\resources\\assets\\js\\components\\personnel-information\\employee-details\\sub-components\\trainings.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] trainings.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-7f29dee8", Component.options)
  } else {
    hotAPI.reload("data-v-7f29dee8", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),
/* 18 */
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-md-12"
  }, [_c('div', {
    staticClass: "panel panel-default"
  }, [_c('div', {
    staticClass: "panel-body"
  }, [_c('table', {
    staticClass: "table"
  }, [_vm._m(0), _vm._v(" "), _c('tbody', _vm._l((_vm.employmentHistories), function(history, index) {
    return _c('tr', [_c('td', [_c('input', {
      directives: [{
        name: "model",
        rawName: "v-model",
        value: (history.history_position),
        expression: "history.history_position"
      }],
      staticClass: "form-control input-sm pull-left",
      attrs: {
        "type": "text"
      },
      domProps: {
        "value": (history.history_position)
      },
      on: {
        "input": function($event) {
          if ($event.target.composing) { return; }
          history.history_position = $event.target.value
        }
      }
    })]), _vm._v(" "), _c('td', [_c('input', {
      directives: [{
        name: "model",
        rawName: "v-model",
        value: (history.history_comp_name),
        expression: "history.history_comp_name"
      }],
      staticClass: "form-control input-sm pull-left",
      attrs: {
        "type": "text"
      },
      domProps: {
        "value": (history.history_comp_name)
      },
      on: {
        "input": function($event) {
          if ($event.target.composing) { return; }
          history.history_comp_name = $event.target.value
        }
      }
    })]), _vm._v(" "), _c('td', [_c('input', {
      directives: [{
        name: "model",
        rawName: "v-model",
        value: (history.history_number),
        expression: "history.history_number"
      }],
      staticClass: "form-control input-sm pull-left",
      attrs: {
        "type": "text"
      },
      domProps: {
        "value": (history.history_number)
      },
      on: {
        "input": function($event) {
          if ($event.target.composing) { return; }
          history.history_number = $event.target.value
        }
      }
    })]), _vm._v(" "), _c('td', [_c('input', {
      directives: [{
        name: "model",
        rawName: "v-model",
        value: (history.history_address),
        expression: "history.history_address"
      }],
      staticClass: "form-control input-sm pull-left",
      attrs: {
        "type": "text"
      },
      domProps: {
        "value": (history.history_address)
      },
      on: {
        "input": function($event) {
          if ($event.target.composing) { return; }
          history.history_address = $event.target.value
        }
      }
    })]), _vm._v(" "), _c('td', [_c('input', {
      directives: [{
        name: "model",
        rawName: "v-model",
        value: (history.history_patest_pay),
        expression: "history.history_patest_pay"
      }],
      staticClass: "form-control input-sm pull-left",
      attrs: {
        "type": "text"
      },
      domProps: {
        "value": (history.history_patest_pay)
      },
      on: {
        "input": function($event) {
          if ($event.target.composing) { return; }
          history.history_patest_pay = $event.target.value
        }
      }
    })]), _vm._v(" "), _c('td', [_c('input', {
      directives: [{
        name: "model",
        rawName: "v-model",
        value: (history.history_date_from),
        expression: "history.history_date_from"
      }],
      staticClass: "form-control input-sm pull-left",
      attrs: {
        "type": "text"
      },
      domProps: {
        "value": (history.history_date_from)
      },
      on: {
        "input": function($event) {
          if ($event.target.composing) { return; }
          history.history_date_from = $event.target.value
        }
      }
    })]), _vm._v(" "), _c('td', [_c('input', {
      directives: [{
        name: "model",
        rawName: "v-model",
        value: (history.history_date_to),
        expression: "history.history_date_to"
      }],
      staticClass: "form-control input-sm pull-left",
      attrs: {
        "type": "text"
      },
      domProps: {
        "value": (history.history_date_to)
      },
      on: {
        "input": function($event) {
          if ($event.target.composing) { return; }
          history.history_date_to = $event.target.value
        }
      }
    })]), _vm._v(" "), _c('td', [(index == 0) ? _c('button', {
      staticClass: "glyphicon glyphicon-plus",
      on: {
        "click": function($event) {
          _vm.addHistory()
        }
      }
    }) : _c('button', {
      staticClass: "glyphicon glyphicon-remove",
      on: {
        "click": function($event) {
          _vm.removeHistory(index)
        }
      }
    })])])
  }))]), _vm._v(" "), _c('div', {
    staticClass: "form-group"
  }, [_c('div', {
    staticClass: "col-md-12 text-right"
  }, [_c('button', {
    staticClass: "btn btn-default",
    on: {
      "click": function($event) {
        $event.preventDefault();
        _vm.update($event)
      }
    }
  }, [_vm._v("Save")]), _vm._v(" "), _c('button', {
    staticClass: "btn btn-default"
  }, [_vm._v("Print")])])])])])])])
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('thead', [_c('tr', [_c('td', [_vm._v("Position")]), _vm._v(" "), _c('td', [_vm._v("Company Name")]), _vm._v(" "), _c('td', [_vm._v("Phone Number")]), _vm._v(" "), _c('td', [_vm._v("Address")]), _vm._v(" "), _c('td', [_vm._v("Latest Pay")]), _vm._v(" "), _c('td', [_vm._v("From")]), _vm._v(" "), _c('td', [_vm._v("To")])])])
}]}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-2b220e98", module.exports)
  }
}

/***/ }),
/* 19 */
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-md-12"
  }, [_c('div', {
    staticClass: "panel panel-default"
  }, [_c('div', {
    staticClass: "panel-body"
  }, [_c('form', {
    staticClass: "form-horizontal"
  }, [_c('div', {
    staticClass: "form-group"
  }, [_vm._m(0), _vm._v(" "), _c('div', {
    staticClass: "col-md-3"
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.personal_information.birthdate),
      expression: "personal_information.birthdate"
    }],
    staticClass: "datepicker form-control input-sm pull-left",
    attrs: {
      "type": "text",
      "id": "bdate"
    },
    domProps: {
      "value": (_vm.personal_information.birthdate)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.personal_information.birthdate = $event.target.value
      }
    }
  })]), _vm._v(" "), _vm._m(1), _vm._v(" "), _c('div', {
    staticClass: "col-md-3"
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.personal_information.height),
      expression: "personal_information.height"
    }],
    staticClass: "form-control input-sm pull-left",
    attrs: {
      "type": "text"
    },
    domProps: {
      "value": (_vm.personal_information.height)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.personal_information.height = $event.target.value
      }
    }
  })])]), _vm._v(" "), _c('div', {
    staticClass: "form-group"
  }, [_vm._m(2), _vm._v(" "), _c('div', {
    staticClass: "col-md-3"
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.personal_information.birthplace),
      expression: "personal_information.birthplace"
    }],
    staticClass: "form-control input-sm pull-left",
    attrs: {
      "type": "text"
    },
    domProps: {
      "value": (_vm.personal_information.birthplace)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.personal_information.birthplace = $event.target.value
      }
    }
  })]), _vm._v(" "), _vm._m(3), _vm._v(" "), _c('div', {
    staticClass: "col-md-3"
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.personal_information.weight),
      expression: "personal_information.weight"
    }],
    staticClass: "form-control input-sm pull-left",
    attrs: {
      "type": "text"
    },
    domProps: {
      "value": (_vm.personal_information.weight)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.personal_information.weight = $event.target.value
      }
    }
  })])]), _vm._v(" "), _c('div', {
    staticClass: "form-group"
  }, [_vm._m(4), _vm._v(" "), _c('div', {
    staticClass: "col-md-3"
  }, [_c('select', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.personal_information.gender),
      expression: "personal_information.gender"
    }],
    staticClass: "form-control input-sm pull-left",
    attrs: {
      "name": ""
    },
    on: {
      "change": function($event) {
        var $$selectedVal = Array.prototype.filter.call($event.target.options, function(o) {
          return o.selected
        }).map(function(o) {
          var val = "_value" in o ? o._value : o.value;
          return val
        });
        _vm.personal_information.gender = $event.target.multiple ? $$selectedVal : $$selectedVal[0]
      }
    }
  }, [_c('option', {
    attrs: {
      "value": "male"
    }
  }, [_vm._v("MALE")]), _vm._v(" "), _c('option', {
    attrs: {
      "value": "female"
    }
  }, [_vm._v("FEMALE")])])]), _vm._v(" "), _vm._m(5), _vm._v(" "), _c('div', {
    staticClass: "col-md-3"
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.personal_information.sss),
      expression: "personal_information.sss"
    }],
    staticClass: "form-control input-sm pull-left",
    attrs: {
      "type": "text"
    },
    domProps: {
      "value": (_vm.personal_information.sss)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.personal_information.sss = $event.target.value
      }
    }
  })])]), _vm._v(" "), _c('div', {
    staticClass: "form-group"
  }, [_vm._m(6), _vm._v(" "), _c('div', {
    staticClass: "col-md-3"
  }, [_c('select', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.personal_information.civil_status),
      expression: "personal_information.civil_status"
    }],
    staticClass: "form-control input-sm pull-left",
    attrs: {
      "name": ""
    },
    on: {
      "change": function($event) {
        var $$selectedVal = Array.prototype.filter.call($event.target.options, function(o) {
          return o.selected
        }).map(function(o) {
          var val = "_value" in o ? o._value : o.value;
          return val
        });
        _vm.personal_information.civil_status = $event.target.multiple ? $$selectedVal : $$selectedVal[0]
      }
    }
  }, [_c('option', {
    attrs: {
      "value": "married"
    }
  }, [_vm._v("Married")]), _vm._v(" "), _c('option', {
    attrs: {
      "value": "single"
    }
  }, [_vm._v("Single")])])]), _vm._v(" "), _vm._m(7), _vm._v(" "), _c('div', {
    staticClass: "col-md-3"
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.personal_information.tin),
      expression: "personal_information.tin"
    }],
    staticClass: "form-control input-sm pull-left",
    attrs: {
      "type": "text"
    },
    domProps: {
      "value": (_vm.personal_information.tin)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.personal_information.tin = $event.target.value
      }
    }
  })])]), _vm._v(" "), _c('div', {
    staticClass: "form-group"
  }, [_vm._m(8), _vm._v(" "), _c('div', {
    staticClass: "col-md-3"
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.personal_information.nationality),
      expression: "personal_information.nationality"
    }],
    staticClass: "form-control input-sm pull-left",
    attrs: {
      "type": "text"
    },
    domProps: {
      "value": (_vm.personal_information.nationality)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.personal_information.nationality = $event.target.value
      }
    }
  })]), _vm._v(" "), _vm._m(9), _vm._v(" "), _c('div', {
    staticClass: "col-md-3"
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.personal_information.pagibig),
      expression: "personal_information.pagibig"
    }],
    staticClass: "form-control input-sm pull-left",
    attrs: {
      "type": "text"
    },
    domProps: {
      "value": (_vm.personal_information.pagibig)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.personal_information.pagibig = $event.target.value
      }
    }
  })])]), _vm._v(" "), _c('div', {
    staticClass: "form-group"
  }, [_vm._m(10), _vm._v(" "), _c('div', {
    staticClass: "col-md-3"
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.personal_information.language_spoken),
      expression: "personal_information.language_spoken"
    }],
    staticClass: "form-control input-sm pull-left",
    attrs: {
      "type": "text"
    },
    domProps: {
      "value": (_vm.personal_information.language_spoken)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.personal_information.language_spoken = $event.target.value
      }
    }
  })]), _vm._v(" "), _vm._m(11), _vm._v(" "), _c('div', {
    staticClass: "col-md-3"
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.personal_information.philhealth),
      expression: "personal_information.philhealth"
    }],
    staticClass: "form-control input-sm pull-left",
    attrs: {
      "type": "text"
    },
    domProps: {
      "value": (_vm.personal_information.philhealth)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.personal_information.philhealth = $event.target.value
      }
    }
  })])]), _vm._v(" "), _c('div', {
    staticClass: "form-group"
  }, [_vm._m(12), _vm._v(" "), _c('div', {
    staticClass: "col-md-3"
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.personal_information.religion),
      expression: "personal_information.religion"
    }],
    staticClass: "form-control input-sm pull-left",
    attrs: {
      "type": "text"
    },
    domProps: {
      "value": (_vm.personal_information.religion)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.personal_information.religion = $event.target.value
      }
    }
  })])]), _vm._v(" "), _c('div', {
    staticClass: "form-group"
  }, [_vm._m(13), _vm._v(" "), _c('div', {
    staticClass: "col-md-3"
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.personal_information.contact_number),
      expression: "personal_information.contact_number"
    }],
    staticClass: "form-control input-sm pull-left",
    attrs: {
      "type": "text"
    },
    domProps: {
      "value": (_vm.personal_information.contact_number)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.personal_information.contact_number = $event.target.value
      }
    }
  })]), _vm._v(" "), _vm._m(14), _vm._v(" "), _c('div', {
    staticClass: "col-md-3"
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.personal_information.email_address),
      expression: "personal_information.email_address"
    }],
    staticClass: "form-control input-sm pull-left",
    attrs: {
      "type": "text"
    },
    domProps: {
      "value": (_vm.personal_information.email_address)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.personal_information.email_address = $event.target.value
      }
    }
  })])]), _vm._v(" "), _c('div', {
    staticClass: "form-group"
  }, [_vm._m(15), _vm._v(" "), _c('div', {
    staticClass: "col-md-3"
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.personal_information.present_address.street),
      expression: "personal_information.present_address.street"
    }],
    staticClass: "form-control input-sm pull-left",
    attrs: {
      "type": "text"
    },
    domProps: {
      "value": (_vm.personal_information.present_address.street)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.personal_information.present_address.street = $event.target.value
      }
    }
  })]), _vm._v(" "), _c('div', {
    staticClass: "col-md-3"
  }), _vm._v(" "), _c('div', {
    staticClass: "col-md-3"
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.personal_information.present_address.municipal),
      expression: "personal_information.present_address.municipal"
    }],
    staticClass: "form-control input-sm pull-left",
    attrs: {
      "type": "text"
    },
    domProps: {
      "value": (_vm.personal_information.present_address.municipal)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.personal_information.present_address.municipal = $event.target.value
      }
    }
  })])]), _vm._v(" "), _c('div', {
    staticClass: "form-group"
  }, [_c('div', {
    staticClass: "col-md-3"
  }), _vm._v(" "), _c('div', {
    staticClass: "col-md-3"
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.personal_information.present_address.province),
      expression: "personal_information.present_address.province"
    }],
    staticClass: "form-control input-sm pull-left",
    attrs: {
      "type": "text"
    },
    domProps: {
      "value": (_vm.personal_information.present_address.province)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.personal_information.present_address.province = $event.target.value
      }
    }
  })]), _vm._v(" "), _c('div', {
    staticClass: "col-md-3"
  }), _vm._v(" "), _c('div', {
    staticClass: "col-md-3"
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.personal_information.present_address.zipcode),
      expression: "personal_information.present_address.zipcode"
    }],
    staticClass: "form-control input-sm pull-left",
    attrs: {
      "type": "text"
    },
    domProps: {
      "value": (_vm.personal_information.present_address.zipcode)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.personal_information.present_address.zipcode = $event.target.value
      }
    }
  })])]), _vm._v(" "), _c('div', {
    staticClass: "form-group"
  }, [_c('div', {
    staticClass: "col-md-6"
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.sameAddress),
      expression: "sameAddress"
    }],
    staticStyle: {
      "margin-right": "10px"
    },
    attrs: {
      "type": "checkbox"
    },
    domProps: {
      "checked": Array.isArray(_vm.sameAddress) ? _vm._i(_vm.sameAddress, null) > -1 : (_vm.sameAddress)
    },
    on: {
      "__c": function($event) {
        var $$a = _vm.sameAddress,
          $$el = $event.target,
          $$c = $$el.checked ? (true) : (false);
        if (Array.isArray($$a)) {
          var $$v = null,
            $$i = _vm._i($$a, $$v);
          if ($$c) {
            $$i < 0 && (_vm.sameAddress = $$a.concat($$v))
          } else {
            $$i > -1 && (_vm.sameAddress = $$a.slice(0, $$i).concat($$a.slice($$i + 1)))
          }
        } else {
          _vm.sameAddress = $$c
        }
      }
    }
  }), _c('label', [_vm._v("same as present address")])])]), _vm._v(" "), _c('div', {
    staticClass: "form-group"
  }, [_vm._m(16), _vm._v(" "), _c('div', {
    staticClass: "col-md-3"
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.personal_information.permanent_address.street),
      expression: "personal_information.permanent_address.street"
    }],
    staticClass: "form-control input-sm pull-left",
    attrs: {
      "type": "text",
      "disabled": _vm.sameAddress
    },
    domProps: {
      "value": (_vm.personal_information.permanent_address.street)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.personal_information.permanent_address.street = $event.target.value
      }
    }
  })]), _vm._v(" "), _c('div', {
    staticClass: "col-md-3"
  }), _vm._v(" "), _c('div', {
    staticClass: "col-md-3"
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.personal_information.permanent_address.municipal),
      expression: "personal_information.permanent_address.municipal"
    }],
    staticClass: "form-control input-sm pull-left",
    attrs: {
      "type": "text",
      "disabled": _vm.sameAddress
    },
    domProps: {
      "value": (_vm.personal_information.permanent_address.municipal)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.personal_information.permanent_address.municipal = $event.target.value
      }
    }
  })])]), _vm._v(" "), _c('div', {
    staticClass: "form-group"
  }, [_c('div', {
    staticClass: "col-md-3"
  }), _vm._v(" "), _c('div', {
    staticClass: "col-md-3"
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.personal_information.permanent_address.province),
      expression: "personal_information.permanent_address.province"
    }],
    staticClass: "form-control input-sm pull-left",
    attrs: {
      "type": "text",
      "disabled": _vm.sameAddress
    },
    domProps: {
      "value": (_vm.personal_information.permanent_address.province)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.personal_information.permanent_address.province = $event.target.value
      }
    }
  })]), _vm._v(" "), _c('div', {
    staticClass: "col-md-3"
  }), _vm._v(" "), _c('div', {
    staticClass: "col-md-3"
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.personal_information.permanent_address.zipcode),
      expression: "personal_information.permanent_address.zipcode"
    }],
    staticClass: "form-control input-sm pull-left",
    attrs: {
      "type": "text",
      "disabled": _vm.sameAddress
    },
    domProps: {
      "value": (_vm.personal_information.permanent_address.zipcode)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.personal_information.permanent_address.zipcode = $event.target.value
      }
    }
  })])]), _vm._v(" "), _vm._m(17), _vm._v(" "), _c('div', {
    staticClass: "form-group"
  }, [_vm._m(18), _vm._v(" "), _c('div', {
    staticClass: "col-md-5"
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.personal_information.contact_person.name),
      expression: "personal_information.contact_person.name"
    }],
    staticClass: "form-control input-sm pull-left",
    attrs: {
      "type": "text"
    },
    domProps: {
      "value": (_vm.personal_information.contact_person.name)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.personal_information.contact_person.name = $event.target.value
      }
    }
  })])]), _vm._v(" "), _c('div', {
    staticClass: "form-group"
  }, [_vm._m(19), _vm._v(" "), _c('div', {
    staticClass: "col-md-3"
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.personal_information.contact_person.relationship),
      expression: "personal_information.contact_person.relationship"
    }],
    staticClass: "form-control input-sm pull-left",
    attrs: {
      "type": "text"
    },
    domProps: {
      "value": (_vm.personal_information.contact_person.relationship)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.personal_information.contact_person.relationship = $event.target.value
      }
    }
  })]), _vm._v(" "), _vm._m(20), _vm._v(" "), _c('div', {
    staticClass: "col-md-3"
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.personal_information.contact_person.number),
      expression: "personal_information.contact_person.number"
    }],
    staticClass: "form-control input-sm pull-left",
    attrs: {
      "type": "text"
    },
    domProps: {
      "value": (_vm.personal_information.contact_person.number)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.personal_information.contact_person.number = $event.target.value
      }
    }
  })])]), _vm._v(" "), _c('div', {
    staticClass: "form-group"
  }, [_vm._m(21), _vm._v(" "), _c('div', {
    staticClass: "col-md-9"
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.personal_information.contact_person.address),
      expression: "personal_information.contact_person.address"
    }],
    staticClass: "form-control input-sm pull-left",
    attrs: {
      "type": "text"
    },
    domProps: {
      "value": (_vm.personal_information.contact_person.address)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.personal_information.contact_person.address = $event.target.value
      }
    }
  })])]), _vm._v(" "), _c('div', {
    staticClass: "form-group"
  }, [_c('div', {
    staticClass: "col-md-12 text-right"
  }, [_c('button', {
    staticClass: "btn btn-default",
    on: {
      "click": function($event) {
        $event.preventDefault();
        _vm.update($event)
      }
    }
  }, [_vm._v("Save")]), _vm._v(" "), _c('button', {
    staticClass: "btn btn-default"
  }, [_vm._v("Print")])])])])])])])])
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "col-md-3"
  }, [_c('label', [_vm._v("Date of Birth : ")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "col-md-3"
  }, [_c('label', [_vm._v("Height : ")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "col-md-3"
  }, [_c('label', [_vm._v("Place of Birth : ")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "col-md-3"
  }, [_c('label', [_vm._v("Weight : ")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "col-md-3"
  }, [_c('label', [_vm._v("Gender : ")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "col-md-3"
  }, [_c('label', [_vm._v("SSS : ")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "col-md-3"
  }, [_c('label', [_vm._v("Civil Status : ")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "col-md-3"
  }, [_c('label', [_vm._v("TIN : ")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "col-md-3"
  }, [_c('label', [_vm._v("Nationality : ")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "col-md-3"
  }, [_c('label', [_vm._v("Pagibig : ")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "col-md-3"
  }, [_c('label', [_vm._v("Language Spoken : ")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "col-md-3"
  }, [_c('label', [_vm._v("Philhealth : ")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "col-md-3"
  }, [_c('label', [_vm._v("Religion : ")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "col-md-3"
  }, [_c('label', [_vm._v("Contact Number : ")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "col-md-3"
  }, [_c('label', [_vm._v("Email Address : ")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "col-md-3"
  }, [_c('label', [_vm._v("Present Address : ")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "col-md-3"
  }, [_c('label', [_vm._v("Permanent Address: ")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "form-group"
  }, [_c('div', {
    staticClass: "col-md-12"
  }, [_c('label', [_vm._v("Contact Person Incase of emergency : ")])])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "col-md-3"
  }, [_c('label', [_vm._v("Name : ")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "col-md-3"
  }, [_c('label', [_vm._v("Relationship : ")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "col-md-3"
  }, [_c('label', [_vm._v("Contact Number : ")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "col-md-3"
  }, [_c('label', [_vm._v("Address : ")])])
}]}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-32df99e0", module.exports)
  }
}

/***/ }),
/* 20 */
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-md-12"
  }, [_c('div', {
    staticClass: "panel panel-default"
  }, [_c('div', {
    staticClass: "panel-body"
  }, [_c('table', {
    staticClass: "table"
  }, [_vm._m(0), _vm._v(" "), _c('tbody', _vm._l((_vm.family_background), function(family, index) {
    return _c('tr', [_c('td', [_c('input', {
      directives: [{
        name: "model",
        rawName: "v-model",
        value: (family.fam_name),
        expression: "family.fam_name"
      }],
      staticClass: "form-control input-sm pull-left",
      attrs: {
        "type": "text"
      },
      domProps: {
        "value": (family.fam_name)
      },
      on: {
        "input": function($event) {
          if ($event.target.composing) { return; }
          family.fam_name = $event.target.value
        }
      }
    })]), _vm._v(" "), _c('td', [_c('input', {
      directives: [{
        name: "model",
        rawName: "v-model",
        value: (family.fam_relationship),
        expression: "family.fam_relationship"
      }],
      staticClass: "form-control input-sm pull-left",
      attrs: {
        "type": "text"
      },
      domProps: {
        "value": (family.fam_relationship)
      },
      on: {
        "input": function($event) {
          if ($event.target.composing) { return; }
          family.fam_relationship = $event.target.value
        }
      }
    })]), _vm._v(" "), _c('td', [_c('input', {
      directives: [{
        name: "model",
        rawName: "v-model",
        value: (family.fam_bdate),
        expression: "family.fam_bdate"
      }],
      staticClass: "form-control input-sm pull-left",
      attrs: {
        "type": "text"
      },
      domProps: {
        "value": (family.fam_bdate)
      },
      on: {
        "input": function($event) {
          if ($event.target.composing) { return; }
          family.fam_bdate = $event.target.value
        }
      }
    })]), _vm._v(" "), _c('td', [_c('input', {
      directives: [{
        name: "model",
        rawName: "v-model",
        value: (family.fam_civil_status),
        expression: "family.fam_civil_status"
      }],
      staticClass: "form-control input-sm pull-left",
      attrs: {
        "type": "text"
      },
      domProps: {
        "value": (family.fam_civil_status)
      },
      on: {
        "input": function($event) {
          if ($event.target.composing) { return; }
          family.fam_civil_status = $event.target.value
        }
      }
    })]), _vm._v(" "), _c('td', [_c('input', {
      directives: [{
        name: "model",
        rawName: "v-model",
        value: (family.fam_education),
        expression: "family.fam_education"
      }],
      staticClass: "form-control input-sm pull-left",
      attrs: {
        "type": "text"
      },
      domProps: {
        "value": (family.fam_education)
      },
      on: {
        "input": function($event) {
          if ($event.target.composing) { return; }
          family.fam_education = $event.target.value
        }
      }
    })]), _vm._v(" "), _c('td', [(index == 0) ? _c('button', {
      staticClass: "glyphicon glyphicon-plus",
      on: {
        "click": function($event) {
          _vm.addBackground()
        }
      }
    }) : _c('button', {
      staticClass: "glyphicon glyphicon-remove",
      on: {
        "click": function($event) {
          _vm.removeBackground(index)
        }
      }
    })])])
  }))]), _vm._v(" "), _c('div', {
    staticClass: "form-group"
  }, [_c('div', {
    staticClass: "col-md-12 text-right"
  }, [_c('button', {
    staticClass: "btn btn-default",
    on: {
      "click": function($event) {
        $event.preventDefault();
        _vm.update($event)
      }
    }
  }, [_vm._v("Save")]), _vm._v(" "), _c('button', {
    staticClass: "btn btn-default"
  }, [_vm._v("Print")])])])])])])])
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('thead', [_c('tr', [_c('td', [_vm._v("Name")]), _vm._v(" "), _c('td', [_vm._v("Relationship")]), _vm._v(" "), _c('td', [_vm._v("Date of Birth")]), _vm._v(" "), _c('td', [_vm._v("Civil Status")]), _vm._v(" "), _c('td', [_vm._v("Education")])])])
}]}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-5d6aa546", module.exports)
  }
}

/***/ }),
/* 21 */
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-md-12"
  }, [_c('div', {
    staticClass: "panel panel-default"
  }, [_c('div', {
    staticClass: "panel-body"
  }, [_c('h4', [_vm._v("Skills and Qualifications")]), _vm._v(" "), _c('form', {
    staticClass: "form-horizontal"
  }, [_c('div', {
    staticClass: "form-group"
  }, [_vm._m(0), _vm._v(" "), _c('div', {
    staticClass: "col-md-9"
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.skills.skill_special),
      expression: "skills.skill_special"
    }],
    staticClass: "form-control input-sm pull-left",
    attrs: {
      "type": "text"
    },
    domProps: {
      "value": (_vm.skills.skill_special)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.skills.skill_special = $event.target.value
      }
    }
  })])]), _vm._v(" "), _c('div', {
    staticClass: "form-group"
  }, [_vm._m(1), _vm._v(" "), _c('div', {
    staticClass: "col-md-9"
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.skills.skill_com_language),
      expression: "skills.skill_com_language"
    }],
    staticClass: "form-control input-sm pull-left",
    attrs: {
      "type": "text"
    },
    domProps: {
      "value": (_vm.skills.skill_com_language)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.skills.skill_com_language = $event.target.value
      }
    }
  })])]), _vm._v(" "), _c('div', {
    staticClass: "form-group"
  }, [_vm._m(2), _vm._v(" "), _c('div', {
    staticClass: "col-md-9"
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.skills.skill_equipment),
      expression: "skills.skill_equipment"
    }],
    staticClass: "form-control input-sm pull-left",
    attrs: {
      "type": "text"
    },
    domProps: {
      "value": (_vm.skills.skill_equipment)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.skills.skill_equipment = $event.target.value
      }
    }
  })])]), _vm._v(" "), _c('div', {
    staticClass: "form-group"
  }, [_vm._m(3), _vm._v(" "), _c('div', {
    staticClass: "col-md-9"
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.skills.skill_affiliations),
      expression: "skills.skill_affiliations"
    }],
    staticClass: "form-control input-sm pull-left",
    attrs: {
      "type": "text"
    },
    domProps: {
      "value": (_vm.skills.skill_affiliations)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.skills.skill_affiliations = $event.target.value
      }
    }
  })])])]), _vm._v(" "), _c('table', {
    staticClass: "table"
  }, [_vm._m(4), _vm._v(" "), _c('tbody', _vm._l((_vm.skills.eligibilities), function(eligibility, index) {
    return _c('tr', [_c('td', [_c('input', {
      directives: [{
        name: "model",
        rawName: "v-model",
        value: (eligibility.eligibility_exam_name),
        expression: "eligibility.eligibility_exam_name"
      }],
      staticClass: "form-control input-sm pull-left",
      attrs: {
        "type": "text"
      },
      domProps: {
        "value": (eligibility.eligibility_exam_name)
      },
      on: {
        "input": function($event) {
          if ($event.target.composing) { return; }
          eligibility.eligibility_exam_name = $event.target.value
        }
      }
    })]), _vm._v(" "), _c('td', [_c('input', {
      directives: [{
        name: "model",
        rawName: "v-model",
        value: (eligibility.eligibility_date_taken),
        expression: "eligibility.eligibility_date_taken"
      }],
      staticClass: "form-control input-sm pull-left",
      attrs: {
        "type": "text"
      },
      domProps: {
        "value": (eligibility.eligibility_date_taken)
      },
      on: {
        "input": function($event) {
          if ($event.target.composing) { return; }
          eligibility.eligibility_date_taken = $event.target.value
        }
      }
    })]), _vm._v(" "), _c('td', [_c('input', {
      directives: [{
        name: "model",
        rawName: "v-model",
        value: (eligibility.eligibility_rating),
        expression: "eligibility.eligibility_rating"
      }],
      staticClass: "form-control input-sm pull-left",
      attrs: {
        "type": "text"
      },
      domProps: {
        "value": (eligibility.eligibility_rating)
      },
      on: {
        "input": function($event) {
          if ($event.target.composing) { return; }
          eligibility.eligibility_rating = $event.target.value
        }
      }
    })]), _vm._v(" "), _c('td', [_c('input', {
      directives: [{
        name: "model",
        rawName: "v-model",
        value: (eligibility.eligibility_conducted_by),
        expression: "eligibility.eligibility_conducted_by"
      }],
      staticClass: "form-control input-sm pull-left",
      attrs: {
        "type": "text"
      },
      domProps: {
        "value": (eligibility.eligibility_conducted_by)
      },
      on: {
        "input": function($event) {
          if ($event.target.composing) { return; }
          eligibility.eligibility_conducted_by = $event.target.value
        }
      }
    })]), _vm._v(" "), _c('td', [_c('input', {
      directives: [{
        name: "model",
        rawName: "v-model",
        value: (eligibility.eligibility_status),
        expression: "eligibility.eligibility_status"
      }],
      staticClass: "form-control input-sm pull-left",
      attrs: {
        "type": "text"
      },
      domProps: {
        "value": (eligibility.eligibility_status)
      },
      on: {
        "input": function($event) {
          if ($event.target.composing) { return; }
          eligibility.eligibility_status = $event.target.value
        }
      }
    })]), _vm._v(" "), _c('td', [(index == 0) ? _c('button', {
      staticClass: "glyphicon glyphicon-plus",
      on: {
        "click": function($event) {
          _vm.addEligibility()
        }
      }
    }) : _c('button', {
      staticClass: "glyphicon glyphicon-remove",
      on: {
        "click": function($event) {
          _vm.removeEligibility(index)
        }
      }
    })])])
  }))]), _vm._v(" "), _c('div', {
    staticClass: "form-group"
  }, [_c('div', {
    staticClass: "col-md-12 text-right"
  }, [_c('button', {
    staticClass: "btn btn-default",
    on: {
      "click": function($event) {
        $event.preventDefault();
        _vm.update($event)
      }
    }
  }, [_vm._v("Save")]), _vm._v(" "), _c('button', {
    staticClass: "btn btn-default"
  }, [_vm._v("Print")])])])])])])])
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "col-md-3"
  }, [_c('label', [_vm._v("Special Skills : ")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "col-md-3"
  }, [_c('label', [_vm._v("Computer Software Languages : ")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "col-md-3"
  }, [_c('label', [_vm._v("Office Equipment can operate : ")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "col-md-3"
  }, [_c('label', [_vm._v("Professional Affiliations : ")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('thead', [_c('tr', [_c('td', [_vm._v("Gov't Examination Passed")]), _vm._v(" "), _c('td', [_vm._v("Date Taken")]), _vm._v(" "), _c('td', [_vm._v("Rating")]), _vm._v(" "), _c('td', [_vm._v("Conducted By")]), _vm._v(" "), _c('td', [_vm._v("Status")])])])
}]}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-5d9eaf43", module.exports)
  }
}

/***/ }),
/* 22 */
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-md-12"
  }, [_c('div', {
    staticClass: "panel panel-default"
  }, [_c('div', {
    staticClass: "panel-body"
  }, [_c('table', {
    staticClass: "table"
  }, [_vm._m(0), _vm._v(" "), _c('tbody', _vm._l((_vm.educational_background), function(educ, index) {
    return _c('tr', [_c('td', [_c('input', {
      directives: [{
        name: "model",
        rawName: "v-model",
        value: (educ.educ_level),
        expression: "educ.educ_level"
      }],
      staticClass: "form-control input-sm pull-left",
      attrs: {
        "type": "text"
      },
      domProps: {
        "value": (educ.educ_level)
      },
      on: {
        "input": function($event) {
          if ($event.target.composing) { return; }
          educ.educ_level = $event.target.value
        }
      }
    })]), _vm._v(" "), _c('td', [_c('input', {
      directives: [{
        name: "model",
        rawName: "v-model",
        value: (educ.educ_school_name),
        expression: "educ.educ_school_name"
      }],
      staticClass: "form-control input-sm pull-left",
      attrs: {
        "type": "text"
      },
      domProps: {
        "value": (educ.educ_school_name)
      },
      on: {
        "input": function($event) {
          if ($event.target.composing) { return; }
          educ.educ_school_name = $event.target.value
        }
      }
    })]), _vm._v(" "), _c('td', [_c('input', {
      directives: [{
        name: "model",
        rawName: "v-model",
        value: (educ.educ_address),
        expression: "educ.educ_address"
      }],
      staticClass: "form-control input-sm pull-left",
      attrs: {
        "type": "text"
      },
      domProps: {
        "value": (educ.educ_address)
      },
      on: {
        "input": function($event) {
          if ($event.target.composing) { return; }
          educ.educ_address = $event.target.value
        }
      }
    })]), _vm._v(" "), _c('td', [_c('input', {
      directives: [{
        name: "model",
        rawName: "v-model",
        value: (educ.educ_year_graduated),
        expression: "educ.educ_year_graduated"
      }],
      staticClass: "form-control input-sm pull-left",
      attrs: {
        "type": "text"
      },
      domProps: {
        "value": (educ.educ_year_graduated)
      },
      on: {
        "input": function($event) {
          if ($event.target.composing) { return; }
          educ.educ_year_graduated = $event.target.value
        }
      }
    })]), _vm._v(" "), _c('td', [_c('input', {
      directives: [{
        name: "model",
        rawName: "v-model",
        value: (educ.educ_degree),
        expression: "educ.educ_degree"
      }],
      staticClass: "form-control input-sm pull-left",
      attrs: {
        "type": "text"
      },
      domProps: {
        "value": (educ.educ_degree)
      },
      on: {
        "input": function($event) {
          if ($event.target.composing) { return; }
          educ.educ_degree = $event.target.value
        }
      }
    })]), _vm._v(" "), _c('td', [(index == 0) ? _c('button', {
      staticClass: "glyphicon glyphicon-plus",
      on: {
        "click": function($event) {
          _vm.addBackground()
        }
      }
    }) : _c('button', {
      staticClass: "glyphicon glyphicon-remove",
      on: {
        "click": function($event) {
          _vm.removeBackground(index)
        }
      }
    })])])
  }))]), _vm._v(" "), _c('div', {
    staticClass: "form-group"
  }, [_c('div', {
    staticClass: "col-md-12 text-right"
  }, [_c('button', {
    staticClass: "btn btn-default",
    on: {
      "click": function($event) {
        $event.preventDefault();
        _vm.update($event)
      }
    }
  }, [_vm._v("Save")]), _vm._v(" "), _c('button', {
    staticClass: "btn btn-default"
  }, [_vm._v("Print")])])])])])])])
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('thead', [_c('tr', [_c('td', [_vm._v("Level")]), _vm._v(" "), _c('td', [_vm._v("School Name")]), _vm._v(" "), _c('td', [_vm._v("Address")]), _vm._v(" "), _c('td', [_vm._v("Year Graduated")]), _vm._v(" "), _c('td', [_vm._v("Degree")])])])
}]}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-6e23012e", module.exports)
  }
}

/***/ }),
/* 23 */
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-md-12"
  }, [_c('div', {
    staticClass: "panel panel-default"
  }, [_c('div', {
    staticClass: "panel-body"
  }, [_c('table', {
    staticClass: "table"
  }, [_vm._m(0), _vm._v(" "), _c('tbody', _vm._l((_vm.references), function(reference, index) {
    return _c('tr', [_c('td', [_c('input', {
      directives: [{
        name: "model",
        rawName: "v-model",
        value: (reference.ref_name),
        expression: "reference.ref_name"
      }],
      staticClass: "form-control input-sm pull-left",
      attrs: {
        "type": "text"
      },
      domProps: {
        "value": (reference.ref_name)
      },
      on: {
        "input": function($event) {
          if ($event.target.composing) { return; }
          reference.ref_name = $event.target.value
        }
      }
    })]), _vm._v(" "), _c('td', [_c('input', {
      directives: [{
        name: "model",
        rawName: "v-model",
        value: (reference.ref_position),
        expression: "reference.ref_position"
      }],
      staticClass: "form-control input-sm pull-left",
      attrs: {
        "type": "text"
      },
      domProps: {
        "value": (reference.ref_position)
      },
      on: {
        "input": function($event) {
          if ($event.target.composing) { return; }
          reference.ref_position = $event.target.value
        }
      }
    })]), _vm._v(" "), _c('td', [_c('input', {
      directives: [{
        name: "model",
        rawName: "v-model",
        value: (reference.ref_company),
        expression: "reference.ref_company"
      }],
      staticClass: "form-control input-sm pull-left",
      attrs: {
        "type": "text"
      },
      domProps: {
        "value": (reference.ref_company)
      },
      on: {
        "input": function($event) {
          if ($event.target.composing) { return; }
          reference.ref_company = $event.target.value
        }
      }
    })]), _vm._v(" "), _c('td', [_c('input', {
      directives: [{
        name: "model",
        rawName: "v-model",
        value: (reference.ref_address),
        expression: "reference.ref_address"
      }],
      staticClass: "form-control input-sm pull-left",
      attrs: {
        "type": "text"
      },
      domProps: {
        "value": (reference.ref_address)
      },
      on: {
        "input": function($event) {
          if ($event.target.composing) { return; }
          reference.ref_address = $event.target.value
        }
      }
    })]), _vm._v(" "), _c('td', [_c('input', {
      directives: [{
        name: "model",
        rawName: "v-model",
        value: (reference.ref_contact_no),
        expression: "reference.ref_contact_no"
      }],
      staticClass: "form-control input-sm pull-left",
      attrs: {
        "type": "text"
      },
      domProps: {
        "value": (reference.ref_contact_no)
      },
      on: {
        "input": function($event) {
          if ($event.target.composing) { return; }
          reference.ref_contact_no = $event.target.value
        }
      }
    })]), _vm._v(" "), _c('td', [(index == 0) ? _c('button', {
      staticClass: "glyphicon glyphicon-plus",
      on: {
        "click": function($event) {
          _vm.addReference()
        }
      }
    }) : _c('button', {
      staticClass: "glyphicon glyphicon-remove",
      on: {
        "click": function($event) {
          _vm.removeReference(index)
        }
      }
    })])])
  }))]), _vm._v(" "), _c('div', {
    staticClass: "form-group"
  }, [_c('div', {
    staticClass: "col-md-12 text-right"
  }, [_c('button', {
    staticClass: "btn btn-default",
    on: {
      "click": function($event) {
        $event.preventDefault();
        _vm.update($event)
      }
    }
  }, [_vm._v("Save")]), _vm._v(" "), _c('button', {
    staticClass: "btn btn-default"
  }, [_vm._v("Print")])])])])])])])
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('thead', [_c('tr', [_c('td', [_vm._v("Name")]), _vm._v(" "), _c('td', [_vm._v("Position")]), _vm._v(" "), _c('td', [_vm._v("Company")]), _vm._v(" "), _c('td', [_vm._v("Address")]), _vm._v(" "), _c('td', [_vm._v("Contact Number")])])])
}]}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-743282d6", module.exports)
  }
}

/***/ }),
/* 24 */
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-md-12"
  }, [_c('div', {
    staticClass: "panel panel-default"
  }, [_c('div', {
    staticClass: "panel-body"
  }, [_c('table', {
    staticClass: "table"
  }, [_vm._m(0), _vm._v(" "), _c('tbody', _vm._l((_vm.trainings), function(training, index) {
    return _c('tr', [_c('td', [_c('input', {
      directives: [{
        name: "model",
        rawName: "v-model",
        value: (training.training_title),
        expression: "training.training_title"
      }],
      staticClass: "form-control input-sm pull-left",
      attrs: {
        "type": "text"
      },
      domProps: {
        "value": (training.training_title)
      },
      on: {
        "input": function($event) {
          if ($event.target.composing) { return; }
          training.training_title = $event.target.value
        }
      }
    })]), _vm._v(" "), _c('td', [_c('input', {
      directives: [{
        name: "model",
        rawName: "v-model",
        value: (training.training_description),
        expression: "training.training_description"
      }],
      staticClass: "form-control input-sm pull-left",
      attrs: {
        "type": "text"
      },
      domProps: {
        "value": (training.training_description)
      },
      on: {
        "input": function($event) {
          if ($event.target.composing) { return; }
          training.training_description = $event.target.value
        }
      }
    })]), _vm._v(" "), _c('td', [_c('input', {
      directives: [{
        name: "model",
        rawName: "v-model",
        value: (training.training_provider),
        expression: "training.training_provider"
      }],
      staticClass: "form-control input-sm pull-left",
      attrs: {
        "type": "text"
      },
      domProps: {
        "value": (training.training_provider)
      },
      on: {
        "input": function($event) {
          if ($event.target.composing) { return; }
          training.training_provider = $event.target.value
        }
      }
    })]), _vm._v(" "), _c('td', [_c('input', {
      directives: [{
        name: "model",
        rawName: "v-model",
        value: (training.training_date_from),
        expression: "training.training_date_from"
      }],
      staticClass: "form-control input-sm pull-left",
      attrs: {
        "type": "text"
      },
      domProps: {
        "value": (training.training_date_from)
      },
      on: {
        "input": function($event) {
          if ($event.target.composing) { return; }
          training.training_date_from = $event.target.value
        }
      }
    })]), _vm._v(" "), _c('td', [_c('input', {
      directives: [{
        name: "model",
        rawName: "v-model",
        value: (training.training_date_to),
        expression: "training.training_date_to"
      }],
      staticClass: "form-control input-sm pull-left",
      attrs: {
        "type": "text"
      },
      domProps: {
        "value": (training.training_date_to)
      },
      on: {
        "input": function($event) {
          if ($event.target.composing) { return; }
          training.training_date_to = $event.target.value
        }
      }
    })]), _vm._v(" "), _c('td', [(index == 0) ? _c('button', {
      staticClass: "glyphicon glyphicon-plus",
      on: {
        "click": function($event) {
          _vm.addTraining()
        }
      }
    }) : _c('button', {
      staticClass: "glyphicon glyphicon-remove",
      on: {
        "click": function($event) {
          _vm.removeTraining(index)
        }
      }
    })])])
  }))]), _vm._v(" "), _c('div', {
    staticClass: "form-group"
  }, [_c('div', {
    staticClass: "col-md-12 text-right"
  }, [_c('button', {
    staticClass: "btn btn-default",
    on: {
      "click": function($event) {
        $event.preventDefault();
        _vm.update($event)
      }
    }
  }, [_vm._v("Save")]), _vm._v(" "), _c('button', {
    staticClass: "btn btn-default"
  }, [_vm._v("Print")])])])])])])])
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('thead', [_c('tr', [_c('td', [_vm._v("Course / Seminar Title")]), _vm._v(" "), _c('td', [_vm._v("Brief Description")]), _vm._v(" "), _c('td', [_vm._v("Training Provider")]), _vm._v(" "), _c('td', [_vm._v("Date From")]), _vm._v(" "), _c('td', [_vm._v("Date To")])])])
}]}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-7f29dee8", module.exports)
  }
}

/***/ }),
/* 25 */,
/* 26 */,
/* 27 */,
/* 28 */
/***/ (function(module, exports, __webpack_require__) {

var app = new Vue({
    el: '#div-employee-details',
    components: {
        "employeeIndex": __webpack_require__(44)
    },
    directive: {}
});

/***/ }),
/* 29 */,
/* 30 */,
/* 31 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__sub_components_employee_details_vue__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__sub_components_employee_details_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__sub_components_employee_details_vue__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__sub_components_char_reference_vue__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__sub_components_char_reference_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__sub_components_char_reference_vue__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__sub_components_employment_history_vue__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__sub_components_employment_history_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2__sub_components_employment_history_vue__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__sub_components_compensation_benefits_vue__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__sub_components_compensation_benefits_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3__sub_components_compensation_benefits_vue__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__sub_components_dependents_vue__ = __webpack_require__(46);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__sub_components_dependents_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4__sub_components_dependents_vue__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__sub_components_educational_background_vue__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__sub_components_educational_background_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5__sub_components_educational_background_vue__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__sub_components_family_background_vue__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__sub_components_family_background_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6__sub_components_family_background_vue__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__sub_components_job_description_vue__ = __webpack_require__(48);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__sub_components_job_description_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7__sub_components_job_description_vue__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__sub_components_personal_information_vue__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__sub_components_personal_information_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_8__sub_components_personal_information_vue__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__sub_components_requirements_vue__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__sub_components_requirements_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_9__sub_components_requirements_vue__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__sub_components_skills_vue__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__sub_components_skills_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_10__sub_components_skills_vue__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__sub_components_trainings_vue__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__sub_components_trainings_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_11__sub_components_trainings_vue__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//













/* harmony default export */ __webpack_exports__["default"] = ({
    props: ['employee_number', 'departments', 'employee_types', 'pay_classes', 'job_positions', 'base_url'],
    name: "employeeIndex",
    components: {
        employeeDetails: __WEBPACK_IMPORTED_MODULE_0__sub_components_employee_details_vue___default.a,
        characterReference: __WEBPACK_IMPORTED_MODULE_1__sub_components_char_reference_vue___default.a,
        employmentHistory: __WEBPACK_IMPORTED_MODULE_2__sub_components_employment_history_vue___default.a,
        compensationBenefits: __WEBPACK_IMPORTED_MODULE_3__sub_components_compensation_benefits_vue___default.a,
        dependents: __WEBPACK_IMPORTED_MODULE_4__sub_components_dependents_vue___default.a,
        educationalBackground: __WEBPACK_IMPORTED_MODULE_5__sub_components_educational_background_vue___default.a,
        familyBackground: __WEBPACK_IMPORTED_MODULE_6__sub_components_family_background_vue___default.a,
        jobDescription: __WEBPACK_IMPORTED_MODULE_7__sub_components_job_description_vue___default.a,
        personalInformation: __WEBPACK_IMPORTED_MODULE_8__sub_components_personal_information_vue___default.a,
        requirements: __WEBPACK_IMPORTED_MODULE_9__sub_components_requirements_vue___default.a,
        skills: __WEBPACK_IMPORTED_MODULE_10__sub_components_skills_vue___default.a,
        trainings: __WEBPACK_IMPORTED_MODULE_11__sub_components_trainings_vue___default.a
    },
    mounted: function mounted() {},
    data: function data() {
        return {
            "tabs": {
                personalInformation: {
                    name: "Personal Information",
                    file: "personal-information"
                },
                compensationBenefits: {
                    name: "Compensation and Benefits",
                    file: "compensation-benefits"
                },
                characterReference: {
                    name: "Character Reference",
                    file: "character-reference"
                },
                dependents: {
                    name: "Dependents",
                    file: "dependents"
                },
                jobDescription: {
                    name: "Job Description",
                    file: "job-description"
                },
                employmentHistory: {
                    name: "Employment History",
                    file: "employment-history"
                },
                educationalBackground: {
                    name: "Educational Background",
                    file: "educational-background"
                },
                familyBackground: {
                    name: "Family Background",
                    file: "family-background"
                },
                requirements: {
                    name: "Requirements",
                    file: "requirements"
                },
                trainings: {
                    name: "Trainings",
                    file: "trainings"
                },
                skills: {
                    name: "Skills",
                    file: "skills"
                }
            },
            "activeTab": "familyBackground",
            "processing": false,
            "employee_details": {}
        };
    },

    methods: {
        switchTab: function switchTab(tab) {
            this.activeTab = tab;
        },
        updateEmployeeDetails: function updateEmployeeDetails(data) {
            this.employee_details = data;
        },
        updateData: function updateData(data) {
            var params = {
                employee_details: this.employee_details,
                to_update: this.activeTab,
                data_for_update: data
            };
            this.$http.post(this.base_url + '/employee-details', params).then(function (response) {
                if (response.body.success) {
                    swal("", response.body.message, "success");
                } else {
                    swal("", response.body.message, "error");

                    // this.decodeErrors(response.body.errors);    
                    swal({
                        title: "Please fill required fields",
                        text: "<ul class='list-unstyled'><li>" + response.body.errors[Object.keys(response.body.errors)[0]][0] + "</li></ul>",
                        type: "error",
                        html: true
                    });
                }
            }, function (response) {
                swal("Error", "Something went wrong!", "error");
            });
        },
        decodeErrors: function decodeErrors(errors) {
            // var errors = Object.keys(errors);
            // for (var i = 0; i < errors.length; i++) {
            //     console.log(errors[i].split("."));
            // }
        }
    },
    watch: {
        processing: function processing() {
            if (this.processing) {}
        }
    }
});

/***/ }),
/* 32 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    name: "compensationBenefits",
    props: ['employee_number', 'base_url'],
    mounted: function mounted() {
        var that = this;
        this.$http.post(that.base_url + '/get-employee-info', { employee_number: that.employee_number, to_get: "compensationBenefits" }).then(function (response) {
            if (response.body) {
                delete response.body.comp_employee_id;
                delete response.body.comp_id;
                that.compensation_and_benefits = response.body;
            }
        }, function (response) {
            swal("Error", "Something went wrong!", "error");
        });
    },
    data: function data() {
        return {
            compensation_and_benefits: {
                comp_basic_pay: "",
                comp_standard_allowance: "",
                comp_special_pay: "",
                comp_meal_allowance: "",
                comp_others: "",
                comp_amount: "",
                comp_cash_bond: "",
                comp_semi_monthly_rate: "",
                comp_sss: "",
                comp_pagibig: "",
                comp_philhealth: "",
                comp_tax: ""
            }
        };
    },

    methods: {
        update: function update() {
            this.$emit("detailsToSave", this.compensation_and_benefits);
        }
    }

});

/***/ }),
/* 33 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    name: "dependents",
    props: ['employee_number', 'base_url'],
    data: function data() {
        return {
            dependents: [{
                dep_name: "",
                dep_relationship: "",
                dep_bdate: "",
                dep_age: "",
                dep_gender: "",
                dep_disability: ""
            }]
        };
    },
    mounted: function mounted() {
        var that = this;
        this.$http.post(that.base_url + '/get-employee-info', { employee_number: that.employee_number, to_get: "dependents" }).then(function (response) {
            if (response.body) {
                var dependents = response.body;
                if (dependents.length != 0) {
                    for (var i = 0; i < dependents.length; i++) {
                        delete dependents[i]['dep_employee_id'];
                        delete dependents[i]['dep_id'];
                    }
                    that.dependents = dependents;
                }
            }
        }, function (response) {
            swal("Error", "Something went wrong!", "error");
        });

        $(".datepicker").datepicker({
            onSelect: function onSelect(date) {
                console.log($(this).data("index"));
            },
            "dateFormat": 'yy-mm-dd'
        });
    },

    methods: {
        addDependent: function addDependent() {
            this.dependents.push({
                dep_name: "",
                dep_relationship: "",
                dep_bdate: "",
                dep_age: "",
                dep_gender: "",
                dep_disability: ""
            });
        },
        removeDependent: function removeDependent(index) {
            this.dependents.splice(index, 1);
        },
        update: function update() {
            this.$emit("detailsToSave", this.dependents);
        }
    }
});

/***/ }),
/* 34 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    name: "employeeDetails",
    props: ["employee_number", "departments", "employee_types", "pay_classes", "job_positions", "base_url"],
    mounted: function mounted() {
        var that = this;
        this.$http.post(that.base_url + '/get-employee-details', { employee_number: that.employee_number }).then(function (response) {
            that.employee_details = {
                'employee_number': that.employee_number,
                'photo': "asdasd.jpg",
                'lastname': response.body.emp_lastname,
                'middlename': response.body.emp_middlename,
                'firstname': response.body.emp_firstname,
                'department': response.body.emp_dept_id,
                'hiring_date': response.body.emp_hiring_date,
                'position': response.body.emp_position_id,
                'contract_duration_from': response.body.emp_contract_from,
                'contract_duration_to': response.body.emp_contract_to,
                'payclass': response.body.emp_class_id,
                'work_schedule': response.body.emp_work_sched_id,
                'employee_status': response.body.emp_status,
                'employee_type': response.body.emp_type_id,
                'monthly_rate': response.body.emp_monthly_rate,
                'semi_monthly_rate': response.body.emp_semi_monthly_rate,
                'daily_rate': response.body.emp_daily_rate,
                'hourly_rate': response.body.emp_hourly_rate
            };
        }, function (response) {
            swal("Error", "Something went wrong!", "error");
        });
        $("#hiring_date").datepicker({
            onSelect: function onSelect(date) {
                that.employee_details.hiring_date = date;
            },
            "dateFormat": 'yy-mm-dd'
        });
        $("#duration_from").datepicker({
            onSelect: function onSelect(date) {
                that.employee_details.contract_duration_from = date;
            },
            "dateFormat": 'yy-mm-dd'
        });
        $("#duration_to").datepicker({
            onSelect: function onSelect(date) {
                that.employee_details.contract_duration_to = date;
            },
            "dateFormat": 'yy-mm-dd'
        });
    },
    data: function data() {
        return {
            "mutated_departments": JSON.parse(this.departments),
            "mutated_employee_types": JSON.parse(this.employee_types),
            "mutated_pay_classes": JSON.parse(this.pay_classes),
            "mutated_job_positions": JSON.parse(this.job_positions),
            'employee_details': {
                'employee_number': "",
                'photo': "asdasd.jpg",
                'active': "",
                'lastname': "",
                'middlename': "",
                'firstname': "",
                'department': "",
                'hiring_date': "",
                'position': "",
                'contract_duration_from': "",
                'contract_duration_to': "",
                'payclass': "",
                'work_schedule': "",
                'employee_status': "",
                'employee_type': "",
                'monthly_rate': "",
                'semi_monthly_rate': "",
                'daily_rate': "",
                'hourly_rate': ""
            }

        };
    },

    methods: {
        passEmployeeDetails: function passEmployeeDetails() {
            this.$emit('employeeDetails', this.employee_details);
        },
        onFileChange: function onFileChange(e) {
            var files = e.target.files;
            if (!files.length) return;
            //                this.$set(this.employee_details,"photo",files[0]);
            console.log(files[0]);
            var formdata = new FormData();
            formdata.append('Content-Type', 'image/');
            formdata.append('photo', files[0]);
            this.$http.post('/admin/personnel-information/test', formdata).then(function (response) {
                console.log(response.body);
            }, function (response) {
                // error callback
            });
        },
        sample: function sample() {
            swal("asdasd", "asdasdasda", "success");
        }
    },
    watch: {
        'employee_details': {
            handler: function handler(val) {
                this.passEmployeeDetails();
            },

            deep: true
        }
    }
});

/***/ }),
/* 35 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    name: "jobDescription",
    props: ['employee_number', 'base_url'],
    mounted: function mounted() {
        var that = this;
        this.$http.post(that.base_url + '/get-employee-info', { employee_number: that.employee_number, to_get: "jobDescription" }).then(function (response) {
            if (response.body) {
                for (var i = 0; i < response.body.responsibilities.length; i++) {
                    delete response.body.responsibilities[i]['job_res_id'];
                    delete response.body.responsibilities[i]['job_description_id'];
                }
                that.job_descriptions = response.body;
            }
        }, function (response) {
            swal("Error", "Something went wrong!", "error");
        });
    },
    data: function data() {
        return {
            job_descriptions: {
                jd_project: "",
                jd_superior: "",
                responsibilities: [{
                    job_res_description: ""
                }]
            }
        };
    },

    methods: {
        addResponsibility: function addResponsibility() {
            this.job_descriptions.responsibilities.push({
                job_res_description: ""
            });
        },
        removeResponsibility: function removeResponsibility(index) {
            this.job_descriptions.responsibilities.splice(index, 1);
        },
        update: function update() {
            this.$emit("detailsToSave", this.job_descriptions);
        }
    }
});

/***/ }),
/* 36 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    name: "requirements",
    props: ['employee_number', 'base_url'],
    mounted: function mounted() {
        var that = this;
        this.$http.post(that.base_url + '/get-employee-info', { employee_number: that.employee_number, to_get: "requirements" }).then(function (response) {
            if (response.body.length != 0) {
                var primary = [];
                var other = [];
                for (var i = 0; i < response.body.length; i++) {
                    if (response.body[i].req_type == 1) {
                        primary.push(response.body[i].req_name);
                    } else if (response.body[i].req_type == 2) {
                        other.push({
                            req_name: response.body[i].req_name
                        });
                    }
                }
                if (other.length != 0) {
                    that.others = true;
                }
                that.requirements.primary = primary;
                that.requirements.other = other;
            }
        }, function (response) {
            swal("Error", "Something went wrong!", "error");
        });
    },
    data: function data() {
        return {
            others: false,
            requirements: {
                primary: [],
                other: []
            }

        };
    },

    methods: {
        addOtherReq: function addOtherReq() {
            this.requirements.other.push({
                req_name: ""
            });
        },
        removeOtherReq: function removeOtherReq(index) {
            this.requirements.other.splice(index, 1);
        },
        update: function update() {
            this.$emit("detailsToSave", this.requirements);
        }
    },
    watch: {
        "others": function others() {
            if (this.others == true) {
                this.requirements.other = [{
                    req_name: ""
                }];
            } else {
                this.requirements.other = [];
            }
        }
    }
});

/***/ }),
/* 37 */,
/* 38 */,
/* 39 */,
/* 40 */,
/* 41 */,
/* 42 */,
/* 43 */,
/* 44 */
/***/ (function(module, exports, __webpack_require__) {

var Component = __webpack_require__(0)(
  /* script */
  __webpack_require__(31),
  /* template */
  __webpack_require__(55),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "C:\\laragon\\www\\buildup\\resources\\assets\\js\\components\\personnel-information\\employee-details\\employee-index.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] employee-index.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-671dc40e", Component.options)
  } else {
    hotAPI.reload("data-v-671dc40e", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),
/* 45 */
/***/ (function(module, exports, __webpack_require__) {

var Component = __webpack_require__(0)(
  /* script */
  __webpack_require__(32),
  /* template */
  __webpack_require__(52),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "C:\\laragon\\www\\buildup\\resources\\assets\\js\\components\\personnel-information\\employee-details\\sub-components\\compensation-benefits.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] compensation-benefits.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-4259695e", Component.options)
  } else {
    hotAPI.reload("data-v-4259695e", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),
/* 46 */
/***/ (function(module, exports, __webpack_require__) {

var Component = __webpack_require__(0)(
  /* script */
  __webpack_require__(33),
  /* template */
  __webpack_require__(51),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "C:\\laragon\\www\\buildup\\resources\\assets\\js\\components\\personnel-information\\employee-details\\sub-components\\dependents.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] dependents.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-36cf4ed5", Component.options)
  } else {
    hotAPI.reload("data-v-36cf4ed5", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),
/* 47 */
/***/ (function(module, exports, __webpack_require__) {

var Component = __webpack_require__(0)(
  /* script */
  __webpack_require__(34),
  /* template */
  __webpack_require__(50),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "C:\\laragon\\www\\buildup\\resources\\assets\\js\\components\\personnel-information\\employee-details\\sub-components\\employee-details.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] employee-details.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-1d079564", Component.options)
  } else {
    hotAPI.reload("data-v-1d079564", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),
/* 48 */
/***/ (function(module, exports, __webpack_require__) {

var Component = __webpack_require__(0)(
  /* script */
  __webpack_require__(35),
  /* template */
  __webpack_require__(54),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "C:\\laragon\\www\\buildup\\resources\\assets\\js\\components\\personnel-information\\employee-details\\sub-components\\job-description.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] job-description.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-5e00f33b", Component.options)
  } else {
    hotAPI.reload("data-v-5e00f33b", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),
/* 49 */
/***/ (function(module, exports, __webpack_require__) {

var Component = __webpack_require__(0)(
  /* script */
  __webpack_require__(36),
  /* template */
  __webpack_require__(57),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "C:\\laragon\\www\\buildup\\resources\\assets\\js\\components\\personnel-information\\employee-details\\sub-components\\requirements.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] requirements.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-bb06dc5e", Component.options)
  } else {
    hotAPI.reload("data-v-bb06dc5e", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),
/* 50 */
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-md-12"
  }, [_c('div', {
    staticClass: "panel panel-default"
  }, [_c('div', {
    staticClass: "panel-body"
  }, [_c('form', {
    staticClass: "form-horizontal"
  }, [_c('div', {
    staticClass: "col-md-9"
  }, [_c('div', {
    staticClass: "form-group"
  }, [_vm._m(0), _vm._v(" "), _c('div', {
    staticClass: "col-md-3"
  }, [_c('input', {
    staticClass: "form-control input-sm pull-left",
    attrs: {
      "type": "text",
      "disabled": ""
    },
    domProps: {
      "value": _vm.employee_details.employee_number
    }
  })]), _vm._v(" "), _c('div', {
    staticClass: "col-md-2"
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.employee_details.active),
      expression: "employee_details.active"
    }],
    attrs: {
      "type": "checkbox"
    },
    domProps: {
      "checked": Array.isArray(_vm.employee_details.active) ? _vm._i(_vm.employee_details.active, null) > -1 : (_vm.employee_details.active)
    },
    on: {
      "__c": function($event) {
        var $$a = _vm.employee_details.active,
          $$el = $event.target,
          $$c = $$el.checked ? (true) : (false);
        if (Array.isArray($$a)) {
          var $$v = null,
            $$i = _vm._i($$a, $$v);
          if ($$c) {
            $$i < 0 && (_vm.employee_details.active = $$a.concat($$v))
          } else {
            $$i > -1 && (_vm.employee_details.active = $$a.slice(0, $$i).concat($$a.slice($$i + 1)))
          }
        } else {
          _vm.employee_details.active = $$c
        }
      }
    }
  }), _vm._v(" "), _c('label', [_vm._v("Activate")])])]), _vm._v(" "), _c('div', {
    staticClass: "form-group"
  }, [_vm._m(1), _vm._v(" "), _c('div', {
    staticClass: "col-md-3"
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.employee_details.lastname),
      expression: "employee_details.lastname"
    }],
    staticClass: "form-control input-sm pull-left",
    attrs: {
      "type": "text"
    },
    domProps: {
      "value": (_vm.employee_details.lastname)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.employee_details.lastname = $event.target.value
      }
    }
  })]), _vm._v(" "), _c('div', {
    staticClass: "col-md-3"
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.employee_details.middlename),
      expression: "employee_details.middlename"
    }],
    staticClass: "form-control input-sm pull-left",
    attrs: {
      "type": "text"
    },
    domProps: {
      "value": (_vm.employee_details.middlename)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.employee_details.middlename = $event.target.value
      }
    }
  })]), _vm._v(" "), _c('div', {
    staticClass: "col-md-3"
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.employee_details.firstname),
      expression: "employee_details.firstname"
    }],
    staticClass: "form-control input-sm pull-left",
    attrs: {
      "type": "text"
    },
    domProps: {
      "value": (_vm.employee_details.firstname)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.employee_details.firstname = $event.target.value
      }
    }
  })])])]), _vm._v(" "), _c('div', {
    staticClass: "col-md-3 text-center"
  }, [_c('img', {
    attrs: {
      "src": "https://placeholdit.co//i/100x100"
    }
  }), _vm._v(" "), _c('input', {
    attrs: {
      "type": "file"
    },
    on: {
      "change": _vm.onFileChange
    }
  })]), _vm._v(" "), _c('div', {
    staticClass: "clearfix"
  }), _vm._v(" "), _c('hr'), _vm._v(" "), _c('div', {
    staticClass: "form-group col-md-12"
  }, [_vm._m(2), _vm._v(" "), _c('div', {
    staticClass: "col-md-3"
  }, [_c('select', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.employee_details.department),
      expression: "employee_details.department"
    }],
    staticClass: "form-control input-sm pull-left",
    on: {
      "change": function($event) {
        var $$selectedVal = Array.prototype.filter.call($event.target.options, function(o) {
          return o.selected
        }).map(function(o) {
          var val = "_value" in o ? o._value : o.value;
          return val
        });
        _vm.employee_details.department = $event.target.multiple ? $$selectedVal : $$selectedVal[0]
      }
    }
  }, _vm._l((_vm.mutated_departments), function(department) {
    return _c('option', {
      domProps: {
        "value": department.dept_id
      }
    }, [_vm._v(_vm._s(department.dept_name))])
  }))]), _vm._v(" "), _vm._m(3), _vm._v(" "), _c('div', {
    staticClass: "col-md-3"
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.employee_details.hiring_date),
      expression: "employee_details.hiring_date"
    }],
    staticClass: "datepicker form-control input-sm pull-left",
    attrs: {
      "type": "text",
      "id": "hiring_date"
    },
    domProps: {
      "value": (_vm.employee_details.hiring_date)
    },
    on: {
      "change": _vm.sample,
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.employee_details.hiring_date = $event.target.value
      }
    }
  })])]), _vm._v(" "), _c('div', {
    staticClass: "form-group col-md-12"
  }, [_vm._m(4), _vm._v(" "), _c('div', {
    staticClass: "col-md-3"
  }, [_c('select', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.employee_details.position),
      expression: "employee_details.position"
    }],
    staticClass: "form-control input-sm pull-left",
    on: {
      "change": function($event) {
        var $$selectedVal = Array.prototype.filter.call($event.target.options, function(o) {
          return o.selected
        }).map(function(o) {
          var val = "_value" in o ? o._value : o.value;
          return val
        });
        _vm.employee_details.position = $event.target.multiple ? $$selectedVal : $$selectedVal[0]
      }
    }
  }, _vm._l((_vm.mutated_job_positions), function(job_position) {
    return _c('option', {
      domProps: {
        "value": job_position.job_id
      }
    }, [_vm._v(_vm._s(job_position.job_title))])
  }))]), _vm._v(" "), _vm._m(5), _vm._v(" "), _c('div', {
    staticClass: "col-md-2"
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.employee_details.contract_duration_from),
      expression: "employee_details.contract_duration_from"
    }],
    staticClass: "datepicker form-control input-sm pull-left",
    attrs: {
      "type": "text",
      "id": "duration_from"
    },
    domProps: {
      "value": (_vm.employee_details.contract_duration_from)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.employee_details.contract_duration_from = $event.target.value
      }
    }
  })]), _vm._v(" "), _vm._m(6), _vm._v(" "), _c('div', {
    staticClass: "col-md-2"
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.employee_details.contract_duration_to),
      expression: "employee_details.contract_duration_to"
    }],
    staticClass: "datepicker form-control input-sm pull-left",
    attrs: {
      "type": "text",
      "id": "duration_to"
    },
    domProps: {
      "value": (_vm.employee_details.contract_duration_to)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.employee_details.contract_duration_to = $event.target.value
      }
    }
  })])]), _vm._v(" "), _c('div', {
    staticClass: "form-group col-md-12"
  }, [_vm._m(7), _vm._v(" "), _c('div', {
    staticClass: "col-md-3"
  }, [_c('select', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.employee_details.payclass),
      expression: "employee_details.payclass"
    }],
    staticClass: "form-control input-sm pull-left",
    on: {
      "change": function($event) {
        var $$selectedVal = Array.prototype.filter.call($event.target.options, function(o) {
          return o.selected
        }).map(function(o) {
          var val = "_value" in o ? o._value : o.value;
          return val
        });
        _vm.employee_details.payclass = $event.target.multiple ? $$selectedVal : $$selectedVal[0]
      }
    }
  }, _vm._l((_vm.mutated_pay_classes), function(pay_class) {
    return _c('option', {
      domProps: {
        "value": pay_class.class_id
      }
    }, [_vm._v(_vm._s(pay_class.class_title))])
  }))]), _vm._v(" "), _vm._m(8), _vm._v(" "), _c('div', {
    staticClass: "col-md-3"
  }, [_c('select', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.employee_details.work_schedule),
      expression: "employee_details.work_schedule"
    }],
    staticClass: "form-control input-sm pull-left",
    on: {
      "change": function($event) {
        var $$selectedVal = Array.prototype.filter.call($event.target.options, function(o) {
          return o.selected
        }).map(function(o) {
          var val = "_value" in o ? o._value : o.value;
          return val
        });
        _vm.employee_details.work_schedule = $event.target.multiple ? $$selectedVal : $$selectedVal[0]
      }
    }
  }, [_c('option', {
    attrs: {
      "value": "1"
    }
  }, [_vm._v("Flexi")]), _vm._v(" "), _c('option', {
    attrs: {
      "value": "2"
    }
  }, [_vm._v("Compressed")]), _vm._v(" "), _c('option', {
    attrs: {
      "value": "3"
    }
  }, [_vm._v("Special")])])])]), _vm._v(" "), _c('div', {
    staticClass: "form-group col-md-12"
  }, [_vm._m(9), _vm._v(" "), _c('div', {
    staticClass: "col-md-3"
  }, [_c('select', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.employee_details.employee_status),
      expression: "employee_details.employee_status"
    }],
    staticClass: "form-control input-sm pull-left",
    on: {
      "change": function($event) {
        var $$selectedVal = Array.prototype.filter.call($event.target.options, function(o) {
          return o.selected
        }).map(function(o) {
          var val = "_value" in o ? o._value : o.value;
          return val
        });
        _vm.employee_details.employee_status = $event.target.multiple ? $$selectedVal : $$selectedVal[0]
      }
    }
  }, [_c('option', {
    attrs: {
      "value": "1"
    }
  }, [_vm._v("Active")]), _vm._v(" "), _c('option', {
    attrs: {
      "value": "0"
    }
  }, [_vm._v("Inactive")])])]), _vm._v(" "), _vm._m(10), _vm._v(" "), _c('div', {
    staticClass: "col-md-3"
  }, [_c('select', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.employee_details.employee_type),
      expression: "employee_details.employee_type"
    }],
    staticClass: "form-control input-sm pull-left",
    on: {
      "change": function($event) {
        var $$selectedVal = Array.prototype.filter.call($event.target.options, function(o) {
          return o.selected
        }).map(function(o) {
          var val = "_value" in o ? o._value : o.value;
          return val
        });
        _vm.employee_details.employee_type = $event.target.multiple ? $$selectedVal : $$selectedVal[0]
      }
    }
  }, _vm._l((_vm.mutated_employee_types), function(employee_type) {
    return _c('option', {
      domProps: {
        "value": employee_type.type_id
      }
    }, [_vm._v(_vm._s(employee_type.type_title))])
  }))])]), _vm._v(" "), _c('div', {
    staticClass: "form-group col-md-12"
  }, [_vm._m(11), _vm._v(" "), _c('div', {
    staticClass: "col-md-3"
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.employee_details.monthly_rate),
      expression: "employee_details.monthly_rate"
    }],
    staticClass: "form-control input-sm pull-left",
    attrs: {
      "type": "text"
    },
    domProps: {
      "value": (_vm.employee_details.monthly_rate)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.employee_details.monthly_rate = $event.target.value
      }
    }
  })]), _vm._v(" "), _vm._m(12), _vm._v(" "), _c('div', {
    staticClass: "col-md-3"
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.employee_details.semi_monthly_rate),
      expression: "employee_details.semi_monthly_rate"
    }],
    staticClass: "form-control input-sm pull-left",
    attrs: {
      "type": "text"
    },
    domProps: {
      "value": (_vm.employee_details.semi_monthly_rate)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.employee_details.semi_monthly_rate = $event.target.value
      }
    }
  })])]), _vm._v(" "), _c('div', {
    staticClass: "form-group col-md-12"
  }, [_vm._m(13), _vm._v(" "), _c('div', {
    staticClass: "col-md-3"
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.employee_details.daily_rate),
      expression: "employee_details.daily_rate"
    }],
    staticClass: "form-control input-sm pull-left",
    attrs: {
      "type": "text"
    },
    domProps: {
      "value": (_vm.employee_details.daily_rate)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.employee_details.daily_rate = $event.target.value
      }
    }
  })]), _vm._v(" "), _vm._m(14), _vm._v(" "), _c('div', {
    staticClass: "col-md-3"
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.employee_details.hourly_rate),
      expression: "employee_details.hourly_rate"
    }],
    staticClass: "form-control input-sm pull-left",
    attrs: {
      "type": "text"
    },
    domProps: {
      "value": (_vm.employee_details.hourly_rate)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.employee_details.hourly_rate = $event.target.value
      }
    }
  })])])])])])])])
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "col-md-3"
  }, [_c('label', [_vm._v("Employee Number : ")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "col-md-3"
  }, [_c('label', [_vm._v("Employee Name : ")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "col-md-2"
  }, [_c('label', [_vm._v("Department / Client : ")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "col-md-2"
  }, [_c('label', [_vm._v("Hiring Date : ")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "col-md-2"
  }, [_c('label', [_vm._v("Position : ")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "col-md-2"
  }, [_c('label', [_vm._v("Contract Duration : ")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "col-md-1"
  }, [_c('label', [_vm._v("To : ")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "col-md-2"
  }, [_c('label', [_vm._v("Payclass : ")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "col-md-2"
  }, [_c('label', [_vm._v("Work Schedule : ")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "col-md-2"
  }, [_c('label', [_vm._v("Employee Status : ")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "col-md-2"
  }, [_c('label', [_vm._v("Employee Type : ")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "col-md-2"
  }, [_c('label', [_vm._v("Monthly Rate : ")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "col-md-2"
  }, [_c('label', [_vm._v("Semi-Monthly Rate : ")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "col-md-2"
  }, [_c('label', [_vm._v("Daily Rate : ")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "col-md-2"
  }, [_c('label', [_vm._v("Hourly Rate : ")])])
}]}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-1d079564", module.exports)
  }
}

/***/ }),
/* 51 */
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-md-12"
  }, [_c('div', {
    staticClass: "panel panel-default"
  }, [_c('div', {
    staticClass: "panel-body"
  }, [_c('table', {
    staticClass: "table"
  }, [_vm._m(0), _vm._v(" "), _c('tbody', _vm._l((_vm.dependents), function(dependent, index) {
    return _c('tr', [_c('td', [_c('input', {
      directives: [{
        name: "model",
        rawName: "v-model",
        value: (dependent.dep_name),
        expression: "dependent.dep_name"
      }],
      staticClass: "form-control input-sm pull-left",
      attrs: {
        "type": "text"
      },
      domProps: {
        "value": (dependent.dep_name)
      },
      on: {
        "input": function($event) {
          if ($event.target.composing) { return; }
          dependent.dep_name = $event.target.value
        }
      }
    })]), _vm._v(" "), _c('td', [_c('input', {
      directives: [{
        name: "model",
        rawName: "v-model",
        value: (dependent.dep_relationship),
        expression: "dependent.dep_relationship"
      }],
      staticClass: "form-control input-sm pull-left",
      attrs: {
        "type": "text"
      },
      domProps: {
        "value": (dependent.dep_relationship)
      },
      on: {
        "input": function($event) {
          if ($event.target.composing) { return; }
          dependent.dep_relationship = $event.target.value
        }
      }
    })]), _vm._v(" "), _c('td', [_c('input', {
      directives: [{
        name: "model",
        rawName: "v-model",
        value: (dependent.dep_bdate),
        expression: "dependent.dep_bdate"
      }],
      staticClass: "form-control input-sm pull-left",
      attrs: {
        "type": "text"
      },
      domProps: {
        "value": (dependent.dep_bdate)
      },
      on: {
        "input": function($event) {
          if ($event.target.composing) { return; }
          dependent.dep_bdate = $event.target.value
        }
      }
    })]), _vm._v(" "), _c('td', [_c('input', {
      directives: [{
        name: "model",
        rawName: "v-model",
        value: (dependent.dep_age),
        expression: "dependent.dep_age"
      }],
      staticClass: "form-control input-sm pull-left",
      attrs: {
        "type": "text"
      },
      domProps: {
        "value": (dependent.dep_age)
      },
      on: {
        "input": function($event) {
          if ($event.target.composing) { return; }
          dependent.dep_age = $event.target.value
        }
      }
    })]), _vm._v(" "), _c('td', [_c('select', {
      directives: [{
        name: "model",
        rawName: "v-model",
        value: (dependent.dep_gender),
        expression: "dependent.dep_gender"
      }],
      staticClass: "form-control input-sm pull-left",
      on: {
        "change": function($event) {
          var $$selectedVal = Array.prototype.filter.call($event.target.options, function(o) {
            return o.selected
          }).map(function(o) {
            var val = "_value" in o ? o._value : o.value;
            return val
          });
          dependent.dep_gender = $event.target.multiple ? $$selectedVal : $$selectedVal[0]
        }
      }
    }, [_c('option', {
      attrs: {
        "value": "male"
      }
    }, [_vm._v("Male")]), _vm._v(" "), _c('option', {
      attrs: {
        "value": "female"
      }
    }, [_vm._v("Female")])])]), _vm._v(" "), _c('td', [_c('input', {
      directives: [{
        name: "model",
        rawName: "v-model",
        value: (dependent.dep_disability),
        expression: "dependent.dep_disability"
      }],
      staticClass: "form-control input-sm pull-left",
      attrs: {
        "type": "text"
      },
      domProps: {
        "value": (dependent.dep_disability)
      },
      on: {
        "input": function($event) {
          if ($event.target.composing) { return; }
          dependent.dep_disability = $event.target.value
        }
      }
    })]), _vm._v(" "), _c('td', [(index == 0) ? _c('button', {
      staticClass: "glyphicon glyphicon-plus",
      on: {
        "click": function($event) {
          _vm.addDependent()
        }
      }
    }) : _c('button', {
      staticClass: "glyphicon glyphicon-remove",
      on: {
        "click": function($event) {
          _vm.removeDependent(index)
        }
      }
    })])])
  }))]), _vm._v(" "), _c('div', {
    staticClass: "form-group"
  }, [_c('div', {
    staticClass: "col-md-12 text-right"
  }, [_c('button', {
    staticClass: "btn btn-default",
    on: {
      "click": function($event) {
        $event.preventDefault();
        _vm.update($event)
      }
    }
  }, [_vm._v("Save")]), _vm._v(" "), _c('button', {
    staticClass: "btn btn-default"
  }, [_vm._v("Print")])])])])])])])
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('thead', [_c('tr', [_c('td', [_vm._v("Name")]), _vm._v(" "), _c('td', [_vm._v("Relationship")]), _vm._v(" "), _c('td', [_vm._v("Birthdate")]), _vm._v(" "), _c('td', [_vm._v("Age")]), _vm._v(" "), _c('td', [_vm._v("Gender")]), _vm._v(" "), _c('td', [_vm._v("With Disability")])])])
}]}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-36cf4ed5", module.exports)
  }
}

/***/ }),
/* 52 */
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-md-12"
  }, [_c('div', {
    staticClass: "panel panel-default"
  }, [_c('div', {
    staticClass: "panel-body"
  }, [_c('form', {
    staticClass: "form-horizontal"
  }, [_c('div', {
    staticClass: "form-group"
  }, [_vm._m(0), _vm._v(" "), _c('div', {
    staticClass: "col-md-3"
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.compensation_and_benefits.comp_basic_pay),
      expression: "compensation_and_benefits.comp_basic_pay"
    }],
    staticClass: "form-control input-sm pull-left",
    attrs: {
      "type": "text"
    },
    domProps: {
      "value": (_vm.compensation_and_benefits.comp_basic_pay)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.compensation_and_benefits.comp_basic_pay = $event.target.value
      }
    }
  })]), _vm._v(" "), _vm._m(1), _vm._v(" "), _c('div', {
    staticClass: "col-md-3"
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.compensation_and_benefits.comp_standard_allowance),
      expression: "compensation_and_benefits.comp_standard_allowance"
    }],
    staticClass: "form-control input-sm pull-left",
    attrs: {
      "type": "text"
    },
    domProps: {
      "value": (_vm.compensation_and_benefits.comp_standard_allowance)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.compensation_and_benefits.comp_standard_allowance = $event.target.value
      }
    }
  })])]), _vm._v(" "), _c('div', {
    staticClass: "form-group"
  }, [_vm._m(2), _vm._v(" "), _c('div', {
    staticClass: "col-md-3"
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.compensation_and_benefits.comp_special_pay),
      expression: "compensation_and_benefits.comp_special_pay"
    }],
    staticClass: "form-control input-sm pull-left",
    attrs: {
      "type": "text"
    },
    domProps: {
      "value": (_vm.compensation_and_benefits.comp_special_pay)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.compensation_and_benefits.comp_special_pay = $event.target.value
      }
    }
  })]), _vm._v(" "), _vm._m(3), _vm._v(" "), _c('div', {
    staticClass: "col-md-3"
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.compensation_and_benefits.comp_meal_allowance),
      expression: "compensation_and_benefits.comp_meal_allowance"
    }],
    staticClass: "form-control input-sm pull-left",
    attrs: {
      "type": "text"
    },
    domProps: {
      "value": (_vm.compensation_and_benefits.comp_meal_allowance)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.compensation_and_benefits.comp_meal_allowance = $event.target.value
      }
    }
  })])]), _vm._v(" "), _c('div', {
    staticClass: "form-group"
  }, [_vm._m(4), _vm._v(" "), _c('div', {
    staticClass: "col-md-3"
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.compensation_and_benefits.comp_others),
      expression: "compensation_and_benefits.comp_others"
    }],
    staticClass: "form-control input-sm pull-left",
    attrs: {
      "type": "text"
    },
    domProps: {
      "value": (_vm.compensation_and_benefits.comp_others)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.compensation_and_benefits.comp_others = $event.target.value
      }
    }
  })]), _vm._v(" "), _vm._m(5), _vm._v(" "), _c('div', {
    staticClass: "col-md-3"
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.compensation_and_benefits.comp_amount),
      expression: "compensation_and_benefits.comp_amount"
    }],
    staticClass: "form-control input-sm pull-left",
    attrs: {
      "type": "text",
      "disabled": !_vm.compensation_and_benefits.comp_others
    },
    domProps: {
      "value": (_vm.compensation_and_benefits.comp_amount)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.compensation_and_benefits.comp_amount = $event.target.value
      }
    }
  })])]), _vm._v(" "), _c('div', {
    staticClass: "form-group"
  }, [_vm._m(6), _vm._v(" "), _c('div', {
    staticClass: "col-md-3"
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.compensation_and_benefits.comp_cash_bond),
      expression: "compensation_and_benefits.comp_cash_bond"
    }],
    staticClass: "form-control input-sm pull-left",
    attrs: {
      "type": "text"
    },
    domProps: {
      "value": (_vm.compensation_and_benefits.comp_cash_bond)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.compensation_and_benefits.comp_cash_bond = $event.target.value
      }
    }
  })]), _vm._v(" "), _vm._m(7), _vm._v(" "), _c('div', {
    staticClass: "col-md-3"
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.compensation_and_benefits.comp_semi_monthly_rate),
      expression: "compensation_and_benefits.comp_semi_monthly_rate"
    }],
    staticClass: "form-control input-sm pull-left",
    attrs: {
      "type": "text"
    },
    domProps: {
      "value": (_vm.compensation_and_benefits.comp_semi_monthly_rate)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.compensation_and_benefits.comp_semi_monthly_rate = $event.target.value
      }
    }
  })])]), _vm._v(" "), _c('div', {
    staticClass: "form-group"
  }, [_vm._m(8), _vm._v(" "), _c('div', {
    staticClass: "col-md-3"
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.compensation_and_benefits.comp_sss),
      expression: "compensation_and_benefits.comp_sss"
    }],
    staticClass: "form-control input-sm pull-left",
    attrs: {
      "type": "text"
    },
    domProps: {
      "value": (_vm.compensation_and_benefits.comp_sss)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.compensation_and_benefits.comp_sss = $event.target.value
      }
    }
  })]), _vm._v(" "), _vm._m(9), _vm._v(" "), _c('div', {
    staticClass: "col-md-3"
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.compensation_and_benefits.comp_pagibig),
      expression: "compensation_and_benefits.comp_pagibig"
    }],
    staticClass: "form-control input-sm pull-left",
    attrs: {
      "type": "text"
    },
    domProps: {
      "value": (_vm.compensation_and_benefits.comp_pagibig)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.compensation_and_benefits.comp_pagibig = $event.target.value
      }
    }
  })])]), _vm._v(" "), _c('div', {
    staticClass: "form-group"
  }, [_vm._m(10), _vm._v(" "), _c('div', {
    staticClass: "col-md-3"
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.compensation_and_benefits.comp_philhealth),
      expression: "compensation_and_benefits.comp_philhealth"
    }],
    staticClass: "form-control input-sm pull-left",
    attrs: {
      "type": "text"
    },
    domProps: {
      "value": (_vm.compensation_and_benefits.comp_philhealth)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.compensation_and_benefits.comp_philhealth = $event.target.value
      }
    }
  })]), _vm._v(" "), _vm._m(11), _vm._v(" "), _c('div', {
    staticClass: "col-md-3"
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.compensation_and_benefits.comp_tax),
      expression: "compensation_and_benefits.comp_tax"
    }],
    staticClass: "form-control input-sm pull-left",
    attrs: {
      "type": "text"
    },
    domProps: {
      "value": (_vm.compensation_and_benefits.comp_tax)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.compensation_and_benefits.comp_tax = $event.target.value
      }
    }
  })])]), _vm._v(" "), _c('div', {
    staticClass: "form-group"
  }, [_c('div', {
    staticClass: "col-md-12 text-right"
  }, [_c('button', {
    staticClass: "btn btn-default",
    on: {
      "click": function($event) {
        $event.preventDefault();
        _vm.update($event)
      }
    }
  }, [_vm._v("Save")]), _vm._v(" "), _c('button', {
    staticClass: "btn btn-default"
  }, [_vm._v("Print")])])])])])])])])
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "col-md-3"
  }, [_c('label', [_vm._v("Basic Pay : ")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "col-md-3"
  }, [_c('label', [_vm._v("Standard Allowance : ")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "col-md-3"
  }, [_c('label', [_vm._v("Special Pay : ")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "col-md-3"
  }, [_c('label', [_vm._v("Meal Allowance : ")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "col-md-3"
  }, [_c('label', [_vm._v("Others : ")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "col-md-3"
  }, [_c('label', [_vm._v("Amount : ")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "col-md-3"
  }, [_c('label', [_vm._v("Cash Bond : ")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "col-md-3"
  }, [_c('label', [_vm._v("Semi-Monthly Rate : ")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "col-md-3"
  }, [_c('label', [_vm._v("SSS : ")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "col-md-3"
  }, [_c('label', [_vm._v("Philhealth : ")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "col-md-3"
  }, [_c('label', [_vm._v("Pagibig : ")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "col-md-3"
  }, [_c('label', [_vm._v("Tax : ")])])
}]}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-4259695e", module.exports)
  }
}

/***/ }),
/* 53 */,
/* 54 */
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-md-12"
  }, [_c('div', {
    staticClass: "panel panel-default"
  }, [_c('div', {
    staticClass: "panel-body"
  }, [_c('div', {
    staticClass: "form-group"
  }, [_vm._m(0), _vm._v(" "), _c('div', {
    staticClass: "col-md-3"
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.job_descriptions.jd_project),
      expression: "job_descriptions.jd_project"
    }],
    staticClass: "form-control input-sm pull-left",
    attrs: {
      "type": "text"
    },
    domProps: {
      "value": (_vm.job_descriptions.jd_project)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.job_descriptions.jd_project = $event.target.value
      }
    }
  })]), _vm._v(" "), _vm._m(1), _vm._v(" "), _c('div', {
    staticClass: "col-md-3"
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.job_descriptions.jd_superior),
      expression: "job_descriptions.jd_superior"
    }],
    staticClass: "form-control input-sm pull-left",
    attrs: {
      "type": "text"
    },
    domProps: {
      "value": (_vm.job_descriptions.jd_superior)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.job_descriptions.jd_superior = $event.target.value
      }
    }
  })])]), _vm._v(" "), _c('table', {
    staticClass: "table"
  }, [_vm._m(2), _vm._v(" "), _c('tbody', _vm._l((_vm.job_descriptions.responsibilities), function(responsibility, index) {
    return _c('tr', [_c('td', [_c('input', {
      directives: [{
        name: "model",
        rawName: "v-model",
        value: (responsibility.job_res_description),
        expression: "responsibility.job_res_description"
      }],
      staticClass: "form-control input-sm pull-left",
      attrs: {
        "type": "text"
      },
      domProps: {
        "value": (responsibility.job_res_description)
      },
      on: {
        "input": function($event) {
          if ($event.target.composing) { return; }
          responsibility.job_res_description = $event.target.value
        }
      }
    })]), _vm._v(" "), _c('td', [(index == 0) ? _c('button', {
      staticClass: "glyphicon glyphicon-plus",
      on: {
        "click": function($event) {
          _vm.addResponsibility()
        }
      }
    }) : _c('button', {
      staticClass: "glyphicon glyphicon-remove",
      on: {
        "click": function($event) {
          _vm.removeResponsibility(index)
        }
      }
    })])])
  }))]), _vm._v(" "), _c('div', {
    staticClass: "form-group"
  }, [_c('div', {
    staticClass: "col-md-12 text-right"
  }, [_c('button', {
    staticClass: "btn btn-default",
    on: {
      "click": function($event) {
        $event.preventDefault();
        _vm.update($event)
      }
    }
  }, [_vm._v("Save")]), _vm._v(" "), _c('button', {
    staticClass: "btn btn-default"
  }, [_vm._v("Print")])])])])])])])
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "col-md-3"
  }, [_c('label', [_vm._v("Project : ")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "col-md-3"
  }, [_c('label', [_vm._v("Immediate Superior: ")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('thead', [_c('tr', [_c('td', [_vm._v("Job Responsibilities")])])])
}]}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-5e00f33b", module.exports)
  }
}

/***/ }),
/* 55 */
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "div"
  }, [_c('div', {
    staticClass: "col-md-2 div-sidebar"
  }, [_c('ul', {
    staticClass: "list-unstyled list-sidebar"
  }, _vm._l((_vm.tabs), function(value, key, index) {
    return _c('li', [_c('a', {
      domProps: {
        "textContent": _vm._s(value.name)
      },
      on: {
        "click": function($event) {
          $event.preventDefault();
          _vm.switchTab(key)
        }
      }
    })])
  }))]), _vm._v(" "), _c('div', {
    staticClass: "col-md-10"
  }, [_c('employee-details', {
    attrs: {
      "employee_number": _vm.employee_number,
      "departments": _vm.departments,
      "employee_types": _vm.employee_types,
      "pay_classes": _vm.pay_classes,
      "job_positions": _vm.job_positions,
      "base_url": _vm.base_url
    },
    on: {
      "employeeDetails": _vm.updateEmployeeDetails
    }
  }), _vm._v(" "), _vm._l((_vm.tabs), function(value, key, index) {
    return (_vm.activeTab == key) ? _c(value.file, {
      tag: "div",
      attrs: {
        "base_url": _vm.base_url,
        "employee_number": _vm.employee_number
      },
      on: {
        "detailsToSave": _vm.updateData
      }
    }) : _vm._e()
  })], 2)])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-671dc40e", module.exports)
  }
}

/***/ }),
/* 56 */,
/* 57 */
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-md-12"
  }, [_c('div', {
    staticClass: "panel panel-default"
  }, [_c('div', {
    staticClass: "panel-body"
  }, [_c('div', {
    staticClass: "col-md-4"
  }, [_c('ul', {
    staticClass: "list list-unstyled"
  }, [_c('li', [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.requirements.primary),
      expression: "requirements.primary"
    }],
    attrs: {
      "type": "checkbox",
      "value": "twobytwo"
    },
    domProps: {
      "checked": Array.isArray(_vm.requirements.primary) ? _vm._i(_vm.requirements.primary, "twobytwo") > -1 : (_vm.requirements.primary)
    },
    on: {
      "__c": function($event) {
        var $$a = _vm.requirements.primary,
          $$el = $event.target,
          $$c = $$el.checked ? (true) : (false);
        if (Array.isArray($$a)) {
          var $$v = "twobytwo",
            $$i = _vm._i($$a, $$v);
          if ($$c) {
            $$i < 0 && (_vm.requirements.primary = $$a.concat($$v))
          } else {
            $$i > -1 && (_vm.requirements.primary = $$a.slice(0, $$i).concat($$a.slice($$i + 1)))
          }
        } else {
          _vm.requirements.primary = $$c
        }
      }
    }
  }), _vm._v(" Colored Picture 2x2(2pcs)")]), _vm._v(" "), _c('li', [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.requirements.primary),
      expression: "requirements.primary"
    }],
    attrs: {
      "type": "checkbox",
      "value": "onebyone"
    },
    domProps: {
      "checked": Array.isArray(_vm.requirements.primary) ? _vm._i(_vm.requirements.primary, "onebyone") > -1 : (_vm.requirements.primary)
    },
    on: {
      "__c": function($event) {
        var $$a = _vm.requirements.primary,
          $$el = $event.target,
          $$c = $$el.checked ? (true) : (false);
        if (Array.isArray($$a)) {
          var $$v = "onebyone",
            $$i = _vm._i($$a, $$v);
          if ($$c) {
            $$i < 0 && (_vm.requirements.primary = $$a.concat($$v))
          } else {
            $$i > -1 && (_vm.requirements.primary = $$a.slice(0, $$i).concat($$a.slice($$i + 1)))
          }
        } else {
          _vm.requirements.primary = $$c
        }
      }
    }
  }), _vm._v(" Colored Picture 1x1(1pcs)")]), _vm._v(" "), _c('li', [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.requirements.primary),
      expression: "requirements.primary"
    }],
    attrs: {
      "type": "checkbox",
      "value": "xray"
    },
    domProps: {
      "checked": Array.isArray(_vm.requirements.primary) ? _vm._i(_vm.requirements.primary, "xray") > -1 : (_vm.requirements.primary)
    },
    on: {
      "__c": function($event) {
        var $$a = _vm.requirements.primary,
          $$el = $event.target,
          $$c = $$el.checked ? (true) : (false);
        if (Array.isArray($$a)) {
          var $$v = "xray",
            $$i = _vm._i($$a, $$v);
          if ($$c) {
            $$i < 0 && (_vm.requirements.primary = $$a.concat($$v))
          } else {
            $$i > -1 && (_vm.requirements.primary = $$a.slice(0, $$i).concat($$a.slice($$i + 1)))
          }
        } else {
          _vm.requirements.primary = $$c
        }
      }
    }
  }), _vm._v(" X-ray Result")]), _vm._v(" "), _c('li', [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.requirements.primary),
      expression: "requirements.primary"
    }],
    attrs: {
      "type": "checkbox",
      "value": "physical_exam"
    },
    domProps: {
      "checked": Array.isArray(_vm.requirements.primary) ? _vm._i(_vm.requirements.primary, "physical_exam") > -1 : (_vm.requirements.primary)
    },
    on: {
      "__c": function($event) {
        var $$a = _vm.requirements.primary,
          $$el = $event.target,
          $$c = $$el.checked ? (true) : (false);
        if (Array.isArray($$a)) {
          var $$v = "physical_exam",
            $$i = _vm._i($$a, $$v);
          if ($$c) {
            $$i < 0 && (_vm.requirements.primary = $$a.concat($$v))
          } else {
            $$i > -1 && (_vm.requirements.primary = $$a.slice(0, $$i).concat($$a.slice($$i + 1)))
          }
        } else {
          _vm.requirements.primary = $$c
        }
      }
    }
  }), _vm._v(" Physical Exam")])])]), _vm._v(" "), _c('div', {
    staticClass: "col-md-4"
  }, [_c('ul', {
    staticClass: "list list-unstyled"
  }, [_c('li', [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.requirements.primary),
      expression: "requirements.primary"
    }],
    attrs: {
      "type": "checkbox",
      "value": "tor"
    },
    domProps: {
      "checked": Array.isArray(_vm.requirements.primary) ? _vm._i(_vm.requirements.primary, "tor") > -1 : (_vm.requirements.primary)
    },
    on: {
      "__c": function($event) {
        var $$a = _vm.requirements.primary,
          $$el = $event.target,
          $$c = $$el.checked ? (true) : (false);
        if (Array.isArray($$a)) {
          var $$v = "tor",
            $$i = _vm._i($$a, $$v);
          if ($$c) {
            $$i < 0 && (_vm.requirements.primary = $$a.concat($$v))
          } else {
            $$i > -1 && (_vm.requirements.primary = $$a.slice(0, $$i).concat($$a.slice($$i + 1)))
          }
        } else {
          _vm.requirements.primary = $$c
        }
      }
    }
  }), _vm._v(" Transcript of Records")]), _vm._v(" "), _c('li', [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.requirements.primary),
      expression: "requirements.primary"
    }],
    attrs: {
      "type": "checkbox",
      "value": "diploma"
    },
    domProps: {
      "checked": Array.isArray(_vm.requirements.primary) ? _vm._i(_vm.requirements.primary, "diploma") > -1 : (_vm.requirements.primary)
    },
    on: {
      "__c": function($event) {
        var $$a = _vm.requirements.primary,
          $$el = $event.target,
          $$c = $$el.checked ? (true) : (false);
        if (Array.isArray($$a)) {
          var $$v = "diploma",
            $$i = _vm._i($$a, $$v);
          if ($$c) {
            $$i < 0 && (_vm.requirements.primary = $$a.concat($$v))
          } else {
            $$i > -1 && (_vm.requirements.primary = $$a.slice(0, $$i).concat($$a.slice($$i + 1)))
          }
        } else {
          _vm.requirements.primary = $$c
        }
      }
    }
  }), _vm._v(" Diploma")]), _vm._v(" "), _c('li', [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.requirements.primary),
      expression: "requirements.primary"
    }],
    attrs: {
      "type": "checkbox",
      "value": "police_clearance"
    },
    domProps: {
      "checked": Array.isArray(_vm.requirements.primary) ? _vm._i(_vm.requirements.primary, "police_clearance") > -1 : (_vm.requirements.primary)
    },
    on: {
      "__c": function($event) {
        var $$a = _vm.requirements.primary,
          $$el = $event.target,
          $$c = $$el.checked ? (true) : (false);
        if (Array.isArray($$a)) {
          var $$v = "police_clearance",
            $$i = _vm._i($$a, $$v);
          if ($$c) {
            $$i < 0 && (_vm.requirements.primary = $$a.concat($$v))
          } else {
            $$i > -1 && (_vm.requirements.primary = $$a.slice(0, $$i).concat($$a.slice($$i + 1)))
          }
        } else {
          _vm.requirements.primary = $$c
        }
      }
    }
  }), _vm._v(" Police Clearance")]), _vm._v(" "), _c('li', [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.requirements.primary),
      expression: "requirements.primary"
    }],
    attrs: {
      "type": "checkbox",
      "value": "nbi"
    },
    domProps: {
      "checked": Array.isArray(_vm.requirements.primary) ? _vm._i(_vm.requirements.primary, "nbi") > -1 : (_vm.requirements.primary)
    },
    on: {
      "__c": function($event) {
        var $$a = _vm.requirements.primary,
          $$el = $event.target,
          $$c = $$el.checked ? (true) : (false);
        if (Array.isArray($$a)) {
          var $$v = "nbi",
            $$i = _vm._i($$a, $$v);
          if ($$c) {
            $$i < 0 && (_vm.requirements.primary = $$a.concat($$v))
          } else {
            $$i > -1 && (_vm.requirements.primary = $$a.slice(0, $$i).concat($$a.slice($$i + 1)))
          }
        } else {
          _vm.requirements.primary = $$c
        }
      }
    }
  }), _vm._v(" NBI Clearance")])])]), _vm._v(" "), _c('div', {
    staticClass: "col-md-4"
  }, [_c('ul', {
    staticClass: "list list-unstyled"
  }, [_c('li', [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.requirements.primary),
      expression: "requirements.primary"
    }],
    attrs: {
      "type": "checkbox",
      "value": "coe"
    },
    domProps: {
      "checked": Array.isArray(_vm.requirements.primary) ? _vm._i(_vm.requirements.primary, "coe") > -1 : (_vm.requirements.primary)
    },
    on: {
      "__c": function($event) {
        var $$a = _vm.requirements.primary,
          $$el = $event.target,
          $$c = $$el.checked ? (true) : (false);
        if (Array.isArray($$a)) {
          var $$v = "coe",
            $$i = _vm._i($$a, $$v);
          if ($$c) {
            $$i < 0 && (_vm.requirements.primary = $$a.concat($$v))
          } else {
            $$i > -1 && (_vm.requirements.primary = $$a.slice(0, $$i).concat($$a.slice($$i + 1)))
          }
        } else {
          _vm.requirements.primary = $$c
        }
      }
    }
  }), _vm._v(" Certificate of Employment")]), _vm._v(" "), _c('li', [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.requirements.primary),
      expression: "requirements.primary"
    }],
    attrs: {
      "type": "checkbox",
      "value": "training"
    },
    domProps: {
      "checked": Array.isArray(_vm.requirements.primary) ? _vm._i(_vm.requirements.primary, "training") > -1 : (_vm.requirements.primary)
    },
    on: {
      "__c": function($event) {
        var $$a = _vm.requirements.primary,
          $$el = $event.target,
          $$c = $$el.checked ? (true) : (false);
        if (Array.isArray($$a)) {
          var $$v = "training",
            $$i = _vm._i($$a, $$v);
          if ($$c) {
            $$i < 0 && (_vm.requirements.primary = $$a.concat($$v))
          } else {
            $$i > -1 && (_vm.requirements.primary = $$a.slice(0, $$i).concat($$a.slice($$i + 1)))
          }
        } else {
          _vm.requirements.primary = $$c
        }
      }
    }
  }), _vm._v(" Training / Seminar Certificate")]), _vm._v(" "), _c('li', [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.others),
      expression: "others"
    }],
    attrs: {
      "type": "checkbox"
    },
    domProps: {
      "checked": Array.isArray(_vm.others) ? _vm._i(_vm.others, null) > -1 : (_vm.others)
    },
    on: {
      "__c": function($event) {
        var $$a = _vm.others,
          $$el = $event.target,
          $$c = $$el.checked ? (true) : (false);
        if (Array.isArray($$a)) {
          var $$v = null,
            $$i = _vm._i($$a, $$v);
          if ($$c) {
            $$i < 0 && (_vm.others = $$a.concat($$v))
          } else {
            $$i > -1 && (_vm.others = $$a.slice(0, $$i).concat($$a.slice($$i + 1)))
          }
        } else {
          _vm.others = $$c
        }
      }
    }
  }), _vm._v(" Others\n                        ")])]), _vm._v(" "), _c('ul', {
    staticClass: "list list-unstyled"
  }, _vm._l((_vm.requirements.other), function(other, index) {
    return _c('li', [_c('div', {
      staticClass: "form-group col-md-8"
    }, [_c('input', {
      directives: [{
        name: "model",
        rawName: "v-model",
        value: (other.req_name),
        expression: "other.req_name"
      }],
      staticClass: "form-control input-sm pull-left",
      staticStyle: {
        "max-width": "100px"
      },
      attrs: {
        "type": "text",
        "disabled": !_vm.others
      },
      domProps: {
        "value": (other.req_name)
      },
      on: {
        "input": function($event) {
          if ($event.target.composing) { return; }
          other.req_name = $event.target.value
        }
      }
    }), _vm._v(" "), (index == 0) ? _c('button', {
      staticClass: "btn btn-default glyphicon glyphicon-plus pull-right",
      attrs: {
        "disabled": !_vm.others
      },
      on: {
        "click": function($event) {
          _vm.addOtherReq()
        }
      }
    }) : _c('button', {
      staticClass: "btn btn-default glyphicon glyphicon-remove pull-right",
      attrs: {
        "disabled": !_vm.others
      },
      on: {
        "click": function($event) {
          _vm.removeOtherReq(index)
        }
      }
    })])])
  }))]), _vm._v(" "), _c('div', {
    staticClass: "form-group"
  }, [_c('div', {
    staticClass: "col-md-12 text-right"
  }, [_c('button', {
    staticClass: "btn btn-default",
    on: {
      "click": function($event) {
        $event.preventDefault();
        _vm.update($event)
      }
    }
  }, [_vm._v("Save")]), _vm._v(" "), _c('button', {
    staticClass: "btn btn-default"
  }, [_vm._v("Print")])])])])])])])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-bb06dc5e", module.exports)
  }
}

/***/ }),
/* 58 */,
/* 59 */,
/* 60 */,
/* 61 */,
/* 62 */,
/* 63 */,
/* 64 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(28);


/***/ })
/******/ ]);