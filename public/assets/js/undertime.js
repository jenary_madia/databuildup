$(document).ready(function() 
{
    var list_table = $('#undertime_tb').DataTable({
    		"sPaginationType": "full_numbers",
    		  'iDisplayLength' : 10,
               'bInfo':false,
               'bLengthChange':false,     
                'bFilter':false,    
               "aoColumns": [
                  { "sClass": "text-center", "bSortable": false },
                  { "sClass": "", "bSortable": true },
                  { "sClass": "", "bSortable": true },
                  { "sClass": "", "bSortable": true },     
      
                ]       
    });

    $(".add_build_up").click(function()
    {
        clear();
        $("#undertime-modal").modal('show');
    })
    

    $(".edit_build_up").click(function(e)
    {
        if($("input[name='und_id[]']:checked").length)
        {
            if($("input[name='und_id[]']:checked").length > 1)
            {
                swal({
                    title: "Error",
                    text: "Choose only 1 item to edit.",
                    type: "error",
                    confirmButtonText: "Ok"
                });
            }
            else
            {
                $("input[name='und_id']").val( $("input[name='und_id[]']:checked").val() );
                $("input[name='und_from']").val( parseInt($("input[name='und_id[]']:checked").parent().siblings(":nth-child(2)").text()) );
                $("input[name='und_to']").val( parseInt($("input[name='und_id[]']:checked").parent().siblings(":nth-child(3)").text()) );
                $("input[name='und_minutes']").val( parseInt($("input[name='und_id[]']:checked").parent().siblings(":nth-child(4)").text()) );
              
                $("#undertime-modal").modal('show');
            }
        }
        else
        {
             swal({
                title: "Error",
                text: "Choose item to edit.",
                type: "error",
                confirmButtonText: "Ok"
            });
        }
    })
 
    $("#save_und").on("click",function (e) {
 
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    method: "POST",
                    url: add_url,
                    data: $("form").serializeArray()
                })
                .done(function( response ) {

                	
                   
                    // $("#to_add_data_buildup").val("");
                    // list_table.draw();
                    if(response.success == true) 
                    {
                    	 list_table.clear();
				        $(response.data).each(function(val,index)
				        {
				              list_table.rows.add([ index ]);  
				        });        
				         list_table.draw();


                        swal({
                            title: "Success",
                            text: response.message,
                            type: "success",
                            confirmButtonText: "Ok"
                        });

                        $("#undertime-modal").modal('hide');
                    }else{
                        swal({
                            title: "Error",
                            text: "Required field must be filled up.",
                            type: "error",
                            confirmButtonText: "Ok"
                        });
                    }

                });
            });



    function clear()
    {
        $("input[name='und_id']").val("");
        $("input[name='und_from']").val("");
        $("input[name='und_to']").val("");
        $("input[name='und_minutes']").val("");
    }

} );