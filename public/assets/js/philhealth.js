$(document).ready(function() 
{
    var list_table = $('#philhealth_tb').DataTable({
    		"sPaginationType": "full_numbers",
    		  'iDisplayLength' : 10,
               'bInfo':false,
               'bLengthChange':false,     
                'bFilter':false,    
               "aoColumns": [
                  { "sClass": "text-center", "bSortable": false },
                  { "sClass": "", "bSortable": true },
                  { "sClass": "", "bSortable": true },
                  { "sClass": "", "bSortable": true },     
                   { "sClass": "", "bSortable": true },
                  { "sClass": "", "bSortable": true },    
      
                ]       
    });

    $(".add_build_up").click(function()
    {
        clear();
        $("#philhealth-modal").modal('show');
    })
    

    $(".edit_build_up").click(function(e)
    {
        if($("input[name='phl_id[]']:checked").length)
        {
            if($("input[name='phl_id[]']:checked").length > 1)
            {
                swal({
                    title: "Error",
                    text: "Choose only 1 item to edit.",
                    type: "error",
                    confirmButtonText: "Ok"
                });
            }
            else
            {
                $("input[name='phl_id']").val( $("input[name='phl_id[]']:checked").val() );
                $("input[name='phl_from']").val( parseFloat($("input[name='phl_id[]']:checked").parent().siblings(":nth-child(2)").text()) );
                $("input[name='phl_to']").val( parseFloat($("input[name='phl_id[]']:checked").parent().siblings(":nth-child(3)").text()) );
                $("input[name='phl_employer']").val( parseFloat($("input[name='phl_id[]']:checked").parent().siblings(":nth-child(4)").text()) );
                $("input[name='phl_employee']").val( parseFloat($("input[name='phl_id[]']:checked").parent().siblings(":nth-child(5)").text()) );
              
                $("#philhealth-modal").modal('show');
            }
        }
        else
        {
             swal({
                title: "Error",
                text: "Choose item to edit.",
                type: "error",
                confirmButtonText: "Ok"
            });
        }
    })
 
    $("#save_phl").on("click",function (e) {
 
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    method: "POST",
                    url: add_url,
                    data: $("form").serializeArray()
                })
                .done(function( response ) {

                	
                   
                    // $("#to_add_data_buildup").val("");
                    // list_table.draw();
                    if(response.success == true) 
                    {
                    	 list_table.clear();
				        $(response.data).each(function(val,index)
				        {
				              list_table.rows.add([ index ]);  
				        });        
				         list_table.draw();


                        swal({
                            title: "Success",
                            text: response.message,
                            type: "success",
                            confirmButtonText: "Ok"
                        });

                        $("#philhealth-modal").modal('hide');
                    }else{
                        swal({
                            title: "Error",
                            text: "Required field must be filled up.",
                            type: "error",
                            confirmButtonText: "Ok"
                        });
                    }

                });
            });



    function clear()
    {
        $("input[name='phl_id']").val("");
        $("input[name='phl_from']").val("");
        $("input[name='phl_to']").val("");
        $("input[name='phl_employer']").val("");
        $("input[name='phl_employee']").val("");
    }

} );