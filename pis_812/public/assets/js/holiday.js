$(document).ready(function() 
{
      $( ".date_picker" ).datepicker({
                "dateFormat" : 'MM dd, yy'
            });


    var list_table = $('#holiday_tb').DataTable({
    		"sPaginationType": "full_numbers",
    		  'iDisplayLength' : 10,
               'bInfo':false,
               'bLengthChange':false,     
                'bFilter':false,    
               "aoColumns": [
                  { "sClass": "text-center", "bSortable": false },
                  { "sClass": "", "bSortable": true },
                  { "sClass": "", "bSortable": true },
                  { "sClass": "", "bSortable": true },
                  { "sClass": "", "bSortable": true },     
      
                ]       
    });

    $(".add_build_up").click(function()
    {
        clear();
        $("#holiday-modal").modal('show');
    })
    

    $(".edit_build_up").click(function(e)
    {
        if($("input[name='hol_id[]']:checked").length)
        {
            if($("input[name='hol_id[]']:checked").length > 1)
            {
                swal({
                    title: "Error",
                    text: "Choose only 1 item to edit.",
                    type: "error",
                    confirmButtonText: "Ok"
                });
            }
            else
            {
                $("input[name='hol_id']").val( $("input[name='hol_id[]']:checked").val() );
                $("input[name='hol_date']").val( $("input[name='hol_id[]']:checked").parent().siblings(":nth-child(2)").text() );
                $("input[name='day']").val( $("input[name='hol_id[]']:checked").parent().siblings(":nth-child(3)").text() );
                $("input[name='hol_desc']").val( $("input[name='hol_id[]']:checked").parent().siblings(":nth-child(4)").text() );
                $("select[name='hol_type']").val( $("input[name='hol_id[]']:checked").parent().siblings(":nth-child(5)").text() );

                $("#holiday-modal").modal('show');
            }
        }
        else
        {
             swal({
                title: "Error",
                text: "Choose item to edit.",
                type: "error",
                confirmButtonText: "Ok"
            });
        }
    })
 
    $("#save_hol").on("click",function (e) {
 
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    method: "POST",
                    url: add_url,
                    data: $("form").serializeArray()
                })
                .done(function( response ) {

                	
                   
                    // $("#to_add_data_buildup").val("");
                    // list_table.draw();
                    if(response.success == true) 
                    {
                    	 list_table.clear();
				        $(response.data).each(function(val,index)
				        {
				              list_table.rows.add([ index ]);  
				        });        
				         list_table.draw();


                        swal({
                            title: "Success",
                            text: response.message,
                            type: "success",
                            confirmButtonText: "Ok"
                        });

                        $("#holiday-modal").modal('hide');
                    }else{
                        swal({
                            title: "Error",
                            text: "Required field must be filled up.",
                            type: "error",
                            confirmButtonText: "Ok"
                        });
                    }

                });
            });



    function clear()
    {
        $("input[name='hol_id']").val("");
        $("input[name='hol_date']").val("");
        $("input[name='day']").val("");
        $("input[name='hol_desc']").val("");
        $("select[name='hol_type']").val("");
    }

} );