$(document).ready(function() 
{
    var list_table = $('#sss_tb').DataTable({
    		"sPaginationType": "full_numbers",
    		  'iDisplayLength' : 10,
               'bInfo':false,
               'bLengthChange':false,     
                'bFilter':false,    
               "aoColumns": [
                  { "sClass": "text-center", "bSortable": false },
                  { "sClass": "", "bSortable": true },
                  { "sClass": "", "bSortable": true },
                  { "sClass": "", "bSortable": true },     
                   { "sClass": "", "bSortable": true },
                  { "sClass": "", "bSortable": true },    
      
                ]       
    });

    $(".add_build_up").click(function()
    {
        clear();
        $("#sss-modal").modal('show');
    })
    

    $(".edit_build_up").click(function(e)
    {
        if($("input[name='sss_id[]']:checked").length)
        {
            if($("input[name='sss_id[]']:checked").length > 1)
            {
                swal({
                    title: "Error",
                    text: "Choose only 1 item to edit.",
                    type: "error",
                    confirmButtonText: "Ok"
                });
            }
            else
            {
                $("input[name='sss_id']").val( $("input[name='sss_id[]']:checked").val() );
                $("input[name='sss_from']").val( parseFloat($("input[name='sss_id[]']:checked").parent().siblings(":nth-child(2)").text()) );
                $("input[name='sss_to']").val( parseFloat($("input[name='sss_id[]']:checked").parent().siblings(":nth-child(3)").text()) );
                $("input[name='sss_employer']").val( parseFloat($("input[name='sss_id[]']:checked").parent().siblings(":nth-child(4)").text()) );
                $("input[name='sss_employee']").val( parseFloat($("input[name='sss_id[]']:checked").parent().siblings(":nth-child(5)").text()) );
              
                $("#sss-modal").modal('show');
            }
        }
        else
        {
             swal({
                title: "Error",
                text: "Choose item to edit.",
                type: "error",
                confirmButtonText: "Ok"
            });
        }
    })
 
    $("#save_sss").on("click",function (e) {
 
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    method: "POST",
                    url: add_url,
                    data: $("form").serializeArray()
                })
                .done(function( response ) {

                	
                   
                    // $("#to_add_data_buildup").val("");
                    // list_table.draw();
                    if(response.success == true) 
                    {
                    	 list_table.clear();
				        $(response.data).each(function(val,index)
				        {
				              list_table.rows.add([ index ]);  
				        });        
				         list_table.draw();


                        swal({
                            title: "Success",
                            text: response.message,
                            type: "success",
                            confirmButtonText: "Ok"
                        });

                        $("#sss-modal").modal('hide');
                    }else{
                        swal({
                            title: "Error",
                            text: "Required field must be filled up.",
                            type: "error",
                            confirmButtonText: "Ok"
                        });
                    }

                });
            });



    function clear()
    {
        $("input[name='sss_id']").val("");
        $("input[name='sss_from']").val("");
        $("input[name='sss_to']").val("");
        $("input[name='sss_employer']").val("");
        $("input[name='sss_employee']").val("");
    }

} );