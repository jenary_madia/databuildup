<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class SSS_table extends Authenticatable
{
      public $timestamps = false;
      protected $guarded = array();
      protected $table = "sss_tables";	
}