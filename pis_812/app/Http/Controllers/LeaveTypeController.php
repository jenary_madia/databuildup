<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\LeaveType;
use Validator;
use Response;
use Redirect;

class LeaveTypeController extends Controller
{
    public function report() 
    {
    	$data["report"] = LeaveType::get();

        return view('leave_type.report' ,$data );
    }

     public function store(Request $request) 
     {
        $rules = ['lt_code' => 'required','lt_desc' => 'required','lt_type' => 'required'];
        $params = [
            'lt_code' => $request->lt_code,
            'lt_desc' => $request->lt_desc,
            'lt_type' => $request->lt_type
        ];

 
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return Response::json(array(
                'success' => false,
                'errors' => $validator->getMessageBag()->toArray(),
                'message' => 'Some fields are incomplete',
                200
            ));
        }

        if($request->lt_id)
        {
            LeaveType::where("lt_id",$request->lt_id)->update($params);
        }
        else
        {
            LeaveType::create($params);
        }
        

        $report = LeaveType::get();
        $data = array();
        foreach($report as $index=>$rs)
        {
        	$data[$index][0] = "<input type='checkbox' class='lt_id' name='lt_id[]' value='$rs->lt_id' />";
        	$data[$index][1]  =  $rs->lt_code; 
        	$data[$index][2]  =  $rs->lt_desc; 
        	$data[$index][3]  =  $rs->lt_type; 
        }

        return Response::json(array(
            'success' => true,
            'message' => $request->lt_id ? "Leave Type successfully updated" : "Leave Type successfully added",
            'data'=>$data,
            200
        ));
    }

  	 public function delete(Request $request) 
     {
        $rules = ['lt_id' => 'required'];
   
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails())
		{
        	return Redirect::to('file-maintenance/leavetype')
		          	 ->with('errorMessage', "At least one leave type is required to proceed transaction.");
		}
		else
		{
			LeaveType::whereIn("lt_id",$request->lt_id)->delete();

			return Redirect::to('file-maintenance/leavetype')
		          	 ->with('successMessage', "Successfully Deleted leave type.");
		}
        
    }
}
