<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Tardiness_table;
use Validator;
use Response;
use Redirect;

class TardinessController extends Controller
{
    public function report() 
    {
    	$data["report"] = Tardiness_table::get();
        $data['module'] = "Time Keeping Table";
        $data['tab'] = "File Maintenance";  

        return view('tables.tardiness' ,$data );
    }

     public function store(Request $request) 
     {
        $rules = ['tar_from' => 'required','tar_to' => 'required','tar_minutes' => 'required'];
        $params = [
            'tar_from' =>  $request->tar_from,
            'tar_to' => $request->tar_to,
            'tar_minutes' => $request->tar_minutes
        ];

 
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return Response::json(array(
                'success' => false,
                'errors' => $validator->getMessageBag()->toArray(),
                'message' => 'Some fields are incomplete',
                200
            ));
        }

        if($request->tar_id)
        {
            Tardiness_table::where("tar_id",$request->tar_id)->update($params);
        }
        else
        {
            Tardiness_table::create($params);
        }
        

        $report = Tardiness_table::get();
        $data = array();
        foreach($report as $index=>$rs)
        {
        	$data[$index][0] = "<input type='checkbox' class='tar_id' name='tar_id[]' value='$rs->tar_id' />";
            $data[$index][1]  =  ($rs->tar_from) . ($rs->tar_from > 1 ?  " minutes" : " minute") ;
        	$data[$index][2]  =  ($rs->tar_to) .  ($rs->tar_to > 1 ?  " minutes" : " minute");
        	$data[$index][3]  =  ($rs->tar_minutes) .  ($rs->tar_minutes > 1 ?   " minutes" : " minute") ;
        }

        return Response::json(array(
            'success' => true,
            'message' => $request->tar_id ? "Tardiness successfully updated" : "Tardiness successfully added",
            'data'=>$data,
            200
        ));
    }

  	 public function delete(Request $request) 
     {
        $rules = ['tar_id' => 'required'];
   
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails())
		{
        	return Redirect::to('file-maintenance/tardiness')
		          	 ->with('errorMessage', "At least one tardiness is required to proceed transaction.");
		}
		else
		{
			Tardiness_table::whereIn("tar_id",$request->tar_id)->delete();

			return Redirect::to('file-maintenance/tardiness')
		          	 ->with('successMessage', "Successfully Deleted tardiness.");
		}
        
    }
}
