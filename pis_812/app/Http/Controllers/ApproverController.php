<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Approver;
use Validator;
use Response;
use Redirect;

class ApproverController extends Controller
{
    public function report() 
    {
        $data["report"] = Approver::get();
        $data["employee"] = Approver::employee();
        $data["department"] = Approver::department();
        $data['tab'] = "File Maintenance";
        $data['module'] = "Approver Set Up";

        return view('approver.report' ,$data );
    }

     public function store(Request $request) 
     {
        $rules = ['app_dept' => 'required','app_approver' => 'required','app_receiver' => 'required'];
        $params = [
            'app_dept' => $request->app_dept,
            'app_approver' => $request->app_approver,
            'app_receiver' => $request->app_receiver
        ];

 
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return Response::json(array(
                'success' => false,
                'errors' => $validator->getMessageBag()->toArray(),
                'message' => 'Some fields are incomplete',
                200
            ));
        }

        if($request->app_id)
        {
            Approver::where("app_id",$request->app_id)->update($params);
        }
        else
        {
            Approver::create($params);
        }
        
        $employee = Approver::employee();
        $department = Approver::department();

        $report = Approver::get();
        $data = array();
        foreach($report as $index=>$rs)
        {
            $data[$index][0] = "<input type='checkbox' class='app_id' name='app_id[]' value='$rs->app_id' />";
            $data[$index][1]  = "<input type='hidden' value='$rs->app_dept' />" . $department[$rs->app_dept];
            $data[$index][2]  = "<input type='hidden' value='$rs->app_approver' />" . $employee[$rs->app_approver];
            $data[$index][3]  = "<input type='hidden' value='$rs->app_receiver' />" .  $employee[$rs->app_receiver]; 
            $data[$index][4]  = "";
        }

        return Response::json(array(
            'success' => true,
            'message' => $request->app_id ? "Approver setup successfully updated" : "Approver setup successfully added",
            'data'=>$data,
            200
        ));
    }

     public function delete(Request $request) 
     {
        $rules = ['app_id' => 'required'];
   
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails())
        {
            return Redirect::to('file-maintenance/approver')
                     ->with('errorMessage', "At least one setup is required to proceed transaction.");
        }
        else
        {
            Approver::whereIn("app_id",$request->app_id)->delete();

            return Redirect::to('file-maintenance/approver')
                     ->with('successMessage', "Successfully Deleted setup.");
        }
        
    }
}
