<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Holiday;
use Validator;
use Response;
use Redirect;

class HolController extends Controller
{
    public function report() 
    {
    	$data["report"] = Holiday::get();
        $data['module'] = "Holiday Set Up";
        $data['tab'] = "File Maintenance";

        return view('holiday.report' ,$data );
    }

     public function store(Request $request) 
     {
        $rules = ['hol_date' => 'required','hol_desc' => 'required','hol_type' => 'required'];
        $params = [
            'hol_date' => date("Y-m-d",strtotime($request->hol_date)),
            'hol_desc' => $request->hol_desc,
            'hol_type' => $request->hol_type
        ];

 
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return Response::json(array(
                'success' => false,
                'errors' => $validator->getMessageBag()->toArray(),
                'message' => 'Some fields are incomplete',
                200
            ));
        }

        if($request->hol_id)
        {
            Holiday::where("hol_id",$request->hol_id)->update($params);
        }
        else
        {
            Holiday::create($params);
        }
        

        $report = Holiday::get();
        $data = array();
        foreach($report as $index=>$rs)
        {
        	$data[$index][0] = "<input type='checkbox' class='hol_id' name='hol_id[]' value='$rs->hol_id' />";
        	$data[$index][1]  =  date("F d, Y",strtotime($rs->hol_date));
        	$data[$index][2]  =  date("l",strtotime($rs->hol_date));
        	$data[$index][3]  =  $rs->hol_desc;
        	$data[$index][4]  =  $rs->hol_type ;
        }

        return Response::json(array(
            'success' => true,
            'message' => $request->hol_id ? "Holiday successfully updated" : "Holiday successfully added",
            'data'=>$data,
            200
        ));
    }

  	 public function delete(Request $request) 
     {
        $rules = ['hol_id' => 'required'];
   
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails())
		{
        	return Redirect::to('file-maintenance/holiday')
		          	 ->with('errorMessage', "At least one holiday is required to proceed transaction.");
		}
		else
		{
			Holiday::whereIn("hol_id",$request->hol_id)->delete();

			return Redirect::to('file-maintenance/holiday')
		          	 ->with('successMessage', "Successfully Deleted holiday.");
		}
        
    }
}
