@extends('layouts.app')
@section('content')
    <form class="form-inline" role="form" method="POST" action="{!! route('overtime.delete') !!}">
    {{ csrf_field() }}
    <table id="overtime_tb" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
        <tr>
            <th></th>
            <th>Minutes From</th>
            <th>Minutes To</th>
            <th>Minutes Overtime</th>
        </tr>
        </thead>
     
        <tbody>

        @foreach($report as $rs)
        <tr>
            <td align="center"><input type="checkbox" name="ovt_id[]" class="ovt_id" value="{{ $rs->ovt_id }}"/></td>
            <td>{{ ($rs->ovt_from) . ($rs->ovt_from > 1 ?  " minutes" : " minute") }}</td>
            <td>{{ ($rs->ovt_to) . ($rs->ovt_to > 1 ?  " minutes" : " minute") }}</td>
            <td>{{ ($rs->ovt_minutes) . ($rs->ovt_minutes > 1 ?  " minutes" : " minute") }}</td> 
        </tr>
        @endforeach


        </tbody>
    </table>

    <button type="button" class="btn btn-default add_build_up">ADD</button>
    <button type="button" class="btn btn-default edit_build_up">EDIT</button>
    <button type="submit" class="btn btn-default delete_build_up">DELETE</button>
    </form>

<div class="modal fade" id="overtime-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
    

      <div class="modal-body">
        <div class="container-fluid bd-example-row">
          <form class="form-horizontal"  >
              <div class="clear_10"></div>
              <input type="hidden" class=" form-control"   name="ovt_id" id="ovt_id"  />

              <div class="row">
                <div class="col-sm-4"><label class="labels"><span class="text-danger">*</span>Minutes From:</label></div>
                <div class="col-sm-8">
                    <input type="text" class="form-control" name="ovt_from" />
                </div>
              </div>

               <br>

               <div class="row">
                <div class="col-sm-4"><label class="labels"><span class="text-danger">*</span>Minutes To:</label></div>
                <div class="col-sm-8">
                    <input type="text" class="form-control" name="ovt_to" />
                </div>
              </div>

               <br>

 

               <div class="row">
                <div class="col-sm-4"><label class="labels"><span class="text-danger">*</span>Minutes Overtime:</label></div>
                <div class="col-sm-8">
                    <input type="text" class="form-control" name="ovt_minutes" />
                </div>
              </div>

               <br>

    

              <div class="row">
                  <div class="col-sm-12">
                      <div class="text-center  ">
                        <button type="button" class="btn btn-secondary text-center" id="save_ovt" >Save</button>
                        <button type="button" class="btn btn-secondary text-center" data-dismiss="modal">Cancel</button>        
                      </div>
                  </div>
              </div>
              
              </form>

          </div>


      </div>

     
     
    </div>
  </div>
</div>

@endsection

@section('scripts')
<script>
 
    var add_url = "{!! route('overtime.add') !!}";
    // var add_url = "{!! route('maintenance.add.buildup') !!}";
 
</script>
<script src="{{ asset('assets/js/overtime.js') }}"></script>
@stop