@extends('layouts.app')
@section('content')
    <form class="form-inline" role="form" method="POST" action="{!! route('tardiness.delete') !!}">
    {{ csrf_field() }}
    <table id="tardiness_tb" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
        <tr>
            <th></th>
            <th>Minutes From</th>
            <th>Minutes To</th>
            <th>Minutes Tardiness</th>
        </tr>
        </thead>
     
        <tbody>

        @foreach($report as $rs)
        <tr>
            <td align="center"><input type="checkbox" name="tar_id[]" class="tar_id" value="{{ $rs->tar_id }}"/></td>
            <td>{{ ($rs->tar_from) . ($rs->tar_from > 1 ?  " minutes" : " minute") }}</td>
            <td>{{ ($rs->tar_to) . ($rs->tar_to > 1 ?  " minutes" : " minute") }}</td>
            <td>{{ ($rs->tar_minutes) . ($rs->tar_minutes > 1 ?  " minutes" : " minute") }}</td> 
        </tr>
        @endforeach


        </tbody>
    </table>

    <button type="button" class="btn btn-default add_build_up">ADD</button>
    <button type="button" class="btn btn-default edit_build_up">EDIT</button>
    <button type="submit" class="btn btn-default delete_build_up">DELETE</button>
    </form>

<div class="modal fade" id="tardiness-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
    

      <div class="modal-body">
        <div class="container-fluid bd-example-row">
          <form class="form-horizontal"  >
              <div class="clear_10"></div>
              <input type="hidden" class=" form-control"   name="tar_id" id="tar_id"  />

              <div class="row">
                <div class="col-sm-4"><label class="labels"><span class="text-danger">*</span>Minutes From:</label></div>
                <div class="col-sm-8">
                    <input type="text" class="form-control" name="tar_from" />
                </div>
              </div>

               <br>

               <div class="row">
                <div class="col-sm-4"><label class="labels"><span class="text-danger">*</span>Minutes To:</label></div>
                <div class="col-sm-8">
                    <input type="text" class="form-control" name="tar_to" />
                </div>
              </div>

               <br>

 

               <div class="row">
                <div class="col-sm-4"><label class="labels"><span class="text-danger">*</span>Minutes Tardiness:</label></div>
                <div class="col-sm-8">
                    <input type="text" class="form-control" name="tar_minutes" />
                </div>
              </div>

               <br>

    

              <div class="row">
                  <div class="col-sm-12">
                      <div class="text-center  ">
                        <button type="button" class="btn btn-secondary text-center" id="save_tar" >Save</button>
                        <button type="button" class="btn btn-secondary text-center" data-dismiss="modal">Cancel</button>        
                      </div>
                  </div>
              </div>
              
              </form>

          </div>


      </div>

     
     
    </div>
  </div>
</div>

@endsection

@section('scripts')
<script>
 
    var add_url = "{!! route('tardiness.add') !!}";
    // var add_url = "{!! route('maintenance.add.buildup') !!}";
 
</script>
<script src="{{ asset('assets/js/tardiness.js') }}"></script>
@stop