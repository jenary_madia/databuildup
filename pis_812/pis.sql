-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 12, 2017 at 03:19 PM
-- Server version: 10.1.25-MariaDB
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pis`
--

-- --------------------------------------------------------

--
-- Table structure for table `applicants`
--

 

CREATE TABLE `approvers` (
  `app_id` int(11) NOT NULL,
  `app_dept` int(11) NOT NULL,
  `app_approver` int(11) NOT NULL,
  `app_receiver` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `character_references`
--
  
--
-- Table structure for table `holidays`
--

CREATE TABLE `holidays` (
  `hol_id` int(11) NOT NULL,
  `hol_date` date NOT NULL,
  `hol_desc` varchar(500) NOT NULL,
  `hol_type` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `job_descriptions`
-- 

CREATE TABLE `leave_types` (
  `lt_id` int(11) NOT NULL,
  `lt_code` varchar(100) NOT NULL,
  `lt_desc` varchar(500) NOT NULL,
  `lt_type` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--
 

CREATE TABLE `overtime_tables` (
  `ovt_id` int(11) NOT NULL,
  `ovt_from` int(11) NOT NULL,
  `ovt_to` int(11) NOT NULL,
  `ovt_minutes` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
 
-- --------------------------------------------------------

--
-- Table structure for table `philhealth_tables`
--

CREATE TABLE `philhealth_tables` (
  `phl_id` int(11) NOT NULL,
  `phl_from` double NOT NULL,
  `phl_to` double NOT NULL,
  `phl_employer` double NOT NULL,
  `phl_employee` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `requirements`
-- 
-- --------------------------------------------------------

--
-- Table structure for table `sss_tables`
--

CREATE TABLE `sss_tables` (
  `sss_id` int(11) NOT NULL,
  `sss_from` double NOT NULL,
  `sss_to` double NOT NULL,
  `sss_employer` double NOT NULL,
  `sss_employee` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tardiness_tables`
--

CREATE TABLE `tardiness_tables` (
  `tar_id` int(11) NOT NULL,
  `tar_from` int(11) NOT NULL,
  `tar_to` int(11) NOT NULL,
  `tar_minutes` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `trainings`
--
 -- --------------------------------------------------------

--
-- Table structure for table `undertime_tables`
--

CREATE TABLE `undertime_tables` (
  `und_id` int(11) NOT NULL,
  `und_from` int(11) NOT NULL,
  `und_to` int(11) NOT NULL,
  `und_minutes` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--
 
--
-- Indexes for dumped tables
--

--
-- Indexes for table `applicants`
--
 
--
-- Indexes for table `approvers`
--
ALTER TABLE `approvers`
  ADD PRIMARY KEY (`app_id`);

 
--
-- Indexes for table `holidays`
--
ALTER TABLE `holidays`
  ADD PRIMARY KEY (`hol_id`);

--
-- Indexes for table `job_descriptions`
-- 

--
-- Indexes for table `leave_types`
--
ALTER TABLE `leave_types`
  ADD PRIMARY KEY (`lt_id`);

--
-- Indexes for table `migrations`
--
 
--
-- Indexes for table `overtime_tables`
--
ALTER TABLE `overtime_tables`
  ADD PRIMARY KEY (`ovt_id`);

--
-- Indexes for table `password_resets`
--
 
--
ALTER TABLE `philhealth_tables`
  ADD PRIMARY KEY (`phl_id`);

--
-- Indexes for table `requirements`
--
 

--
-- Indexes for table `sss_tables`
--
ALTER TABLE `sss_tables`
  ADD PRIMARY KEY (`sss_id`);

--
-- Indexes for table `tardiness_tables`
--
ALTER TABLE `tardiness_tables`
  ADD PRIMARY KEY (`tar_id`);

--
-- Indexes for table `trainings`
-- 
--
-- Indexes for table `undertime_tables`
--
ALTER TABLE `undertime_tables`
  ADD PRIMARY KEY (`und_id`);

--
-- Indexes for table `users`
-- 
ALTER TABLE `approvers`
  MODIFY `app_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `character_references`
--
 
--
-- AUTO_INCREMENT for table `companies`
--
  
ALTER TABLE `holidays`
  MODIFY `hol_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `job_descriptions`
-- 
ALTER TABLE `overtime_tables`
  MODIFY `ovt_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `pay_classes`
-- 
ALTER TABLE `philhealth_tables`
  MODIFY `phl_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `requirements`
-- 

ALTER TABLE `leave_types`
  MODIFY `lt_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

ALTER TABLE `sss_tables`
  MODIFY `sss_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `tardiness_tables`
--
ALTER TABLE `tardiness_tables`
  MODIFY `tar_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `trainings`
-- 
ALTER TABLE `undertime_tables`
  MODIFY `und_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `users`
-- 